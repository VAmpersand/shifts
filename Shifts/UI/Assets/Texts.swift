import UIKit

public enum Texts {
    static var create: String {
        return "Create"
    }
    
    static var cancel: String {
        return "Cancel"
    }
    
    static var save: String {
        return "Save"
    }
    
    static var ok: String {
        return "OK"
    }
    
    static var delete: String {
        return "Delete"
    }
    
    static var edit: String {
        return "Edit"
    }
    
    static var clear: String {
        return "Clear"
    }
    
    static var dayOfTheWeek: [String] {
        [
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa",
            "Su",
        ]
    }
    
    static var months: [String] {
        [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ]
    }
    
    static var monthsReduct: [String] {
           [
               "Jan",
               "Feb",
               "Mar",
               "Apr",
               "May",
               "Jun",
               "Jul",
               "Aug",
               "Sep",
               "Oct",
               "Nov",
               "Dec",
           ]
       }
}

// MARK: - TabBar
public extension Texts {
    enum TabBar {
        static var tabBarTitles: [String] {
            [
                "Personal schedule",
                "Team schedule",
                "Settings",
            ]
        }
        
        static func title(for tab: TabBarController.Tab) -> String {
            switch tab {
            case .personalSchedule: return "Personal schedule"
            case .teamShifts: return "Team schedule"
            case .settings: return "Settings"
            }
        }
    }
}

// MARK: - PersonalSchedule
public extension Texts {
    enum PersonalSchedule {
        static var title: String {
            return "Personal schedule"
        }
    }
}

// MARK: - TeamSchedule
public extension Texts {
    enum TeamSchedule {
        static var title: String {
            return "Team schedule"
        }
    }
}

// MARK: - Settings
public extension Texts {
    enum Settings {
        static var title: String {
            return "Settings"
        }
        
        static var addButton: String {
                   return "Add"
               }
        
        static func setMenuCellData(type: MenuCellType) -> (title: String,
                                                            subtitle: String) {
                switch type {
                case .addEmployees:
                    return (title: "Employees data",
                            subtitle: "You can create and edit\nyour employees list")
                case .addShiftType:
                    return (title: "Shift types",
                            subtitle: "Сreate shift types by specifying the runtime and defining the type color")
                case .addTeamSchedule:
                    return (title: "Schedules data",
                            subtitle: "Сreate a schedule for your team")
                }
        }
                
        enum ViewsTitle: String {
            case staffEdit = "Add staff"
            case shiftTypeEdit = "Add shift type"
            case shiftsEdit = "Create schedule"
            case darkTheme = "Dark theme"
        }
        
        enum ViewsSubtitle: String {
            case staffEdit = "You can create and edit your employees list"
            case shiftTypeEdit = "Сreate shift types by specifying the runtime and defining the type color"
            case shiftsEdit = "Сreate a schedule for your team"
            case darkTheme = "Here you can switch to dark theme"
        }
        
        enum AddButton: String {
            case staffEdit = "Add employee"
            case shiftTypeEdit = "Add shift type"
            case shiftsEdit = "Add shifts"
            case darkTheme = "Switch theme"
        }
    }
}


// MARK: - AddEmployee
public extension Texts {
    enum AddEmployee {
        static var title: String {
            return "Fill in employee data"
        }
        
        enum DefaultEmployeeCell: String {
            case firstName = "First name"
            case lastName = "Last name"
            case patronymic = "Patronim"
            case position = "Position"
            case adminRights = "Aadmin rights:"
        }
    }
}

// MARK: - AddShiftType
public extension Texts {
    enum AddShiftType {
        static var title: String {
            return "Create shift type"
        }
        
        static var shiftTypeLable: String {
            return "Enter shift type name"
        }
        
        static var shiftTypePlaceholder: String {
            return "Shift type"
        }
        
        static var fromLable: String {
            return "From:"
        }
        
        static var toLable: String {
            return "To:"
        }
        
        static var colorsLable: String {
            return "Choose shift type color"
        }
        
        static var defaultShiftTypeName: String {
            return "Weekend"
        }
        
        enum DefaultShiftTypeCell {
            static var shiftTypeName: String {
                return "Shif type"
            }
            
            static func shiftTime(startTime: String, endTime: String) -> String {
                return "From: \(startTime) to: \(endTime)"
            }
        }
    }
}

// MARK: - AddTeamSchedule
public extension Texts {
    enum AddTeamSchedule {
        static var title: String {
            return "Create team shchedule"
        }
        
        static var numberOfEmployeesTitle: String {
            return "The number\n of employees"
        }
        
        static var workingTime: String {
            return "Enter team working time\n(in hours)"
        }
        
        static var workingTimePlaceholder: String {
            return "0"
        }
        
        static var monthPickerLable: String {
            return "Choose month for your schedule"
        }
        
        static var scheduleNamePlaceholder: String {
            return "Schedule name"
        }
        
        static var scheduleNameSubtitle: String {
            return "* Not necessary"
        }
    }
}

// MARK: - BuildTeamSchedule
public extension Texts {
    enum BuildTeamSchedule {
        static var hourLabel: String {
            return "hour"
        }
        
        static var dayLabel: String {
            return "day"
        }
    }
}

// MARK: - PopupAlert
public extension Texts {
    enum PopupAlert {
        static func alertDescrtiption(for alertType: AlertType) -> String {
            switch alertType {
            case .deleteEmployee:
                return "You can't delete an employee while he has at least one shift in the schedule. This employee has a shift in:"
            case .deleteShiftType:
                return "You can't delete this type of shift. It is used in:"
            case .deleteTeamSchedule:
                return "If you delete a team schedule, recovery will not be possible. Delete schedule?"
            case .clearTeamSchedule:
                return "Are you sure you want to reset the schedule data? In the future, data recovery does not work out."
            case .cancelScheduleEditing:
                return "Are you sure you want to undo the latest schedule changes?"
            case .employeeDataMatch:
                return "Employee with such data is already registered. Please enter unique data in at least one field."
            case .shiftTypeDataMatch:
                return "Shift type with such name is already registered. Please enter unique shift type name."
            case .teamScheduleDataMatch:
                return "A schedule has already been created for the selected month. Please add a unique name for the schedule."
            }
        }
    }
}
