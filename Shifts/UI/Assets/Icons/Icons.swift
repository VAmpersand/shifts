import UIKit

public enum Icons { }

// MARK: - TabBar
public extension Icons {
    enum TabBar {
        static var tabBarIcons: [UIImage] {
            
            let names = [
                "tabBar_track_unselected",
                "tabBar_progress_unselected",
                "tabBar_discover_unselected",
                "tabBar_upgrade_unselected",
                "tabBar_more_unselected",
                ].map { $0 + "_" + Theme.current.rawValue }
            
            return names.map { image(named: $0).withRenderingMode(.alwaysOriginal) }
            
        }
        
        static var selectedTabBarIcons: [UIImage] {
            
            let names = [
                "tabBar_track_selected",
                "tabBar_progress_selected",
                "tabBar_discover_selected",
                "tabBar_upgrade_selected",
                "tabBar_more_selected",
                ].map { $0 + "_" + Theme.current.rawValue }
            
            return names.map { image(named: $0).withRenderingMode(.alwaysOriginal) }
            
        }
    }
}

// MARK: - Upgrade
public extension Icons {
    enum Upgrade {
        static var upgradeCellsIcons: [UIImage] {
            let names = [
                "upgrade_cell_plus",
                "upgrade_cell_stats",
                "upgrade_cell_goals",
                "upgrade_cell_moon",
                "upgrade_cell_insights",
                "upgrade_cell_clap",
            ]

            return names.map { image(named: $0).withRenderingMode(.alwaysOriginal) }
        }

        static var tic: UIImage {
            image(named: "upgrade_done")
        }
    }
}

// MARK: - FirstUpgrade
public extension Icons {
    enum FirstUpgrade {
        static var firstUpgradeIcons: [UIImage] {
            let names = [
                "trial_cell_plus",
                "trial_cell_stats",
                "trial_cell_goals",
                "trial_cell_moon",
                "trial_cell_insights",
                "trial_cell_clap",
            ]

            return names.map { image(named: $0).withRenderingMode(.alwaysOriginal) }
        }
    }
}

// MARK: - Launch
public extension Icons {
    enum Launch {
        static var logo: UIImage {
            image(named: "launch_logo")
        }
    }
}

// MARK: - Launch
public extension Icons {
    enum Welcome {
        static var logo: UIImage {
            image(named: "welcome_logo")
        }

        static var triangle: UIImage {
            image(named: "welcome_triangle")
        }
    }
}

// MARK: - More
public extension Icons {
    enum More {
        static var chat: UIImage {
            image(named: "more_chat")
        }

        static var discuss: UIImage {
            image(named: "more_discuss")
        }

        static var moon: UIImage {
            image(named: "more_moon")
        }

        static var more: UIImage {
            image(named: "more_more")
        }

        static var star: UIImage {
            image(named: "more_star")
        }

        static var premium: UIImage {
            image(named: "more_premium")
        }

        static var trial: UIImage {
            image(named: "more_trial")
        }
    }
}

// MARK: - Tracker
public extension Icons {
    enum Tracker {
        static var add: UIImage {
            image(named: "tracker_add")
        }

        static var tutorial: UIImage {
            image(named: "tracker_tutorial")
        }

        static var calendar: UIImage {
            image(named: "tracker_calendar")
        }

        static var simple: UIImage {
            image(named: "tracker_simple")
        }

        static var howMuch: UIImage {
            image(named: "tracker_how_much")
        }

        static var slider: UIImage {
            image(named: "tracker_slider")
        }
    }
}

// MARK: - Common
public extension Icons {
    enum Common {
        static var back: UIImage {
            image(named: "common_back")
        }

        static var close: UIImage {
            image(named: "common_close")
        }
    }
}

// MARK: - Add
public extension Icons {
    enum Add {
        static var cycle: UIImage {
            image(named: "add_cycle")
        }

        static var minus: UIImage {
            image(named: "add_minus")
        }

        static var plus: UIImage {
            image(named: "add_plus")
        }
    }
}

// MARK: - Emojis
public extension Icons {
    enum Emojis {
        static var emojiSelected: UIImage {
            image(named: "emoji_selected")
        }

        static var emojiUnselected: UIImage {
            image(named: "emoji_unselected")
        }

    }
}

// MARK: - Stats
public extension Icons {
    enum Stats {
        static var triangleForward: UIImage {
            image(named: "triangle_forward")
        }
        static var trianglePrevious: UIImage {
            image(named: "triangle_previous")
        }
        
        static var todayMark: UIImage {
            image(named: "calendar_todayMark")
        }
    }
}

// MARK: - Goal
public extension Icons {
    enum Goal {
        static var noGoal: UIImage {
            image(named: "goal_noGoal")
        }

        static var maximum: UIImage {
            image(named: "goal_maximum")
        }

        static var minimum: UIImage {
            image(named: "goal_minimum")
        }

        static var exactNumber: UIImage {
            image(named: "goal_exactNumber")
        }
    }
}

// MARK: - Edit
public extension Icons {
    enum Edit {
        static var triangleUp: UIImage {
            image(named: "triangleUp_accessory")
        }

        static var triangleDown: UIImage {
            image(named: "triangleDown_accessory")
        }
    }
}

// MARK: - Settings
public extension Icons {
    enum Settings {
        static var delete: UIImage {
            image(named: "delete_icon")
        }

        static var edit: UIImage {
            image(named: "edit_icon")
        }
        
        static var admin: UIImage {
            image(named: "admin_icon")
        }
    }
}


extension Icons {
    static func image(named name: String) -> UIImage {
        UIImage(named: name) ?? UIImage()
    }
}
