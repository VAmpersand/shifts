import UIKit

public enum Colors { }

extension Colors {
    static var calendarWeekday: UIColor {
        if Theme.current == .light { return .black }
        return .white
    }
    
    static var calendarWeekend: UIColor {
        if Theme.current == .light { return .red }
        return .red
    }
    
    static var calendarTodayMark: UIColor {
        if Theme.current == .light { return .red }
        return .white
    }
    
    static var trailingDeleteButton: UIColor {
        if Theme.current == .light { return .red }
        return .red
    }
    
    static var trailingEditButton: UIColor {
        if Theme.current == .light { return .gray }
        return .gray
    }
    
    static var shiftTypeColors: [UIColor] {
        if Theme.current == .light {
            return [
                #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1), #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1), #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1), #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1),
                #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1), #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1),
                #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1), #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1), #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1), #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),
                .clear,
            ]
        }
        return [
            #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1), #colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1), #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1), #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1),
            #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1), #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1), #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1),
            #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1), #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1), #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1), #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1), #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),
            .clear,
        ]
    }
    
//    static var weekendColor: UIColor {
//        return .clear
//    }
}





extension Colors {
    static var tabTintColor: UIColor {
        if Theme.current == .light { return .init(hex: 0xFFFFFF) }
        return .init(hex: 0x566082)
    }

    static var tabBarTextUnselected: UIColor {
        if Theme.current == .light { return .init(hex: 0x252F55) }
        return .init(hex: 0xFFFFFF)
    }

    static var tabBarTextSelected: UIColor {
        if Theme.current == .light { return .init(hex: 0x9E89FA) }
        return .init(hex: 0x87A1FB)
    }

    static var title: UIColor {
        if Theme.current == .light { return .init(hex: 0x252F55) }
        return .init(hex: 0xFFFFFF)
    }

    static var titleInverted: UIColor {
        if Theme.current == .light { return .init(hex: 0xFFFFFF) }
        return .init(hex: 0x252F55)
    }

    static var periodView: UIColor {
        return Theme.current == .light ? UIColor(hex: 0xFFFFFF) : UIColor(hex: 0x5b6281)
    }

    static var upgradeProductView: UIColor {
        if Theme.current == .light { return .init(hex: 0xF4F6FA) }
        return UIColor(hex: 0xF4F6FA).withAlphaComponent(0.3)
    }

    static var amountDeleteButton: UIColor {
        return Theme.current == .light ? UIColor(hex: 0xBFC1C9) : UIColor(hex: 0xFFFFFF)
    }

    static var statCellSeparator: UIColor {
        return Theme.current == .light ? UIColor(hex: 0xBFC1C9) : UIColor(hex: 0xFFFFFF)
    }

    static var progressTextView: UIColor {
        return Theme.current == .light ? UIColor(hex: 0xF4F6FA) : UIColor(hex: 0x566082)
    }

    static var thumbColor: UIColor {
        return UIColor(hex: 0xFFFFFF).withAlphaComponent(0.3)
    }

    static var moreCellSeparator: UIColor {
        return UIColor(hex: 0xBDBDBD)
    }

    static var alertTint: UIColor {
        return UIColor(hex: 0x87A1FB)
    }

    static var trackCell: UIColor {
        if Theme.current == .light { return .init(hex: 0xF4F6FA) }
        return UIColor(hex: 0x404A6F)
    }

    static var trackEditBackground: UIColor {
        return UIColor(hex: 0x252F55).withAlphaComponent(0.6)
    }

    static var discoverCellActionLabel: UIColor {
        return UIColor(hex: 0x999999)
    }

    static var discoverHeaderSeparator: UIColor {
        return UIColor(hex: 0x9E89FA)
    }

    static var lightBlue: UIColor {
        return UIColor(hex: 0xF4F6FA)
    }

    static var darkBlue: UIColor {
        return UIColor(hex: 0x252F55)
    }

    static var chartAxis: UIColor {
        return Theme.current == .light ? UIColor(hex: 0xBFC1C9) : UIColor(hex: 0xFFFFFF)
    }

    static var chartLine: UIColor {
        return UIColor(hex: 0x70BAFC)
    }

    static var moreSwitchOff: UIColor {
        return UIColor(hex: 0xE0E0E0)
    }

    static var firstUpgradeTextColor: UIColor {
        return UIColor(hex: 0xFFFFFF)
    }

    static var firstUpgradeProductView: UIColor {
        return UIColor(hex: 0xF4F6FA).withAlphaComponent(0.3)
    }

    static var trialProductView: UIColor {
           return UIColor(hex: 0xF4F6FA).withAlphaComponent(0.1)
       }
}

extension Colors {
    static var backgroundGradientDark: UIColor {
        return .init(patternImage: Icons.image(named: "background_gradient_dark"))
    }

    static var progressTextGradient: UIColor {
        return .init(patternImage: Icons.image(named: "progress_text_gradient"))
    }
    static var upgradeGradient: UIColor {
        return .init(patternImage: Icons.image(named: "upgrade_gradient"))
    }

    static var productViewBorderGradient: UIColor {
        return .init(patternImage: Icons.image(named: "product_view_border_gradient"))
    }

    static var chartViewBarGradient: UIColor {
        return .init(patternImage: Icons.image(named: "chart_view_bar_gradient"))
    }

    static var moreSwitchGradient: UIColor {
        return .init(patternImage: Icons.image(named: "more_switch_gradient"))
    }

    static var discoverCellGradient: [UIColor] {
        return [.init(hex: 0x9E89FA),
                .init(hex: 0x6EB9FB), ]
    }

    static var darkBackgroundGradient: [UIColor] {
        return [.init(hex: 0x34416D),
                .init(hex: 0x141A3A), ]
    }

    static var discoverBackButtonText: UIColor {
        if Theme.current == .light {
            return UIColor(red: 0.145, green: 0.184, blue: 0.333, alpha: 1)
        }
        return .white
    }

    static var discoverBackButtonBorder: UIColor {
        if Theme.current == .light {
            return UIColor(red: 0.751, green: 0.756, blue: 0.787, alpha: 1)
        }
        return .white
    }
}

public enum Theme: String {
    case light, dark

    static func toggle() {
        switch current {
        case .dark:
            UserDefaults.theme = .light
            NotificationCenter.default.post(name: .lightThemeDidApplied, object: nil)
        case .light:
            UserDefaults.theme = .dark
            NotificationCenter.default.post(name: .darkThemeDidApplied, object: nil)
        }
    }

    static var current: Theme {
        return UserDefaults.theme
    }
}
