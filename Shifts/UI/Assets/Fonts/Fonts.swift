//import UIKit
//
//public enum Fonts {
//    static func regular(of size: CGFloat) -> UIFont {
//        UIFont(name: "SFProDisplay-Regular", size: size) ?? UIFont()
//    }
//
//    static func robotoRegular(of size: CGFloat) -> UIFont {
//        UIFont(name: "Roboto-Regular", size: size) ?? UIFont()
//    }
//
//    static func robotoLight(of size: CGFloat) -> UIFont {
//        UIFont(name: "Roboto-Light", size: size) ?? UIFont()
//    }
//
//    static func bold(of size: CGFloat) -> UIFont {
//        UIFont(name: "SFProDisplay-Bold", size: size) ?? UIFont()
//    }
//
//    static func semiBold(of size: CGFloat) -> UIFont {
//        UIFont(name: "SFProDisplay-Semibold", size: size) ?? UIFont()
//    }
//}
