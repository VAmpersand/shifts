import UIKit

public enum Constants {}

// MARK: - Current date value
extension Constants {
    static var currentYear: Int {
        return Calendar.current.component(.year, from: Date())
    }
    
    static var currentMonth: Int {
        return Calendar.current.component(.month, from: Date())
    }
    
    static var currentDay: Int {
        return Calendar.current.component(.day, from: Date())
    }
    
    static func numbersOfDaysInMonth(presentedYear: Int) -> [Int] {
        if (presentedYear % 4) == 0 {
            return [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        }
        return [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    }
    
    enum cgFloat: CGFloat {
        case p1 = 1
        case p2 = 2
        case p4 = 4
        case p5 = 5
        case p10 = 10
        case p15 = 15
        case p20 = 20
        case p23 = 23
        case p35 = 35
        case p50 = 50
        case p60 = 60
        case p100 = 100
        case p120 = 120
    }
}

// MARK: - PersonalSchedule
extension Constants {
    enum PersonalShifts {
        static var heightToWidthRatio: CGFloat {
            return 0.844
        }
        
        
        static func getMarkForTodayParametrs() -> (center: CGPoint, radius: CGFloat) {
            switch IPhone.screen {
            case .screen4inch:
                return (center: CGPoint(x: 20.4, y: 20.4), radius: 18)
            case .screen4_7inch, .screen5_8inch:
                return (center: CGPoint(x: 24.4, y: 24.4), radius: 21)
            case .screen5_5inch, .screen6_1and6_5inch:
                return (center: CGPoint(x: 27.2, y: 27.2), radius: 24)
            }
        }
    }
}

// MARK: - Settings
extension Constants {
    enum Settings {
        static var employeeTextFieldWidth: CGFloat {
            return (UIScreen.main.bounds.width - 50) / 2
        }
    }
}

// MARK: - AddShiftType
extension Constants {
    enum AddShiftType {
        static var employeeTextFieldHeight: CGFloat {
            return 50
        }
        
        static var colorCellWidth: CGFloat {
            return (UIScreen.main.bounds.width - 40) / 6
        }
    }
}

// MARK: - AddTeamShifts
extension Constants {
    enum AddTeamShifts {
        static var buttonHeight: CGFloat {
            return 32
        }
        
        static var rightViewWidth: CGFloat {
            return 90
        }
        
        static var workingTimeFieldHeight: CGFloat {
            return 50
        }
        
        enum NumbersOf {
            static var firstRows: Int {
                return 2
            }
            
            static var lastRows: Int {
                return 1
            }
            
            static var firstColumn: Int {
                return 1
            }
            
            static var lastColumn: Int {
                return 2
            }
        }
        
        enum CellWidth {
            static var firstColumn: CGFloat {
                return 120
            }
            
            static var nextColumns: CGFloat {
                return 35
            }
        }
        
        enum CellHeight {
            static var firstRows: CGFloat {
                return 20
            }
            
            static var nextRows: CGFloat {
                return 27
            }
        }
    }
}


extension Constants {
    static var formatForScheduleDate: String {
        return "dd-MM-yyyy"
    }
}
