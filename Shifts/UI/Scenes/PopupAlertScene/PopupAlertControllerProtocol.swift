public protocol PopupAlertControllerProtocol: class {
    func setAlert(alertType: AlertType)
    func setScheduleList(scheduleList: [String])
}

