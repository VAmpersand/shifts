import UIKit

public final class PopupAlertController: BaseController {
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidLoad()
        setupSelf()
    }
    
    public var viewModel: PopupAlertViewModelProtocol!
    private var scheduleList: [String] = []
    
    private var alertType: AlertType! {
        didSet {
            let text = Texts.PopupAlert.alertDescrtiption(for: alertType) + "\n" + scheduleList.joined(separator: "\n")
            descriptionLabel.text = text
            setupOkButton(with: alertType)
        }
    }
    
    private var blurView: UIVisualEffectView =  {
        let visualEffectView = UIVisualEffectView()
        visualEffectView.effect = UIBlurEffect(style: .systemUltraThinMaterialDark)
        
        return visualEffectView
    }()
    
    private lazy var conteinerView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).withAlphaComponent(0.8)
        view.layer.cornerRadius = 15
        
        return view
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = .black
        
        return label
    }()
    
    private lazy var okButton: UIButton = {
        let button = UIButton()
        button.setTitle(Texts.ok, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleOkButton), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle(Texts.ok, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleCancelButton), for: .touchUpInside)
        
        return button
    }()
}

extension PopupAlertController {
    override func addSubviews() {
        super.addSubviews()
        view.addSubview(blurView)
        view.addSubview(conteinerView)
        conteinerView.addSubview(descriptionLabel)
        conteinerView.addSubview(okButton)
        conteinerView.addSubview(cancelButton)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        blurView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        conteinerView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.left.right.equalTo(view.safeAreaLayoutGuide).inset(Constants.cgFloat.p20.rawValue)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.top.right.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
        
        cancelButton.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.top.bottom.equalTo(okButton)
        }
    }
}

// MARK: - PopupAlertControllerProtocol
extension PopupAlertController: PopupAlertControllerProtocol {
    public func setAlert(alertType: AlertType) {
        self.alertType = alertType
    }
    
    public func setScheduleList(scheduleList: [String]) {
        self.scheduleList = scheduleList
    }
}

// MARK: - Actions
private extension PopupAlertController {
    @objc func handleOkButton() {
        viewModel.handleClose()
        NotificationCenter.default.post(name: .okButtonPressed, object: nil)
    }
    
    @objc func handleCancelButton() {
        viewModel.handleClose()
    }
}

private extension PopupAlertController {
    func setupOkButton(with alertType: AlertType!) {
        guard let alertType = alertType else { return }
        
        switch alertType {
        case .deleteEmployee, .deleteShiftType, .employeeDataMatch,
             .shiftTypeDataMatch, .teamScheduleDataMatch:
            cancelButton.setTitle(Texts.ok, for: .normal)
        case .deleteTeamSchedule:
            cancelButton.setTitle(Texts.cancel, for: .normal)
            okButton.setTitle(Texts.delete, for: .normal)
        case .cancelScheduleEditing, .clearTeamSchedule:
            cancelButton.setTitle(Texts.cancel, for: .normal)
            okButton.setTitle(Texts.ok, for: .normal)
        }
        
        okButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.right.equalToSuperview()
            make.left.equalTo(cancelButton.snp.right)
            make.top.equalTo(descriptionLabel.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            switch alertType {
            case .deleteEmployee, .deleteShiftType, .employeeDataMatch,
                 .shiftTypeDataMatch, .teamScheduleDataMatch:
                make.width.equalTo(0)
            default:
                make.width.equalToSuperview().multipliedBy(0.5)
            }
        }
    }
}
