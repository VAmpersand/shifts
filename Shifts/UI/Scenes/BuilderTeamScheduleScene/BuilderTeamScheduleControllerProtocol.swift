public protocol BuilderTeamScheduleControllerProtocol: class {
    func setTeamSchedule(teamSchedule: TeamSchedule)
    func setShiftTypes(shiftTypes: [ShiftType])
}

