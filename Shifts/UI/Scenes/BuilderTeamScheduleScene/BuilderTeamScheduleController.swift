import UIKit
import RealmSwift
import SpreadsheetView

public final class BuilderTeamScheduleController: UIViewController {
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidLoad()
        setupSelf()
        applyTheme()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    public var viewModel: BuilderTeamScheduleViewModelProtocol!
    
    private var teamSchedule: TeamSchedule!
    private var scheduleData: [[ShiftType]] = []
    
    public lazy var teamScheduleView = TeamScheduleView(teamSchedule)
    
    private lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1).withAlphaComponent(0.3)
        view.layer.cornerRadius = 15
        
        return view
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleSaveButton), for: .touchUpInside)
        button.setTitle(Texts.save, for: .normal)
        
        return button
    }()
    
    private lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleCancelButton), for: .touchUpInside)
        button.setTitle(Texts.cancel, for: .normal)
        
        return button
    }()
    
    private lazy var clearButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleClearButton), for: .touchUpInside)
        button.setTitle(Texts.clear, for: .normal)
        
        return button
    }()
    
    private lazy var shiftTypePickerView = BuilderTeamScheduleController.ShiftTypePickerView()
}

private extension BuilderTeamScheduleController {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        addObserver()
        setDeviceOrientation(value: .landscapeRight)
        teamScheduleView.delegate = self
      
        view.backgroundColor = .white
    }
    
    func addSubviews() {
        view.addSubview(teamScheduleView)
        view.addSubview(contentView)
        contentView.addSubview(saveButton)
        contentView.addSubview(cancelButton)
        contentView.addSubview(clearButton)
        contentView.addSubview(shiftTypePickerView)
    }
    
    func constraintSubviews() {
        teamScheduleView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).inset(Constants.cgFloat.p10.rawValue)
            make.left.bottom.equalTo(view.safeAreaLayoutGuide)
            make.right.equalTo(contentView.snp.left).offset(-Constants.cgFloat.p10.rawValue)
        }
        
        contentView.snp.makeConstraints { make in
            make.top.bottom.equalTo(teamScheduleView)
            make.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.width.equalTo(Constants.AddTeamShifts.rightViewWidth)
        }
        
        saveButton.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p5.rawValue)
            make.height.equalTo(Constants.AddTeamShifts.buttonHeight)
        }
        
        cancelButton.snp.makeConstraints { make in
            make.top.equalTo(saveButton.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p5.rawValue)
            make.height.equalTo(Constants.AddTeamShifts.buttonHeight)
        }
        
        shiftTypePickerView.snp.makeConstraints { make in
            make.top.equalTo(cancelButton.snp.bottom).offset(Constants.cgFloat.p5.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p5.rawValue)
        }
        
        clearButton.snp.makeConstraints { make in
            make.top.equalTo(shiftTypePickerView.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p5.rawValue)
            make.bottom.equalToSuperview().inset(Constants.cgFloat.p15.rawValue)
            make.height.equalTo(Constants.AddTeamShifts.buttonHeight)
        }
    }
}

// MARK: - BuilderTeamScheduleControllerProtocol
extension BuilderTeamScheduleController: BuilderTeamScheduleControllerProtocol {
    public func setTeamSchedule(teamSchedule: TeamSchedule) {
        self.teamSchedule = teamSchedule
        teamScheduleView.setTeamSchedule(teamSchedule: teamSchedule)
    }
    
    public func setShiftTypes(shiftTypes: [ShiftType]) {
        shiftTypePickerView.setShiftTypes(shiftTypes: shiftTypes)
        teamScheduleView.setShiftTypes(shiftTypes: shiftTypes)
    }
}

// MARK: - Actions
extension BuilderTeamScheduleController {
    @objc func handleCancelButton() {
        NotificationCenter.default.post(name: .closeAddTeamScheduleScene, object: nil)
        viewModel.handleClose()
        setDeviceOrientation(value: .portrait)
        NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
    }
    
    @objc func handleSaveButton() {
        NotificationCenter.default.post(name: .closeAddTeamScheduleScene, object: nil)
        teamScheduleView.updateScheduleData()
        for index in 0..<teamSchedule.personalSchedules.count {
            let newPersonalSchedule = PersonalSchedule()
            if !scheduleData.isEmpty {
                scheduleData[index].forEach { newPersonalSchedule.shifts.append($0) }
                viewModel.updatePersonalSchedule(personalSchedule: teamSchedule.personalSchedules[index],
                                                 newPersonalSchedule: newPersonalSchedule)
            }
        }
        viewModel.handleClose()
        setDeviceOrientation(value: .portrait)
        NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
    }
    
    @objc func handleClearButton() {
        viewModel.presentPopupAlertScene(alertType: .clearTeamSchedule, scheduleList: [])
    }
}

//MARK: - TeamScheduleViewDelegate
extension BuilderTeamScheduleController: TeamScheduleViewDelegate {
    public func createButtonPressed(scheduleData: [[ShiftType]]) {
        self.scheduleData = scheduleData
    }
    
    public func updateTeamSchedule() {
        viewModel.viewDidLoad()
    }
}

private extension BuilderTeamScheduleController{
    func addObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(alertOkButtonPressed),
                                               name: .okButtonPressed,
                                               object: nil)
    }
    
    @objc func alertOkButtonPressed() {
        teamScheduleView.updateScheduleData()
        for index in 0..<teamSchedule.personalSchedules.count {
            let newPersonalSchedule = PersonalSchedule()
            if !scheduleData.isEmpty {
                guard let defaultShiftType = teamSchedule.shiftTypes.first(where: {
                    $0.shiftTypeName == Texts.AddShiftType.defaultShiftTypeName
                }) else { return }
                scheduleData[index].forEach { _ in newPersonalSchedule.shifts.append(defaultShiftType) }
                viewModel.updatePersonalSchedule(personalSchedule: teamSchedule.personalSchedules[index],
                                                 newPersonalSchedule: newPersonalSchedule)
                viewModel.viewDidLoad()
                teamScheduleView.scheduleData = []
                teamScheduleView.setScheduleData(teamSchedule: teamSchedule)
                teamScheduleView.teamScheduleView.reloadData()
            }
        }
    }
}

private extension BuilderTeamScheduleController {
    func setDeviceOrientation(value:  UIInterfaceOrientationMask) {
        appDelegate.myOrientation = value
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
    }
}

// MARK: - Themable
extension BuilderTeamScheduleController: Themable {
    @objc public func applyGenericTheme() {
        setNeedsStatusBarAppearanceUpdate()
    }

    public func applyLightTheme() {
        applyGenericTheme()
    }
    public func applyDarkTheme() {
        applyGenericTheme()
    }
}


