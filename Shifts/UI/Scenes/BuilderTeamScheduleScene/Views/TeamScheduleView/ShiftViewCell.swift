import UIKit
import SpreadsheetView

extension BuilderTeamScheduleController.TeamScheduleView {
    class ShiftViewCell: Cell {
        override init(frame: CGRect) {
            super.init(frame: frame)
            setupSelf()
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override public func layoutSubviews() {
            super.layoutSubviews()
            
            colorView.layer.cornerRadius = 2
        }
        
        public lazy var valueLable: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            
            return label
        }()
        
        public lazy var colorView = UIView()
    }
}

private extension BuilderTeamScheduleController.TeamScheduleView.ShiftViewCell {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        
        backgroundColor = .clear
    }
    
    func addSubviews() {
        addSubview(colorView)
        addSubview(valueLable)
    }
    
    func constraintSubviews() {
        colorView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(1)
        }
        
        valueLable.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

