public protocol TeamScheduleViewDelegate: class {
    func createButtonPressed(scheduleData: [[ShiftType]])
    func updateTeamSchedule()
}
