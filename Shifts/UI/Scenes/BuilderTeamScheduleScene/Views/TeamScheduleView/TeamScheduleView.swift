import UIKit
import SpreadsheetView
import RealmSwift

extension BuilderTeamScheduleController {
    public class TeamScheduleView: UIView {
        init(_ teamSchedule: TeamSchedule) {
            self.teamSchedule = teamSchedule
            super.init(frame: .zero)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        weak var delegate: TeamScheduleViewDelegate?
        
        private let cellID = String(describing: BuilderTeamScheduleController.TeamScheduleView.ShiftViewCell.self)
        private let numbersOf = Constants.AddTeamShifts.NumbersOf.self
        private var teamSchedule: TeamSchedule {
            didSet {
                let personalSchedules = teamSchedule.personalSchedules.sorted(byKeyPath: "order")
                personalSchedules.forEach { self.personalSchedules.append($0) }
            }
        }
        private var personalSchedules: [PersonalSchedule] = []
        
        private var shiftTypes: [ShiftType] = []
        private var weekdays: [String] = []
        public var scheduleData: [[ShiftType]] = []
        private var averageEmployees: [Double] = []
        private var amountWorkingTime: [Double] = []
        private var selectedShiftTypeIndex = 0

        public lazy var teamScheduleView: SpreadsheetView = {
            let view = SpreadsheetView()
            view.register(ShiftViewCell.self, forCellWithReuseIdentifier: cellID)
            view.delegate = self
            view.dataSource = self
            view.showsVerticalScrollIndicator = false
            view.showsHorizontalScrollIndicator = false
            
            return view
        }()
    }
}

//extension AddTeamShiftsController.TeamScheduleView: LayerEffectable {
//    public func applyEffects() {
//
//    }
//}

private extension BuilderTeamScheduleController.TeamScheduleView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        addObserver()
        
        backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1).withAlphaComponent(0.3)
        layer.cornerRadius = 15
    }
    
    func addSubviews() {
        addSubview(teamScheduleView)
    }
    
    func constraintSubviews() {
        teamScheduleView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: - SpreadsheetViewDataSource
extension BuilderTeamScheduleController.TeamScheduleView: SpreadsheetViewDataSource {
    public func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column < numbersOf.firstColumn {
            return Constants.AddTeamShifts.CellWidth.firstColumn
        }
        return Constants.AddTeamShifts.CellWidth.nextColumns
    }
    
    public func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if row < numbersOf.firstRows {
            return Constants.AddTeamShifts.CellHeight.firstRows
        }
        return Constants.AddTeamShifts.CellHeight.nextRows
    }
    
    public func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return weekdays.count + numbersOf.firstColumn + numbersOf.lastColumn
    }
    
    public func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return personalSchedules.count + numbersOf.firstRows + numbersOf.lastRows
    }
    
    public func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return numbersOf.firstColumn
    }
    
    public func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return numbersOf.firstRows
    }
    
    public func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(
            withReuseIdentifier: cellID,
            for: indexPath
            ) as! BuilderTeamScheduleController.TeamScheduleView.ShiftViewCell
        
        setGridlinesForCell(for: cell)
        setLabel(label: cell.valueLable, text: "")
        cell.colorView.backgroundColor = .clear
       
        let currentColumn = indexPath.column - numbersOf.firstColumn
        let currentRow = indexPath.row - numbersOf.firstRows
     
        if indexPath.row == 0 {
            if indexPath.column == 0 {
                setLabel(label: cell.valueLable,
                         text: "")
            } else if numbersOf.firstColumn...weekdays.count ~= indexPath.column {
                setLabel(label: cell.valueLable,
                         text: "\(indexPath.column)")
            }
            
        } else if indexPath.row == 1 {
            if indexPath.column == 0 {
                setLabel(label: cell.valueLable,
                         text: "")
            } else if numbersOf.firstColumn...weekdays.count ~= indexPath.column {
                setLabel(label: cell.valueLable,
                         text: "\(weekdays[currentColumn])")
            } else if indexPath.column == weekdays.count + 1 {
                setLabel(label: cell.valueLable,
                         text: Texts.BuildTeamSchedule.hourLabel)
            } else if indexPath.column == weekdays.count + 2 {
                setLabel(label: cell.valueLable,
                         text: Texts.BuildTeamSchedule.dayLabel)
            }
            
        } else if !personalSchedules.isEmpty &&
            numbersOf.firstRows...(numbersOf.firstRows + personalSchedules.count - 1) ~= indexPath.row {
            if indexPath.column == 0 {
                guard let employee = personalSchedules[currentRow].employee else { return cell }
                let lastName = "\(employee.lastName)"
                guard let firstName = personalSchedules[currentRow].employee.firstName.first else { return cell }
                guard let patronymic = personalSchedules[currentRow].employee.patronymic.first else { return cell }
                setLabel(label: cell.valueLable,
                         text: lastName + " \(firstName)." + "\(patronymic).",
                         textAlignment: .left,
                         fontSize: 15)
            } else if numbersOf.firstColumn...weekdays.count ~= indexPath.column {
                let personaleShifts = scheduleData[currentRow]
                let shift = personaleShifts[currentColumn]
                
                if shift.shiftDuration == 0 {
                    setLabel(label: cell.valueLable,
                             text: String(format: ""))
                } else {
                    setLabel(label: cell.valueLable,
                             text: String(format: "%g", shift.shiftDuration))
                }
                cell.colorView.backgroundColor = Colors.shiftTypeColors[shift.shiftColorIndex]
            } else if indexPath.column == weekdays.count + 1 {
                setLabel(label: cell.valueLable,
                         text: String(format: "%g", amountWorkingTime[currentRow]))
            } else if indexPath.column == weekdays.count + 2 {
                let workingDay = amountWorkingTime[currentRow] / teamSchedule.workingTime
                setLabel(label: cell.valueLable,
                         text: convertDoubleToStr(workingDay))
            }
            
            
        } else if indexPath.row == numbersOf.firstRows + personalSchedules.count {
            if indexPath.column == 0 {
                cell.valueLable.numberOfLines = 2
                setLabel(label: cell.valueLable,
                         text: Texts.AddTeamSchedule.numberOfEmployeesTitle,
                         textAlignment: .right,
                         fontSize: 10)
            } else if numbersOf.firstColumn...weekdays.count ~= indexPath.column {
                setLabel(label: cell.valueLable,
                         text: convertDoubleToStr(averageEmployees[currentColumn]))
            }
        }
        
        
        if indexPath.row == 1 {
            setGridlinesForCell(for: cell,
                                width: 1,
                                color: .black,
                                cellBorders: [.bottom])
            if indexPath.column == 0 {
                setGridlinesForCell(for: cell,
                                    width: 1,
                                    color: .black,
                                    cellBorders: [.right, .bottom])
            } else if indexPath.column == weekdays.count + 1 {
                setGridlinesForCell(for: cell,
                                    width: 1,
                                    color: .black,
                                    cellBorders: [.left, .bottom])
            }
            
        } else if indexPath.row == personalSchedules.count + numbersOf.firstRows {
            setGridlinesForCell(for: cell,
                                width: 1,
                                color: .black,
                                cellBorders: [.top])
            if indexPath.column == 0 {
                setGridlinesForCell(for: cell,
                                    width: 1,
                                    color: .black,
                                    cellBorders: [.right, .top])
            } else if indexPath.column == weekdays.count + 1 {
                setGridlinesForCell(for: cell,
                                    width: 1,
                                    color: .black,
                                    cellBorders: [.left, .top])
            }
        } else {
            if indexPath.column == 0 {
                setGridlinesForCell(for: cell,
                                    width: 1,
                                    color: .black,
                                    cellBorders: [.right])
            } else if indexPath.column == weekdays.count + 1 {
                setGridlinesForCell(for: cell,
                                    width: 1,
                                    color: .black,
                                    cellBorders: [.left])
            }
        }
        
        return cell
    }
}

// MARK: - SpreadsheetViewDelegate
extension BuilderTeamScheduleController.TeamScheduleView: SpreadsheetViewDelegate {
    public func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        let cell = spreadsheetView.cellForItem(at: indexPath) as! BuilderTeamScheduleController.TeamScheduleView.ShiftViewCell
        
        let currentColumn = indexPath.column - numbersOf.firstColumn
        let currentRow = indexPath.row - numbersOf.firstRows
        
        if numbersOf.firstRows...scheduleData.count + 1 ~= indexPath.row
            && numbersOf.firstColumn...weekdays.count ~= indexPath.column {
            var personaleShifts = scheduleData[currentRow]
            
            personaleShifts[currentColumn] = shiftTypes[selectedShiftTypeIndex]
            scheduleData[currentRow] = personaleShifts
            
            cell.colorView.backgroundColor = Colors.shiftTypeColors[shiftTypes[selectedShiftTypeIndex].shiftColorIndex]
              
            if shiftTypes[selectedShiftTypeIndex].shiftDuration == 0 {
                setLabel(label: cell.valueLable,
                         text: String(format: ""))
            } else {
                setLabel(label: cell.valueLable,
                         text: String(format: "%g", shiftTypes[selectedShiftTypeIndex].shiftDuration))
            }
            
            updateScheduleData()
            
            let avgEmployeesCell = spreadsheetView.cellForItem(
                at: IndexPath(row: numbersOf.firstRows + personalSchedules.count,
                              column: indexPath.column)
                ) as! BuilderTeamScheduleController.TeamScheduleView.ShiftViewCell
            setLabel(label: avgEmployeesCell.valueLable,
                     text: convertDoubleToStr(averageEmployees[currentColumn]))
            
            guard let amountHourCell = spreadsheetView.cellForItem(
                at: IndexPath(row: indexPath.row,
                              column: weekdays.count + 1)
                ) as? BuilderTeamScheduleController.TeamScheduleView.ShiftViewCell else { return }
            setLabel(label: amountHourCell.valueLable,
                     text: String(format: "%g", amountWorkingTime[currentRow]))
            
            guard let amountDayCell = spreadsheetView.cellForItem(
                at: IndexPath(row: indexPath.row,
                              column: weekdays.count + 2)
                ) as? BuilderTeamScheduleController.TeamScheduleView.ShiftViewCell else { return }
            let workingDay = amountWorkingTime[currentRow] / teamSchedule.workingTime
            
            setLabel(label: amountDayCell.valueLable,
                     text: convertDoubleToStr(workingDay))
        }
    }
}

extension BuilderTeamScheduleController.TeamScheduleView {
    public func setTeamSchedule(teamSchedule: TeamSchedule) {
        self.teamSchedule = teamSchedule
        setScheduleData(teamSchedule: teamSchedule)
    }
    
    public func setShiftTypes(shiftTypes: [ShiftType]) {
        self.shiftTypes = shiftTypes
    }
}

private extension BuilderTeamScheduleController.TeamScheduleView {
    func setGridlinesForCell(for cell: Cell,
                             width: CGFloat? = nil,
                             color: UIColor? = nil,
                             cellBorders: [CellBorders]? = nil) {
        
        cell.gridlines.top = .none
        cell.gridlines.left = .none
        cell.gridlines.bottom = .none
        cell.gridlines.right = .none
        
        guard let width = width, let color = color, let cellBorders = cellBorders else { return }
        cellBorders.forEach {
            switch $0 {
            case .top: cell.gridlines.top = .solid(width: width, color: color)
            case .right: cell.gridlines.right = .solid(width: width, color: color)
            case .bottom: cell.gridlines.bottom = .solid(width: width, color: color)
            case .left: cell.gridlines.left = .solid(width: width, color: color)
            }
        }
    }
    
    func setLabel(label: UILabel, text: String, textAlignment:  NSTextAlignment? = nil, fontSize: CGFloat? = nil) {
        label.text = text
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 13)
        guard let textAlignment = textAlignment, let fontSize = fontSize else { return }
        label.font = UIFont.systemFont(ofSize: fontSize)
        label.textAlignment = textAlignment
    }
    
    func convertDoubleToStr(_ value: Double) -> String {
        return  String(format: "%.1f", value)
    }
}

extension BuilderTeamScheduleController.TeamScheduleView {
    func setScheduleData(teamSchedule: TeamSchedule!) {
        guard let teamSchedule = teamSchedule else { return }
        let firstWeekDay = teamSchedule.scheduleDate.firstDayOfTheMonth.weekday
        var currentWeekday = firstWeekDay - 1

        for _ in 1...teamSchedule.scheduleDate.getDaysInMonth() {
            weekdays.append(Texts.dayOfTheWeek[currentWeekday])
            currentWeekday += 1
            if currentWeekday == 7 {
                currentWeekday = 0
            }
        }
        
        delegate?.updateTeamSchedule()
 
        personalSchedules.forEach { personalSchedule in
            var shifts: [ShiftType] = []
            if !personalSchedule.shifts.isEmpty {
                personalSchedule.shifts.forEach { shift in
                    shifts.append(shift)
                }
            } else {
                if let shiftType = shiftTypes.first(where: {
                    $0.shiftTypeName == Texts.AddShiftType.defaultShiftTypeName
                }) { shifts.append(shiftType) }
            }
            self.scheduleData.append(shifts)
        }

        updateScheduleData()
    }
    
    public func updateScheduleData() {
        amountWorkingTime = []
        averageEmployees = []
        
        var amountTime: Double = 0
        var amountsEmployees: [Double] = []
        for _ in 1...weekdays.count {
            amountsEmployees.append(0)
        }
        
        scheduleData.forEach { shifts in
            amountTime = 0
            for index in 0..<shifts.count {
                amountTime += shifts[index].shiftDuration
                amountsEmployees[index] += shifts[index].shiftDuration
            }
            amountWorkingTime.append(amountTime)
        }
        
        amountsEmployees.forEach {
            averageEmployees.append($0 / teamSchedule.workingTime)
        }
        delegate?.createButtonPressed(scheduleData: scheduleData)
    }
}

private extension BuilderTeamScheduleController.TeamScheduleView {
    func addObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(shiftTypesWasSelected(notification:)),
                                               name: .shiftTypesWasSelected,
                                               object: nil)
    }
    
    @objc func shiftTypesWasSelected(notification: Notification) {
        guard let selectedShuftTypeIndex = notification.object as? Int else { return }
        self.selectedShiftTypeIndex = selectedShuftTypeIndex
    }
}


