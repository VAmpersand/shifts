import UIKit
import SpreadsheetView

extension BuilderTeamScheduleController {
    public class ShiftTypeView: UIView {
        init() {
            super.init(frame: .zero)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override public func layoutSubviews() {
            super.layoutSubviews()
            
            colorView.layer.cornerRadius = colorView.frame.height / 2
        }
        
        public lazy var colorView = UIView()
        
        public lazy var timeLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .center
            label.font = UIFont.systemFont(ofSize: 10)
            
            return label
        }()
    }
}

//extension AddTeamShiftsController.TeamScheduleView: LayerEffectable {
//    public func applyEffects() {
//
//    }
//}

private extension BuilderTeamScheduleController.ShiftTypeView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
    }
    
    func addSubviews() {
        addSubview(colorView)
        addSubview(timeLabel)
    }
    
    func constraintSubviews() {
        colorView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        timeLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
