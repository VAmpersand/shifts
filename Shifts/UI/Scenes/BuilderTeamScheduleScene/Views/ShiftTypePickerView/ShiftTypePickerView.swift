import UIKit

extension BuilderTeamScheduleController {
    public class ShiftTypePickerView: UIView {
        init() {
            super.init(frame: .zero)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        public var shiftTypes: [ShiftType]!
        
        public lazy var shiftTypePickerView: UIPickerView = {
            let picker = UIPickerView()
            picker.dataSource = self
            picker.delegate = self
            
            return picker
        }()
    }
}

//extension AddTeamShiftsController.ShiftTypePickerView: LayerEffectable {
//    public func applyEffects() {
//
//    }
//}

private extension BuilderTeamScheduleController.ShiftTypePickerView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
    }
    
    func addSubviews() {
        addSubview(shiftTypePickerView)
    }
    
    func constraintSubviews() {
        shiftTypePickerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: - UIPickerViewDataSource
extension BuilderTeamScheduleController.ShiftTypePickerView: UIPickerViewDataSource {
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return Constants.cgFloat.p35.rawValue
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return shiftTypes.count
    }
}

// MARK: - UIPickerViewDelegate
extension BuilderTeamScheduleController.ShiftTypePickerView: UIPickerViewDelegate {
    public func pickerView(_ pickerView: UIPickerView,
                           viewForRow row: Int,
                           forComponent component: Int,
                           reusing view: UIView?) -> UIView {
    
        return getShiftTypeView(for: row)
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        NotificationCenter.default.post(name: .shiftTypesWasSelected, object: row)
    }
}

private extension BuilderTeamScheduleController.ShiftTypePickerView {
    func getShiftTypeView(for row: Int) -> UIView {
        let view = BuilderTeamScheduleController.ShiftTypeView()
        guard let shiftTypes = shiftTypes else { return view}
        
        view.colorView.backgroundColor = Colors.shiftTypeColors[shiftTypes[row].shiftColorIndex]
        view.timeLabel.text = [shiftTypes[row].startTime,
                               shiftTypes[row].endTime].joined(separator: "-")
        if shiftTypes[row].shiftTypeName == Texts.AddShiftType.defaultShiftTypeName {
            view.timeLabel.text = Texts.AddShiftType.defaultShiftTypeName
        }
        
        return view
    }
}

extension BuilderTeamScheduleController.ShiftTypePickerView {
    public func setShiftTypes(shiftTypes: [ShiftType]) {
        self.shiftTypes = shiftTypes
    }
}

