import UIKit

public final class SettingsController: BaseController {
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidLoad()
        setupSelf()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let selectedIndex = selectedIndex else { return }
        settingsTableView.reloadRows(at: [selectedIndex], with: .none)
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }
    
    public var viewModel: SettingsViewModelProtocol!
    
    private var selectedIndex: IndexPath!
    private var prevSelectedIndex: IndexPath!
    private var employees: [Employee]!
    private var shiftTypes: [ShiftType]!
    private var teamSchedules: [TeamSchedule]!
    private var personalSchedules: [PersonalSchedule]!
    
    private lazy var settingsTableView: UITableView = {
        let table = UITableView()
        table.delegate = self
        table.dataSource = self
        table.separatorStyle = .none
        
        return table
    }()
    
    private lazy var employeesListEditingView = EmployeesListEditingView()
}

extension SettingsController {
    override func setupSelf() {
        super.setupSelf()
        addObserversForViewsEditing()
        setSettingsTableData()

        titleLabel.text = Texts.Settings.title
    }
    
    override func addSubviews() {
        super.addSubviews()
        view.addSubview(settingsTableView)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        settingsTableView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(Constants.cgFloat.p4.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(Constants.cgFloat.p10.rawValue)
        }
    }
}

// MARK: - SettingsControllerProtocol
extension SettingsController: SettingsControllerProtocol {
    public func setEmployees(_ employees: [Employee]!) {
        self.employees = employees
    }
    
    public func setShiftTypes(_ shiftTypes: [ShiftType]!) {
        self.shiftTypes = shiftTypes
    }
    
    public func setTeamSchedules(_ teamSchedules: [TeamSchedule]!) {
        self.teamSchedules = teamSchedules
    }
    
    public func setPersonalSchedules(_ personalSchedules: [PersonalSchedule]!) {
        self.personalSchedules = personalSchedules
    }
}

//MARK: - UITableViewDataSource
extension SettingsController: UITableViewDataSource {
    public func tableView(_ tableView: UITableView,
                          numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView,
                          cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.item {
        case 0: cell = tableView.dequeueReusableCell(withIdentifier: EmployeesListEditingView.cellID,
                                                     for: indexPath) as! EmployeesListEditingView
        (cell as? EmployeesListEditingView)?.delegate = self
        (cell as? EmployeesListEditingView)?.setEmployees(employees)
        (cell as? EmployeesListEditingView)?.setPersonalSchedules(personalSchedules)
        (cell as? EmployeesListEditingView)?.setTeamSchedules(teamSchedules)
        (cell as? EmployeesListEditingView)?.animateCell()
  
        case 1: cell = tableView.dequeueReusableCell(withIdentifier: ShiftTypeEditingView.cellID,
                                                     for: indexPath) as! ShiftTypeEditingView
        (cell as? ShiftTypeEditingView)?.delegate = self
        (cell as? ShiftTypeEditingView)?.setShiftTypes(shiftTypes)
        (cell as? ShiftTypeEditingView)?.setTeamSchedules(teamSchedules)
        (cell as? ShiftTypeEditingView)?.animateCell()
            
        
        case 2: cell = tableView.dequeueReusableCell(withIdentifier: TeamScheduleEditingView.cellID,
                                                     for: indexPath) as! TeamScheduleEditingView
        (cell as? TeamScheduleEditingView)?.delegate = self
        (cell as? TeamScheduleEditingView)?.setTeamSchedules(teamSchedules)
        (cell as? TeamScheduleEditingView)?.animateCell()
            

        default: fatalError("Invalid indexPath in settingsTableView")
        }

        return cell
    }
    
    public func tableView(_ tableView: UITableView,
                          heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let selectedIndexPath = selectedIndex else { return Constants.cgFloat.p50.rawValue }
        if selectedIndexPath == indexPath {
            return settingsTableView.frame.height - Constants.cgFloat.p50.rawValue * 2
        }
        return Constants.cgFloat.p50.rawValue
    }
}

//MARK: - UITableViewDelegate
extension SettingsController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView,
                          didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath
        
        if selectedIndex != prevSelectedIndex {
            tableView.beginUpdates()
            tableView.reloadRows(at: [selectedIndex], with: .none)
            tableView.endUpdates()
        }
        prevSelectedIndex = selectedIndex
        
        NotificationCenter.default.post(name: .saveEmployeesChange, object: nil)
    }
}

private extension SettingsController {
    func setSettingsTableData() {
        [(EmployeesListEditingView.self, EmployeesListEditingView.cellID),
         (ShiftTypeEditingView.self, ShiftTypeEditingView.cellID),
         (TeamScheduleEditingView.self, TeamScheduleEditingView.cellID)].forEach { cell, id in
            settingsTableView.register(cell, forCellReuseIdentifier: id)
        }
    }
}

private extension SettingsController {
    func addObserversForViewsEditing() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadScheduleData),
                                               name: .scheduleDataWasEdited,
                                               object: nil)
    }
    
    @objc func reloadScheduleData() {
        viewModel.viewDidLoad()
        settingsTableView.reloadData()
    }
}

//MARK: - EmployeesListEditingViewDelegate
extension SettingsController: EmployeesListEditingViewDelegate {
    public func moveEmployee(from index: Int, to newIndex: Int) {
        viewModel.moveEmployee(from: index, to: newIndex)
    }
    
    public func presentAddEmployeeScene(for employee: Employee) {
        viewModel.presentAddEmployeeScene(employee)
    }
    
    public func deleteEmployee(_ employee: Employee) {
        viewModel.deleteEmployee(employee)
    }
    
    public func presentEmployeeAlert(with alertType: AlertType,
                                       scheduleList: [String]) {
        viewModel.presentPopupAlertScene(with: alertType,
                                         scheduleList: scheduleList)
    }
}

//MARK: - ShiftTypeEditingViewDelegate
extension SettingsController: ShiftTypeEditingViewDelegate {
    public func presentAddShiftTypeScene(for shiftType: ShiftType) {
        viewModel.presentAddShiftTypeScene(shiftType)
    }
    
    public func deleteShiftType(_ shiftType: ShiftType) {
        viewModel.deleteShiftType(shiftType)
    }
    
    public func presentShiftTypeAlert(with alertType: AlertType,
                                       scheduleList: [String]) {
        viewModel.presentPopupAlertScene(with: alertType,
                                         scheduleList: scheduleList)
    }
}

//MARK: - TeamScheduleEditingViewDelegate
extension SettingsController: TeamScheduleEditingViewDelegate {
    public func presentAddTeamScheduleScene(for teamSchedule: TeamSchedule) {
        viewModel.presentAddTeamScheduleScene(teamSchedule)
    }
    
    public func deleteTeamSchedule(_ teamSchedule: TeamSchedule) {
        viewModel.deleteTeamSchedule(teamSchedule)
    }
    
    public func presentTeamScheduleAlert(with alertType: AlertType,
                                         scheduleList: [String]) {
        viewModel.presentPopupAlertScene(with: alertType,
                                         scheduleList: scheduleList)
    }
        
    public func presentBuilderTeamScheduleScene(for teamSchedule: TeamSchedule) {
        viewModel.presentBuilderTeamScheduleScene(teamSchedule)
    }
}
