public protocol SettingsControllerProtocol: class {
    func setEmployees(_ employees: [Employee]!)
    func setShiftTypes(_ shiftTypes: [ShiftType]!)
    func setTeamSchedules(_ teamSchedules: [TeamSchedule]!)
    func setPersonalSchedules(_ personalSchedules: [PersonalSchedule]!) 
}

