import UIKit

extension SettingsController.EmployeesListEditingView {
    public class EmployeeCell: BaseSettingsDataCell {
        public static let cellID = String(describing: EmployeeCell.self)
        
        private let adminIcon = Icons.Settings.admin.withRenderingMode(.alwaysTemplate)
        
        private lazy var nameLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            
            return label
        }()
        
        private lazy var positionLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            label.text = Texts.AddEmployee.DefaultEmployeeCell.position.rawValue
            label.font = UIFont.systemFont(ofSize: 12)
            
            return label
        }()
        
        private lazy var stackView: UIStackView = {
            let stackView = UIStackView()
            stackView.distribution = .fill
            stackView.spacing = 2
            stackView.axis = .vertical
            [
                nameLabel,
                positionLabel
                ].forEach { stackView.addArrangedSubview($0) }
            
            return stackView
        }()
        
        public lazy var adminRightsView: UIImageView = {
            let view = UIImageView()
            view.image = adminIcon
            view.isHidden = true
            
            return view
        }()
    }
}

extension SettingsController.EmployeesListEditingView.EmployeeCell {
    override func setupSelf() {
        super.setupSelf()
        addSubviews()
        constraintSubviews()
    }
    
    override func addSubviews() {
        super.addSubviews()
        addSubview(stackView)
        addSubview(adminRightsView)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        stackView.snp.makeConstraints { make in
            make.left.top.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.right.equalTo(adminRightsView.snp.left).offset(-Constants.cgFloat.p10.rawValue)
        }
        
        adminRightsView.snp.makeConstraints { make in
            make.right.top.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.width.equalTo(adminRightsView.snp.height)
        }
    }
}

extension SettingsController.EmployeesListEditingView.EmployeeCell {
    func setEmployeeData(employee: Employee!) {
        guard let employee = employee else { return }
        nameLabel.text = employee.lastName + " " + employee.firstName + " " + employee.patronymic
        positionLabel.text = employee.position
        adminRightsView.tintColor = employee.adminRights ? #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1).withAlphaComponent(0.3) : .clear
    }
}
