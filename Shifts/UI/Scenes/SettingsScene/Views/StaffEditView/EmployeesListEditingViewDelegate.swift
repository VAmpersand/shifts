import UIKit

public protocol EmployeesListEditingViewDelegate: class {
    func moveEmployee(from index: Int, to newIndex: Int)
    func presentAddEmployeeScene(for employee: Employee)
    func deleteEmployee(_ employee: Employee)
    func presentEmployeeAlert(with alertType: AlertType, scheduleList: [String])
}
