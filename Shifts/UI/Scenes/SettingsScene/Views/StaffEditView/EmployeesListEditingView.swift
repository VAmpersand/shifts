import UIKit

extension SettingsController {
    public class EmployeesListEditingView: BaseSettingsMenuCell {        
        public static let cellID = String(describing: EmployeesListEditingView.self)
        weak var delegate: EmployeesListEditingViewDelegate?
        
        private var teamSchedules: [TeamSchedule]!
        
        private var employees: [Employee]! {
            didSet {
                staffTableView.reloadData()
            }
        }
        
        private var personalSchedules: [PersonalSchedule]! {
            didSet {
                staffTableView.reloadData()
            }
        }
        
        
        private var deletedIndexPath: IndexPath!
        
        public lazy var staffTableView: UITableView = {
            let table = BaseSettingsDataTable()
            table.register(EmployeeCell.self, forCellReuseIdentifier: EmployeeCell.cellID)
            table.delegate = self
            table.dataSource = self
            
            return table
        }()
        
        private lazy var editingButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1).withAlphaComponent(0.3)
            button.layer.cornerRadius = 10
            button.setTitle("Edit", for: .normal)
            button.addTarget(self,
                             action: #selector(editingButtonPresesd),
                             for: .touchUpInside)
            
            return button
        }()
    }
}

//extension SettingsController.EmployeesListEditingView: LayerEffectable {
//    public func applyEffects() {
//
//    }
//}

extension SettingsController.EmployeesListEditingView {
    override func setupSelf() {
        super.setupSelf()
        addObservers()
    
        setCellData(type: .addEmployees)
        addButton.addTarget(self,
                         action: #selector(addEmployeeButtonPressed),
                         for: .touchUpInside)
    }
    
    override func addSubviews() {
        super.addSubviews()
        container.addSubview(staffTableView)
        container.addSubview(editingButton)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        editingButton.snp.makeConstraints { make in
            make.width.equalTo(Constants.cgFloat.p60.rawValue)
            make.top.bottom.equalTo(addButton)
            make.right.equalTo(addButton.snp.left).offset(-Constants.cgFloat.p10.rawValue)
        }
        
        staffTableView.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.top.equalTo(addButton.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.bottom.equalTo(container).inset(Constants.cgFloat.p10.rawValue)
        }
    }
}

// MARK: - Action
private extension SettingsController.EmployeesListEditingView {
    @objc func addEmployeeButtonPressed() {
        delegate?.presentAddEmployeeScene(for: Employee())
        UserDefaults.dataIsBeingEdited = false
    }
    
    
    @objc func editingButtonPresesd() {
        staffTableView.isEditing.toggle()
        if staffTableView.isEditing {
            editingButton.backgroundColor = UIColor.white.withAlphaComponent(0.3)
            editingButton.layer.cornerRadius = 10
            editingButton.setTitle("Save", for: .normal)
            staffTableView.reloadData()
        } else {
            editingButton.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1).withAlphaComponent(0.3)
            editingButton.layer.cornerRadius = 10
            editingButton.setTitle("Edit", for: .normal)
            staffTableView.reloadData()
        }
    }
}

// MARK:- Add gestures
private extension SettingsController.EmployeesListEditingView {
    func addObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(saveEmployeesChange),
                                               name: .saveEmployeesChange,
                                               object: nil)
        
    }
    
    @objc func saveEmployeesChange() {
        staffTableView.isEditing = false
        editingButton.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1).withAlphaComponent(0.3)
        editingButton.layer.cornerRadius = 10
        editingButton.setTitle("Edit", for: .normal)
        staffTableView.reloadData()
    }
}


extension SettingsController.EmployeesListEditingView {
    public func setEmployees(_ employees: [Employee]!) {
        self.employees = employees
    }
    
    public func setTeamSchedules(_ teamSchedules: [TeamSchedule]!) {
        self.teamSchedules = teamSchedules
    }
    
    public func setPersonalSchedules(_ personalSchedules: [PersonalSchedule]!) {
        self.personalSchedules = personalSchedules
    }
}


// MARK: - UITableViewDataSource
extension SettingsController.EmployeesListEditingView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let employees = employees else { return 0 }
        return employees.count
    }
    
    public func tableView(_ tableView: UITableView,
                          cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeCell.cellID,
                                                 for: indexPath) as! EmployeeCell
        cell.setEmployeeData(employee: employees[indexPath.item])
        cell.adminRightsView.isHidden = staffTableView.isEditing
        cell.separatorView.isHidden = indexPath.row == employees.count - 1 ? true : false
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SettingsController.EmployeesListEditingView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cgFloat.p60.rawValue
    }
    
    public func tableView(_ tableView: UITableView,
                          trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let tableView = tableView as? BaseSettingsDataTable,
            let employees = self.employees else {
                return UISwipeActionsConfiguration(actions: [])
        }
        
        let delete = tableView.deleteAction(at: indexPath) {
            let checkForUsingEmployeeInSchedules = self.checkForUsingEmployeeInSchedules(
                for: employees[indexPath.row]
            )
            if checkForUsingEmployeeInSchedules.isUsing {
                let scheduleList = checkForUsingEmployeeInSchedules.scheduleList
                self.delegate?.presentEmployeeAlert(with: .deleteEmployee,
                                                    scheduleList: scheduleList)
            } else {
                self.delegate?.deleteEmployee(employees[indexPath.row])
                NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
            }
        }
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    public func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let tableView = tableView as? BaseSettingsDataTable,
            let employees = self.employees else {
                return UISwipeActionsConfiguration(actions: [])
        }
        
        let editing = tableView.editingAction(at: indexPath) {
            UserDefaults.dataIsBeingEdited = true
            self.delegate?.presentAddEmployeeScene(for: employees[indexPath.row])
        }
   
        return UISwipeActionsConfiguration(actions: [editing])
    }
    
    public func tableView(_ tableView: UITableView,
                          moveRowAt sourceIndexPath: IndexPath,
                          to destinationIndexPath: IndexPath) {
        guard var employees = employees else { return }
        let elementToMove = employees[sourceIndexPath.row]
        employees.remove(at: sourceIndexPath.row)
        employees.insert(elementToMove, at: destinationIndexPath.row)
        
        delegate?.moveEmployee(from: sourceIndexPath.row,
                               to: destinationIndexPath.row)
        NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
    }
    
    public func tableView(_ tableView: UITableView,
                          editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
}

private extension SettingsController.EmployeesListEditingView {
    func checkForUsingEmployeeInSchedules(for employee: Employee) -> (isUsing: Bool,
                                                                      scheduleList: [String]) {
        var isUsed = false
        var scheduleList: [String] = []
        guard let teamSchedules = teamSchedules else { return (isUsed, scheduleList) }
        teamSchedules.forEach { teamSchedule in
            teamSchedule.personalSchedules.forEach { personalSchedule in
                if personalSchedule.employee.firstName == employee.firstName
                    && personalSchedule.employee.lastName == employee.lastName
                    && personalSchedule.employee.position == employee.position {
                    personalSchedule.shifts.forEach {
                        if $0.shiftTypeName != Texts.AddShiftType.defaultShiftTypeName {
                            isUsed = true
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMMM yyyy"
                            let scheduleDate = dateFormatter.string(from: teamSchedule.scheduleDate)
                            let schedule = teamSchedule.scheduleName + scheduleDate
                            if !scheduleList.contains(schedule) {
                                scheduleList.append(schedule)
                            }
                        }
                    }
                }
            }
        }

        return (isUsed, scheduleList)
    }
}
