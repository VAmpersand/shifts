import UIKit

public protocol TeamScheduleEditingViewDelegate: class {
    func presentAddTeamScheduleScene(for teamSchedule: TeamSchedule)
    func deleteTeamSchedule(_ teamSchedule: TeamSchedule)
    func presentTeamScheduleAlert(with alertType: AlertType, scheduleList: [String])
    func presentBuilderTeamScheduleScene(for teamSchedule: TeamSchedule)
}
