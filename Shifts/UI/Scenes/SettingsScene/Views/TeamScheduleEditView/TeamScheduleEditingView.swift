import UIKit

extension SettingsController {
    public class TeamScheduleEditingView: BaseSettingsMenuCell {
        public static let cellID = String(describing: TeamScheduleEditingView.self)
        weak var delegate: TeamScheduleEditingViewDelegate?
        
        private var teamSchedules: [TeamSchedule]! {
            didSet {
                teamScheduleTableView.reloadData()
            }
        }
                
        public lazy var teamScheduleTableView: UITableView = {
            let table = BaseSettingsDataTable()
            table.register(TeamScheduleCell.self, forCellReuseIdentifier: TeamScheduleCell.cellID)
            table.delegate = self
            table.dataSource = self
            
            return table
        }()
    }
}

//extension SettingsController.TeamScheduleEditingView: LayerEffectable {
//    public func applyEffects() {
//        
//    }
//}

extension SettingsController.TeamScheduleEditingView {
    override func setupSelf() {
        super.setupSelf()
        
        setCellData(type: .addTeamSchedule)
        addButton.addTarget(self,
                            action: #selector(addTeamScheduleButtonPressed),
                            for: .touchUpInside)
    }
    
    override func addSubviews() {
        super.addSubviews()
        container.addSubview(teamScheduleTableView)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        teamScheduleTableView.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.top.equalTo(addButton.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.bottom.equalTo(container.snp.bottom).inset(Constants.cgFloat.p10.rawValue)
        }
    }
}

// MARK: - Action
extension SettingsController.TeamScheduleEditingView {
    @objc func addTeamScheduleButtonPressed() {
        delegate?.presentAddTeamScheduleScene(for: TeamSchedule())
        UserDefaults.dataIsBeingEdited = false
    }
}

extension SettingsController.TeamScheduleEditingView {
    public func setTeamSchedules(_ teamSchedules: [TeamSchedule]!) {
        self.teamSchedules = teamSchedules
    }
}

// MARK: - UITableViewDataSource
extension SettingsController.TeamScheduleEditingView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let teamSchedules = teamSchedules else { return 0 }
        return teamSchedules.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TeamScheduleCell.cellID,
                                                 for: indexPath) as! TeamScheduleCell
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        
        cell.teamScheduleDateLabel.text = dateFormatter.string(from: teamSchedules[indexPath.item].scheduleDate)
        cell.teamScheduleNameSuntitle.text = teamSchedules[indexPath.item].scheduleName
        cell.separatorView.isHidden = indexPath.row == teamSchedules.count - 1 ? true : false
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SettingsController.TeamScheduleEditingView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cgFloat.p60.rawValue
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let teamSchedules = self.teamSchedules else { return }
        delegate?.presentBuilderTeamScheduleScene(for: teamSchedules[indexPath.row])
    }
    
    public func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let tableView = tableView as? BaseSettingsDataTable,
            let teamSchedules = self.teamSchedules else {
                return UISwipeActionsConfiguration(actions: [])
        }

        let delete = tableView.deleteAction(at: indexPath) {
            self.delegate?.deleteTeamSchedule(teamSchedules[indexPath.row])
            NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
        }
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    public func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let tableView = tableView as? BaseSettingsDataTable,
            let teamSchedules = self.teamSchedules else {
                return UISwipeActionsConfiguration(actions: [])
        }
        
        let editing = tableView.editingAction(at: indexPath) {
            UserDefaults.dataIsBeingEdited = true
            self.delegate?.presentAddTeamScheduleScene(for: teamSchedules[indexPath.row])
        }
        
        return UISwipeActionsConfiguration(actions: [editing])
    }
}

