import UIKit

extension SettingsController.TeamScheduleEditingView {
    public class TeamScheduleCell: BaseSettingsDataCell {
        public static let cellID = String(describing: TeamScheduleCell.self)
        
        public lazy var teamScheduleDateLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            
            return label
        }()
        
        public lazy var teamScheduleNameSuntitle: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            label.font = UIFont.systemFont(ofSize: 12)
            
            return label
        }()
        
        private lazy var stackView: UIStackView = {
            let stackView = UIStackView()
            stackView.distribution = .fill
            stackView.spacing = 2
            stackView.axis = .vertical
            [
                teamScheduleDateLabel,
                teamScheduleNameSuntitle
                ].forEach { stackView.addArrangedSubview($0) }
            
            return stackView
        }()
    }
}

extension SettingsController.TeamScheduleEditingView.TeamScheduleCell {
    override func setupSelf() {
        super.setupSelf()
        addSubviews()
        constraintSubviews()
    }
    
    override func addSubviews() {
        super.addSubviews()
        addSubview(stackView)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
    }
}
