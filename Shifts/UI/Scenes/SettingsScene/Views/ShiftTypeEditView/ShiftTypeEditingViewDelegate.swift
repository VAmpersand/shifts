import UIKit

public protocol ShiftTypeEditingViewDelegate: class {
    func presentAddShiftTypeScene(for shiftType: ShiftType)
    func deleteShiftType(_ shifType: ShiftType)
    func presentShiftTypeAlert(with alertType: AlertType, scheduleList: [String])
}
