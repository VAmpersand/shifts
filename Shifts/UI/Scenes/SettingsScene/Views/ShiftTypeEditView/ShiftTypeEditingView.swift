import UIKit

extension SettingsController {
    public class ShiftTypeEditingView: BaseSettingsMenuCell {
        public static let cellID = String(describing: ShiftTypeEditingView.self)
        weak var delegate: ShiftTypeEditingViewDelegate?
        
        private var teamSchedules: [TeamSchedule]!
        
        private var shiftTypes: [ShiftType]! {
            didSet {
                shiftTypeTableView.reloadData()
            }
        }
 
        public lazy var shiftTypeTableView: UITableView = {
            let table = BaseSettingsDataTable()
            table.register(ShiftTypeCell.self, forCellReuseIdentifier: ShiftTypeCell.cellID)
            table.delegate = self
            table.dataSource = self
            
            return table
        }()
    }
}


extension SettingsController.ShiftTypeEditingView: LayerEffectable {
    public func applyEffects() {

    }
}

extension SettingsController.ShiftTypeEditingView {
    override func setupSelf() {
        super.setupSelf()
        
        setCellData(type: .addShiftType)
        addButton.addTarget(self,
                            action: #selector(addShiftTypeButtonPressed),
                            for: .touchUpInside)
    }
    
    override func addSubviews() {
        super.addSubviews()
        container.addSubview(shiftTypeTableView)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        shiftTypeTableView.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.top.equalTo(addButton.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.bottom.equalTo(container.snp.bottom).inset(Constants.cgFloat.p10.rawValue)
        }
    }
}

// MARK: - Action
extension SettingsController.ShiftTypeEditingView {
    @objc func addShiftTypeButtonPressed() {
        delegate?.presentAddShiftTypeScene(for: ShiftType())
        UserDefaults.dataIsBeingEdited = false
    }
}

extension SettingsController.ShiftTypeEditingView {
    public func setShiftTypes(_ shiftTypes: [ShiftType]!) {
        self.shiftTypes = shiftTypes
    }
    
    public func setTeamSchedules(_ teamSchedules: [TeamSchedule]!) {
        self.teamSchedules = teamSchedules
    }
}

// MARK: - UITableViewDataSource
extension SettingsController.ShiftTypeEditingView: UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let shiftTypes = shiftTypes else { return 0 }
        return shiftTypes.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ShiftTypeCell.cellID,
                                                 for: indexPath) as! ShiftTypeCell
        cell.setShiftTypeData(shiftType: shiftTypes[indexPath.item])
        cell.separatorView.isHidden = indexPath.row == shiftTypes.count - 1 ? true : false
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SettingsController.ShiftTypeEditingView: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 0
        }
        return Constants.cgFloat.p60.rawValue
    }
    
    public func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let tableView = tableView as? BaseSettingsDataTable,
            let shiftTypes = self.shiftTypes else {
                return UISwipeActionsConfiguration(actions: [])
        }
        
        let delete = tableView.deleteAction(at: indexPath) {
            let checkForUsingShiftTypeInSchedules = self.checkForUsingShiftTypeInSchedules(
                for: shiftTypes[indexPath.row]
            )
            if checkForUsingShiftTypeInSchedules.isUsing {
                let scheduleList = checkForUsingShiftTypeInSchedules.scheduleList
                self.delegate?.presentShiftTypeAlert(with: .deleteShiftType,
                                                     scheduleList: scheduleList)
            } else {
                self.delegate?.deleteShiftType(shiftTypes[indexPath.row])
                NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
            }
        }
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    public func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let tableView = tableView as? BaseSettingsDataTable,
            let shiftTypes = self.shiftTypes else {
                return UISwipeActionsConfiguration(actions: [])
        }
        
        let editing = tableView.editingAction(at: indexPath) {
            UserDefaults.dataIsBeingEdited = true
            self.delegate?.presentAddShiftTypeScene(for: shiftTypes[indexPath.row])
        }
        
         return UISwipeActionsConfiguration(actions: [editing])
    }
}

private extension  SettingsController.ShiftTypeEditingView {
    func checkForUsingShiftTypeInSchedules(for shiftType: ShiftType) -> (isUsing: Bool, scheduleList: [String]) {
        var isUsed = false
        var scheduleList: [String] = []
        guard let teamSchedules = teamSchedules else { return (isUsed, scheduleList) }
        teamSchedules.forEach { teamSchedule in
            teamSchedule.personalSchedules.forEach { personalSchedule in
                personalSchedule.shifts.forEach { shift in
                    if shift.shiftTypeName == shiftType.shiftTypeName {
                        isUsed = true
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MMMM yyyy"
                        let scheduleDate = dateFormatter.string(from: teamSchedule.scheduleDate)
                        
                        let schedule = teamSchedule.scheduleName + scheduleDate
                        if !scheduleList.contains(schedule) {
                            scheduleList.append(schedule)
                        }
                    }
                }
            }
        }
        
        return (isUsed, scheduleList)
    }
}
