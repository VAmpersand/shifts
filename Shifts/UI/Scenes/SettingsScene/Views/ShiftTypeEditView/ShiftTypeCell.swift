import UIKit

extension SettingsController.ShiftTypeEditingView {
    public class ShiftTypeCell: BaseSettingsDataCell {
        public static let cellID = String(describing: ShiftTypeCell.self)
        
        override public func layoutSubviews() {
            super.layoutSubviews()
            
            shiftTypeColorView.layer.cornerRadius = shiftTypeColorView.frame.height / 2
        }
        
        private lazy var shiftTypeNameLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            
            return label
        }()
        
        private lazy var shiftTimeLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            label.font = UIFont.systemFont(ofSize: 12)
            
            return label
        }()
        
        private lazy var shiftTypeColorView: UIView = {
            let view = UIView()
            view.backgroundColor = .green
            
            return view
        }()
    }
}

extension SettingsController.ShiftTypeEditingView.ShiftTypeCell {
    override func setupSelf() {
        super.setupSelf()
        addSubviews()
        constraintSubviews()
    }
    
    override func addSubviews() {
        super.addSubviews()
        addSubview(shiftTypeNameLabel)
        addSubview(shiftTimeLabel)
        addSubview(shiftTypeColorView)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        shiftTypeNameLabel.snp.makeConstraints { make in
            make.left.top.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.right.equalTo(shiftTypeColorView.snp.left).offset(-Constants.cgFloat.p10.rawValue)
        }
        
        shiftTimeLabel.snp.makeConstraints { make in
            make.top.equalTo(shiftTypeNameLabel.snp.bottom)
            make.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.right.left.equalTo(shiftTypeNameLabel)
        }
        
        shiftTypeColorView.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.centerY.equalToSuperview()
            make.height.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.width.equalTo(shiftTypeColorView.snp.height)
        }
    }
}

extension SettingsController.ShiftTypeEditingView.ShiftTypeCell {
    func setShiftTypeData(shiftType: ShiftType!) {
        guard let shiftType = shiftType else { return }
        shiftTypeNameLabel.text = shiftType.shiftTypeName
        shiftTimeLabel.text = Texts.AddShiftType.DefaultShiftTypeCell.shiftTime(startTime: shiftType.startTime,
                                                                                endTime: shiftType.endTime)
        shiftTypeColorView.backgroundColor = Colors.shiftTypeColors[shiftType.shiftColorIndex]
    }
}
