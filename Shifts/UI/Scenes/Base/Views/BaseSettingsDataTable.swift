import UIKit

public final class BaseSettingsDataTable: UITableView {
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        setupSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let editIcon = Icons.Settings.edit.withRenderingMode(.alwaysTemplate)
    private let deleteIcon = Icons.Settings.delete.withRenderingMode(.alwaysTemplate)
}

private extension BaseSettingsDataTable {
    func setupSelf() {
        showsVerticalScrollIndicator = false
        translatesAutoresizingMaskIntoConstraints = false
        decelerationRate = .fast
        backgroundColor = .clear
        layer.cornerRadius = Constants.cgFloat.p15.rawValue
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 1
        separatorStyle = .none
    }
}

public extension BaseSettingsDataTable {
    func editingAction(at indexPath: IndexPath, actionHendler: @escaping () -> Void) -> UIContextualAction {
        let button = UIContextualAction(style: .normal, title: "Editing") { (_, action, completion) in
            actionHendler()
            completion(true)
        }
        button.backgroundColor = .white
        button.image = Icons.Settings.edit.withTintColor(#colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1).withAlphaComponent(0.5),
                                                         renderingMode: .automatic)
        
        return button
    }
    
    func deleteAction(at indexPath: IndexPath, actionHendler: @escaping () -> Void) -> UIContextualAction {
        let button = UIContextualAction(style: .destructive , title: "Delete") { (_, action, completion) in
            actionHendler()
            completion(true)
        }
        button.backgroundColor = .white
        button.image = Icons.Settings.delete.withTintColor(#colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1).withAlphaComponent(0.8),
                                                           renderingMode: .automatic)
        
        return button
    }
}

