import UIKit

public class BaseSettingsDataCell: UITableViewCell {
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSelf()
        addLongTapGesture()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public var onLongTap: (() -> Void)?
    
    public lazy var separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1).withAlphaComponent(0.2)
        
        return view
    }()
}

extension BaseSettingsDataCell {
    func addLongTapGesture() {
        self.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(handleLongTap)))
    }

    @objc func handleLongTap(_ gesture: UILongPressGestureRecognizer) {
        guard gesture.state == .began else { return }

        onLongTap?()
    }
}

extension BaseSettingsDataCell {
    @objc func setupSelf() {
        addSubviews()
        constraintSubviews()
        
        selectionStyle = .none
    }
    
    @objc func addSubviews() {
        addSubview(separatorView)
    }
    
    @objc func constraintSubviews() {
        separatorView.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p15.rawValue)
            make.height.equalTo(Constants.cgFloat.p1.rawValue)
            make.bottom.equalToSuperview()
        }
    }
}
