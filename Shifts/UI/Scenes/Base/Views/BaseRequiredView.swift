import UIKit

public final class BaseRequiredView: UIView {
    public init() {
        super.init(frame: .zero)

        setupSelf()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension BaseRequiredView {
    func setupSelf() {
        backgroundColor = .white
        layer.cornerRadius = Constants.cgFloat.p15.rawValue
        layer.borderWidth = Constants.cgFloat.p2.rawValue
        layer.borderColor = UIColor.red.cgColor
        alpha = 0
    }
}

public extension BaseRequiredView {
    func unimateRequiredFields() {
           UIView.animate(withDuration: 0.4) {
               self.alpha = 1
           }
           DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
               UIView.animate(withDuration: 0.4) {
                   self.alpha = 0
               }
           }
       }
}
