import UIKit

public enum MenuCellType {
    case addEmployees
    case addShiftType
    case addTeamSchedule
}

public class BaseSettingsMenuCell: UITableViewCell {
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public lazy var container = UIView()
        
    public var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        
        return label
    }()
    
    public var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        
        return label
    }()
    
    public lazy var addButton: UIButton = {
        let button = UIButton()
        button.setTitle(Texts.Settings.addButton, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1).withAlphaComponent(0.3)
        button.layer.cornerRadius = 10
        
        return button
    }()
}

extension BaseSettingsMenuCell {
    @objc func setupSelf() {
        addSubviews()
        constraintSubviews()
        
        container.layer.cornerRadius = 15
        container.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1).withAlphaComponent(0.2)
        container.clipsToBounds = true
        selectionStyle = .none
    }
    
    @objc func addSubviews() {
        addSubview(container)
        container.addSubview(titleLabel)
        container.addSubview(subtitleLabel)
        container.addSubview(addButton)
    }
    
    @objc func constraintSubviews() {
        container.snp.makeConstraints { make in
            make.left.right.top.equalToSuperview()
            make.bottom.equalToSuperview().inset(Constants.cgFloat.p4.rawValue)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalTo(container.snp.top).offset(Constants.cgFloat.p23.rawValue)
            make.centerX.equalToSuperview()
        }
        
        subtitleLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.right.equalToSuperview().inset(Constants.cgFloat.p100.rawValue)
            make.centerY.equalTo(addButton)
        }
        
        addButton.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(Constants.cgFloat.p50.rawValue)
            make.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(Constants.cgFloat.p35.rawValue)
            make.width.equalTo(Constants.cgFloat.p60.rawValue)
        }
    }
}

extension BaseSettingsMenuCell {
    @objc func animateCell() {
        UIView.animate(withDuration: 0.5,
                       delay: 0.3,
                       usingSpringWithDamping: 0.8,
                       initialSpringVelocity: 1,
                       options: .curveEaseIn,
                       animations: { self.layoutIfNeeded() })
    }
    
    public func setCellData(type: MenuCellType) {
        let data = Texts.Settings.setMenuCellData(type: type)
        titleLabel.text = data.title
        subtitleLabel.text = data.subtitle
    }
}
