import UIKit

public final class WhiteTextField: UITextField {
    public init() {
        super.init(frame: .zero)

        setupSelf()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func addTouchUpInsideTarget(_ target: Any?, action: Selector) {
        addTarget(target, action: action, for: .touchUpInside)
    }

    override public func layoutSubviews() {
        super.layoutSubviews()

        layer.cornerRadius = frame.height / 2
        updateAllSublayers()
    }
}

private extension WhiteTextField {
    func setupSelf() {
        backgroundColor = .white
        borderStyle = .none
        returnKeyType = .next
        textColor = Colors.darkBlue
        clipsToBounds = true
        layer.sublayerTransform = CATransform3DMakeTranslation(28, 0, 0)

        if let placeholderLabel = value(forKey: "placeholderLabel") as? UILabel {
            placeholderLabel.textColor = UIColor.lightGray.withAlphaComponent(0.6)
        }
    }
}

