import UIKit

protocol PopUpViewDelegate: class {
    func popupViewDidPressButton(view: PopUpView)
}

class PopUpView: UIView {

    weak var delegate: PopUpViewDelegate?

    var image: UIImage? {
        get {
            imageView.image
        }
        set {
            imageView.image = newValue
        }
    }

    var text: String? {
        get {
            label.text
        }
        set {
            label.text = newValue
        }
    }

    var buttonImage: UIImage? {
        get {
            button.image(for: .normal)
        }

        set {
            button.setImage(newValue, for: .normal)
        }
    }

    private let padding: CGFloat = 20

    private var imageView: UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()

    private var label: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 15, weight: .light)
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()

    private let button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(didPressButton), for: .touchUpInside)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupSelf()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension PopUpView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        backgroundColor = Colors.darkBlue
        layer.cornerRadius = 25
    }

    func addSubviews() {
        addSubview(button)
        addSubview(label)
        addSubview(imageView)
    }

    func constraintSubviews() {
        imageView.snp.makeConstraints { make in
            make.left.equalTo(snp.left).inset(padding)
            make.height.equalTo(50)
            make.width.equalTo(40)
            make.centerY.equalTo(snp.centerY)
        }
        button.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.centerY.equalTo(snp.centerY)
            make.right.equalTo(snp.right).inset(padding)
        }
        label.snp.makeConstraints { make in
            make.left.equalTo(imageView.snp.right).offset(padding / 2)
            make.right.equalTo(button.snp.left).inset(-padding / 2)
            make.height.equalTo(snp.height)
        }
    }
}

@objc
private extension PopUpView {
    func didPressButton(sender: UIButton) {
        delegate?.popupViewDidPressButton(view: self)
    }
}
