import UIKit

public final class NavigationCloseButton: UIButton {
    override public init(frame: CGRect) {
        super.init(frame: frame)

        setupSelf()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func addTouchUpInsideTarget(_ target: Any?, action: Selector) {
        addTarget(target, action: action, for: .touchUpInside)
    }
}

private extension NavigationCloseButton {
    func setupSelf() {
        setImage(Icons.Common.close, for: .normal)
        tintColor = .white
    }
}
