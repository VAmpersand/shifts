//import UIKit
//
//public final class BottomActionButton: UIButton {
//    public init() {
//        super.init(frame: .zero)
//
//        setupSelf()
//    }
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    func addTouchUpInsideTarget(_ target: Any?, action: Selector) {
//        addTarget(target, action: action, for: .touchUpInside)
//    }
//
//    override public func layoutSubviews() {
//        super.layoutSubviews()
//
//        layer.cornerRadius = frame.height / 2
//        updateAllSublayers()
//    }
//}
//
//private extension BottomActionButton {
//    func setupSelf() {
//        titleLabel?.font = Fonts.robotoRegular(of: 18)
//        tintColor = .white
//        clipsToBounds = true
//        titleLabel?.textAlignment = .center
//        contentEdgeInsets = .init(top: 0, left: 20, bottom: 0, right: 20)
//        backgroundColor = UIColor.white.withAlphaComponent(0.3)
//
//        makeSystem()
//    }
//}
