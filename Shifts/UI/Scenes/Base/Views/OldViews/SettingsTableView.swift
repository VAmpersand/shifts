import UIKit

public final class SettingsTableView: UITableView {
    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        setupSelf()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

//    func addTouchUpInsideTarget(_ target: Any?, action: Selector) {
//        addTarget(target, action: action, for: .touchUpInside)
//    }

//    override public func layoutSubviews() {
//        super.layoutSubviews()
//
//        layer.cornerRadius = frame.height / 2
//        updateAllSublayers()
//    }
}

private extension SettingsTableView {
    func setupSelf() {
        backgroundColor = .white
        separatorStyle = .none
//        returnKeyType = .next
//        font = Fonts.regular(of: 20)
//        textColor = Colors.darkBlue
//        clipsToBounds = true
//        layer.sublayerTransform = CATransform3DMakeTranslation(28, 0, 0)
//
//        if let placeholderLabel = value(forKey: "placeholderLabel") as? UILabel {
//            placeholderLabel.textColor = UIColor.lightGray.withAlphaComponent(0.6)
//        }
    }
}

