import UIKit

public final class BaseTextField: UITextField {
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

private extension BaseTextField {
    func setupSelf() {
        layer.cornerRadius = 15
        attributedPlaceholder = NSAttributedString(
            string: "placeholder text",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray.withAlphaComponent(0.6)]
        )
        layer.sublayerTransform = CATransform3DMakeTranslation(15, 0, 0)
        textAlignment = .left
    }
}
