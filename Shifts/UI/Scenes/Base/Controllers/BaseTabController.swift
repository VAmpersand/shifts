import UIKit

public class BaseTabController: UITabBarController {

}

// MARK: - Themable
extension BaseTabController: Themable {
    @objc public func applyGenericTheme() {

    }

    public func applyLightTheme() {
        applyGenericTheme()
    }
    public func applyDarkTheme() {
        applyGenericTheme()
    }
}

extension UIView {
func roundedView() {
    let maskPath1 = UIBezierPath(roundedRect: bounds,
                                 byRoundingCorners: [.topLeft, .topRight],
                                 cornerRadii: CGSize(width: 30, height: 30))
    let maskLayer1 = CAShapeLayer()
    maskLayer1.frame = bounds
    maskLayer1.path = maskPath1.cgPath
    layer.mask = maskLayer1
  }
}
