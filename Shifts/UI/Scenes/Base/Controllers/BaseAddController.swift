import UIKit

public class BaseAddController: UIViewController {
    override public func viewDidLoad() {
        super.viewDidLoad()

        setupSelf()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setRightButtonTitle()
    }
    
    lazy var blurView: UIView = {
        let view = UIView(frame: UIScreen.main.bounds)
        let blurEffect = UIBlurEffect(style: .systemUltraThinMaterialDark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.frame
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        
        return view
    }()
    
    lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).withAlphaComponent(0.8)
        view.layer.cornerRadius = 15

        return view
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.textColor = .white

        return label
    }()

    lazy var createButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleCreateButton), for: .touchUpInside)
        button.setTitle(Texts.create, for: .normal)
        
        return button
    }()

    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(handleCancelButton), for: .touchUpInside)
        button.setTitle(Texts.cancel, for: .normal)

        return button
    }()
}

extension BaseAddController {
    @objc func setupSelf() {
        addSubviews()
        constraintSubviews()
        addGestureRecogniserForEndEditing(view: view)
    }

    @objc func addSubviews() {
        view.addSubview(blurView)
        blurView.addSubview(contentView)
        blurView.addSubview(titleLabel)
        blurView.addSubview(createButton)
        blurView.addSubview(cancelButton)
    }

    @objc func constraintSubviews() {
        contentView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.width.equalToSuperview().inset(Constants.cgFloat.p15.rawValue)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p15.rawValue)
            make.bottom.equalTo(contentView.snp.top).offset(-Constants.cgFloat.p20.rawValue)
        }
        
        createButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(24)
            make.right.equalTo(view.safeAreaLayoutGuide).inset(20)
            make.height.equalTo(32)
        }

        cancelButton.snp.makeConstraints { make in
            make.bottom.equalTo(view.safeAreaLayoutGuide).inset(24)
            make.left.equalTo(view.safeAreaLayoutGuide).inset(20)
            make.height.equalTo(32)
        }
    }
}

extension BaseAddController {
    @objc func handleCancelButton() { }

    @objc func handleCreateButton() { }
    
    func addGestureRecogniserForEndEditing(view: UIView) {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTouch))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func handleTouch() {
        view.endEditing(true)
    }
}

extension BaseAddController {
    func setRightButtonTitle() {
        let text = UserDefaults.dataIsBeingEdited ? Texts.save : Texts.create
        createButton.setTitle(text, for: .normal)
    }
}
