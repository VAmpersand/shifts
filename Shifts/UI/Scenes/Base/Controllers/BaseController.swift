import UIKit

public class BaseController: UIViewController {
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        let theme = Theme.current
        return theme == .light ? .darkContent : .lightContent
    }

    override public func viewDidLoad() {
        super.viewDidLoad()

        setupSelf() 
        applyTheme()
    }
    
    public var titleView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 15
        view.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1).withAlphaComponent(0.2)
        
        return view
    }()
    
    public var titleLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(20)
        label.textAlignment = .center
        
        return label
    }()
}

extension BaseController {
    @objc func setupSelf() {
        addSubviews()
        constraintSubviews()
        
        view.backgroundColor = .white
    }
    
    @objc func addSubviews() {
        view.addSubview(titleView)
        titleView.addSubview(titleLabel)
    }
    
    @objc func constraintSubviews() {
        titleView.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).inset(Constants.cgFloat.p10.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(50)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
//    
//        @discardableResult
//    func addStaticNavigationBar(_ title: String) -> (navigationBar: StaticNavigationBar,
//                                                     leftButton: UIButton?,
//                                                     rightButton: UIButton?) {
//        let bar = StaticNavigationBar(title: title)
//        return addStaticNavigationBar(bar)
//    }
//
//    @discardableResult
//    func addStaticNavigationBar(_ bar: StaticNavigationBar) -> (
//        navigationBar: StaticNavigationBar,
//        leftButton: UIButton?,
//        rightButton: UIButton?
//        ) {
//            view.addSubview(bar)
//
//        bar.snp.makeConstraints { make in
//            make.right.left.equalToSuperview()
//            make.top.equalTo(view.safeAreaLayoutGuide)
//        }
//
//        return (bar, bar.leftButton, bar.rightButton)
//    }
}

// MARK: - Themable
extension BaseController: Themable {
    @objc public func applyGenericTheme() {
        setNeedsStatusBarAppearanceUpdate()
    }

    public func applyLightTheme() {
        applyGenericTheme()
    }
    public func applyDarkTheme() {
        applyGenericTheme()
    }
}
