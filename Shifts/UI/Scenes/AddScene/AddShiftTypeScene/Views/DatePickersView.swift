import UIKit

extension AddShiftTypeController {
    public class DatePickersView: UIView {
        init() {
            super.init(frame: .zero)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private lazy var startPickerContainer: UIView = {
            let view = UIView()
            
            return view
        }()
        
        private lazy var fromLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            label.text = Texts.AddShiftType.fromLable
            
            return label
        }()
        
        public lazy var startTimePickerView: UIDatePicker = {
            let picker = UIDatePicker()
            picker.datePickerMode = .time
            picker.timeZone = .current
            picker.minuteInterval = .init(15)
            picker.setValue(UIColor.black, forKeyPath: "textColor")
            
            return picker
        }()
        
        private lazy var endPickerContainer: UIView = {
            let view = UIView()

            return view
        }()
        
        private lazy var toLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .left
            label.text = Texts.AddShiftType.toLable
            
            return label
        }()
        
        public lazy var endTimePickerView: UIDatePicker = {
            let picker = UIDatePicker()
            picker.datePickerMode = .time
            picker.timeZone = .current
            picker.minuteInterval = .init(15)
            picker.setValue(UIColor.black, forKeyPath: "textColor")
            
            return picker
        }()
    }
}

//extension PersonalScheduleController.CalendarHeaderView: LayerEffectable {
//    public func applyEffects() {
//
//    }
//}

private extension AddShiftTypeController.DatePickersView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
    }
    
    func addSubviews() {
        addSubview(startPickerContainer)
        startPickerContainer.addSubview(fromLabel)
        startPickerContainer.addSubview(startTimePickerView)
        addSubview(endPickerContainer)
        endPickerContainer.addSubview(toLabel)
        endPickerContainer.addSubview(endTimePickerView)
    }
    
    func constraintSubviews() {
        startPickerContainer.snp.makeConstraints { make in
            make.left.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
        }
        
        fromLabel.snp.makeConstraints { make in
            make.top.left.equalToSuperview().inset(10)
        }
        
        startTimePickerView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(startTimePickerView.snp.width)
            make.top.equalTo(fromLabel.snp.bottom)
            make.bottom.equalToSuperview()
        }
        
        endPickerContainer.snp.makeConstraints { make in
            make.right.top.bottom.equalToSuperview()
            make.width.equalToSuperview().multipliedBy(0.5)
        }
        
        toLabel.snp.makeConstraints { make in
            make.top.left.equalToSuperview().inset(10)
        }
        
        endTimePickerView.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(endTimePickerView.snp.width)
            make.top.equalTo(toLabel.snp.bottom)
            make.bottom.equalToSuperview()
        }
    }
}

