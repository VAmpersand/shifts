import UIKit

extension AddShiftTypeController {
    public class ColorsView: UICollectionViewCell {
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private let cellID = String(describing: AddShiftTypeController.ColorViewCell.self)
        public var prevSelectedColorIndex: IndexPath!
        public var selectedColorIndex: IndexPath!
        private var usedColor: [Bool] = []
        
        public lazy var colorsView: UICollectionView = {
            let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
            layout.minimumLineSpacing = 1
            layout.minimumInteritemSpacing = 1
            layout.scrollDirection = .vertical
            
            let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            collectionView.showsVerticalScrollIndicator = false
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            collectionView.register(ColorViewCell.self,
                                    forCellWithReuseIdentifier: cellID)
            collectionView.decelerationRate = .fast
            collectionView.backgroundColor = .clear
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.isScrollEnabled = false
            
            return collectionView
        }()
    }
}

private extension AddShiftTypeController.ColorsView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        getUsedColor()
    }
    
    func addSubviews() {
        addSubview(colorsView)
    }
    
    func constraintSubviews() {
        colorsView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: - UICollectionViewDataSource
extension AddShiftTypeController.ColorsView: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Colors.shiftTypeColors.count - 1
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID,
                                                      for: indexPath) as! AddShiftTypeController.ColorViewCell
        
        cell.colorView.backgroundColor = Colors.shiftTypeColors[indexPath.row]
        if usedColor[indexPath.row] {
            markSelectedColors(at: cell, indexPath: indexPath)
        }
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! AddShiftTypeController.ColorViewCell
        
        if !usedColor[indexPath.row] {
            selectedColorIndex = indexPath
            selectColors(at: cell)
        }
        deselectPrevColor(at: selectedColorIndex)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension AddShiftTypeController.ColorsView: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 6 - 2
        return CGSize(width: width, height: width)
    }
}

extension AddShiftTypeController.ColorsView {
    public func markSelectedColors(at cell: UICollectionViewCell, indexPath: IndexPath) {
        guard let cell = cell as? AddShiftTypeController.ColorViewCell else { return }
        selectColors(at: cell)
        guard let prevIndexPath = prevSelectedColorIndex else {
            cell.colorView.alpha = 0.1
            return
        }
        if prevIndexPath != indexPath {
            cell.colorView.alpha = 0.1
        }
    }
    
    public func selectColors(at cell: UICollectionViewCell) {
        guard let cell = cell as? AddShiftTypeController.ColorViewCell else { return }
        cell.colorView.snp.remakeConstraints { make in
            make.edges.equalToSuperview()
            make.center.equalToSuperview()
        }
    }
    
    public func deselectColor(at indexPath: IndexPath) {
        guard let cell = colorsView.cellForItem(at: indexPath) as? AddShiftTypeController.ColorViewCell else { return }
        cell.colorView.snp.remakeConstraints { make in
            make.edges.equalToSuperview().inset(5)
            make.center.equalToSuperview()
        }
        cell.colorView.alpha = 1
    }
    
    public func deselectPrevColor(at indexPath: IndexPath) {
        guard let prevIndexPath = prevSelectedColorIndex else {
            prevSelectedColorIndex = indexPath
            return
        }
        
        usedColor[prevIndexPath.row] = false
        if prevIndexPath != indexPath {
            deselectColor(at: prevIndexPath)
            prevSelectedColorIndex = indexPath
        }
    }
    
    func getUsedColor() {
        let shiftTypes = getShiftTypes()
        for _ in 1...18 {
            usedColor.append(false)
        }
        shiftTypes.forEach {
            if $0.shiftTypeName != Texts.AddShiftType.defaultShiftTypeName {
                usedColor[$0.shiftColorIndex] = true
            }
        }
    }
}

private extension AddShiftTypeController.ColorsView {
    func getShiftTypes() -> [ShiftType] {
        let shiftTypeStorageService = ShiftTypeStorageService()
        shiftTypeStorageService.loadShiftType()
        let shiftType =  shiftTypeStorageService.shiftTypes.value
        
        return shiftType
    }
}
