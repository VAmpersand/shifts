import UIKit

extension AddShiftTypeController{
    public class ColorViewCell: UICollectionViewCell {
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override public func layoutSubviews() {
            super.layoutSubviews()
            
            colorView.layer.cornerRadius = colorView.frame.width / 2
        }
        
        public lazy var colorView = UIView()
    }
}

private extension AddShiftTypeController.ColorViewCell {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
    }
    
    func addSubviews() {
        addSubview(colorView)
    }
    
    func constraintSubviews() {
        colorView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(5)
            make.center.equalToSuperview()
        }
    }
}

