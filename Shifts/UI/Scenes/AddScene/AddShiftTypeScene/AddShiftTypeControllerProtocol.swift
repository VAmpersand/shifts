public protocol AddShiftTypeControllerProtocol: class {
    func setShiftTypeData(shiftType: ShiftType)
    func setShiftTypesData(shiftTypes: [ShiftType])
}
