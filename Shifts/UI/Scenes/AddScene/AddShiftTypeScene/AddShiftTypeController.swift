import UIKit

public final class AddShiftTypeController: BaseAddController {
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidLoad()
        setupSelf()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    public var viewModel: AddShiftTypeViewModelProtocol!
    private var shiftTypes: [ShiftType]!
    private var shiftType: ShiftType!

    private lazy var shiftTypeLabel: UILabel = {
           let label = UILabel()
           label.textAlignment = .left
           label.text = Texts.AddShiftType.shiftTypeLable
           
           return label
       }()
    
    private lazy var shiftTypeNameContainer = BaseRequiredView()
    
    public lazy var shiftTypeNameField: UITextField = {
        let textField = BaseTextField()
        textField.placeholder = Texts.AddShiftType.shiftTypePlaceholder
        
        return textField
    }()
    
    private lazy var datePickersView = DatePickersView()
    
    private lazy var colorsLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.text = Texts.AddShiftType.colorsLable
        
        return label
    }()
    
    private lazy var colorsViewContainer = BaseRequiredView()
    private lazy var colorsView = ColorsView()
}

extension AddShiftTypeController {
    override func setupSelf() {
        super.setupSelf()
        addGestureRecogniserForEndEditing(view: contentView)
        
        titleLabel.text = Texts.AddShiftType.title
        
    }
    
    override func addSubviews() {
        super.addSubviews()
        contentView.addSubview(shiftTypeLabel)
        contentView.addSubview(shiftTypeNameContainer)
        contentView.addSubview(shiftTypeNameField)
        contentView.addSubview(datePickersView)
        contentView.addSubview(colorsLabel)
        contentView.addSubview(colorsViewContainer)
        contentView.addSubview(colorsView)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        shiftTypeLabel.snp.makeConstraints { make in
           make.left.right.top.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
        
        shiftTypeNameContainer.snp.makeConstraints { make in
            make.top.equalTo(shiftTypeLabel.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(50)
        }
        
        shiftTypeNameField.snp.makeConstraints { make in
            make.edges.equalTo(shiftTypeNameContainer)
        }
        
        datePickersView.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.top.equalTo(shiftTypeNameField.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
        }
        
        colorsLabel.snp.makeConstraints { make in
            make.top.equalTo(datePickersView.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.equalToSuperview().offset(Constants.cgFloat.p10.rawValue)
        }
        
        colorsViewContainer.snp.makeConstraints { make in
            make.top.equalTo(colorsLabel.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p20.rawValue)
            make.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(colorsViewContainer.snp.width).multipliedBy(0.5)
        }
        
        colorsView.snp.makeConstraints { make in
            make.edges.equalTo(colorsViewContainer)
        }
    }
}

// MARK: - AddShiftTypeControllerProtocol
extension AddShiftTypeController: AddShiftTypeControllerProtocol {
    public func setShiftTypeData(shiftType: ShiftType) {
        shiftTypeNameField.text = shiftType.shiftTypeName
        guard let startTime = shiftType.startTime.roundedTimeStr else { return }
        datePickersView.startTimePickerView.setDate(startTime, animated: false)
        guard let endTime = shiftType.endTime.roundedTimeStr else { return }
        datePickersView.endTimePickerView.setDate(endTime, animated: false)
        colorsView.prevSelectedColorIndex = IndexPath(row: shiftType.shiftColorIndex,
                                                        section: 0)

        self.shiftType = shiftType
    }
    
    public func setShiftTypesData(shiftTypes: [ShiftType]) {
        self.shiftTypes = shiftTypes
    }
}

extension AddShiftTypeController {
    override func handleCancelButton() {
        viewModel.handleClose()
    }
    
    override func handleCreateButton() {
        let newShiftType = ShiftType()
        guard let shiftTypeName = shiftTypeNameField.text, !shiftTypeName.isEmpty,
            let selectedColorIndex = colorsView.selectedColorIndex else {
                shiftTypeNameContainer.unimateRequiredFields()
                colorsViewContainer.unimateRequiredFields()
                return
        }
        
        newShiftType.shiftTypeName = shiftTypeName
        newShiftType.startTime = datePickersView.startTimePickerView.date.getRoundedTime()
        newShiftType.endTime = datePickersView.endTimePickerView.date.getRoundedTime()
        newShiftType.shiftDuration = getWorkingTime(startTime: newShiftType.startTime,
                                                  endTime: newShiftType.endTime)
        newShiftType.shiftColorIndex = selectedColorIndex.row
        
        
        var shiftTypeDataIsMatch = false
        shiftTypes.forEach { shiftType in
            if shiftType.shiftTypeName == newShiftType.shiftTypeName {
                shiftTypeDataIsMatch = true
            }
        }
        
        if UserDefaults.dataIsBeingEdited, let shiftType = shiftType {
            newShiftType.shiftColorIndex = shiftType.shiftColorIndex
            viewModel.updateShifType(shiftType: shiftType, newShiftType: newShiftType)
        } else if shiftTypeDataIsMatch {
            viewModel.presentPopupAlertScene(alertType: .shiftTypeDataMatch,
                                             scheduleList: [])
        } else {
            viewModel.createShiftType(shiftType: newShiftType)
        }
        NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
    }
}


private extension AddShiftTypeController {
    func getWorkingTime(startTime: String, endTime: String) -> Double {
        guard let startTime = startTime.roundedTimeStr else { return 0.0 }
        guard let endTime = endTime.roundedTimeStr else { return 0.0 }
        guard let workingTime = Calendar.current.dateComponents( [.hour],
                                                                 from: startTime,
                                                                 to: endTime ).hour else { return 0.0 }
        
        return Double(workingTime)
    }
}
