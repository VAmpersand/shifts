import UIKit

public final class AddEmployeeController: BaseAddController {
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidLoad()
        setupSelf()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    public var viewModel: AddEmployeeViewModelProtocol!
    
    private lazy var adminRights = false
    private var employees: [Employee]!
    private var employee: Employee!

    public lazy var firstNameContainer = BaseRequiredView()
    
    public lazy var firstNameField: UITextField = {
        let textField = BaseTextField()
        textField.placeholder = Texts.AddEmployee.DefaultEmployeeCell.firstName.rawValue

        return textField
    }()
    
    public lazy var lastNameContainer = BaseRequiredView()

    public lazy var lastNameField: UITextField = {
        let textField = BaseTextField()
        textField.placeholder = Texts.AddEmployee.DefaultEmployeeCell.lastName.rawValue

        return textField
    }()
    
    public lazy var patronymicContainer = BaseRequiredView()
    
    public lazy var patronymicField: UITextField = {
        let textField = BaseTextField()
        textField.placeholder = Texts.AddEmployee.DefaultEmployeeCell.patronymic.rawValue

        return textField
    }()

    public lazy var positionField: UITextField = {
        let textField = BaseTextField()
        textField.placeholder = Texts.AddEmployee.DefaultEmployeeCell.position.rawValue

        return textField
    }()

    private lazy var adminLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .left
        label.text = Texts.AddEmployee.DefaultEmployeeCell.adminRights.rawValue

        return label
    }()

    public lazy var adminRightsSwitch: UISwitch = {
        let adminSwitch = UISwitch()
        adminSwitch.addTarget(self, action: #selector(handleSwitch), for: .valueChanged)
        
        return adminSwitch
    }()
}

extension AddEmployeeController {
    override func setupSelf() {
        super.setupSelf()
        addGestureRecogniserForEndEditing(view: contentView)
        
        titleLabel.text = Texts.AddEmployee.title
    }
    
    override func addSubviews() {
        super.addSubviews()
        contentView.addSubview(firstNameContainer)
        contentView.addSubview(firstNameField)
        contentView.addSubview(lastNameContainer)
        contentView.addSubview(lastNameField)
        contentView.addSubview(patronymicContainer)
        contentView.addSubview(patronymicField)
        contentView.addSubview(positionField)
        contentView.addSubview(adminLabel)
        contentView.addSubview(adminRightsSwitch)
    }

    override func constraintSubviews() {
        super.constraintSubviews()
        lastNameContainer.snp.makeConstraints { make in
            make.top.right.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(Constants.cgFloat.p50.rawValue)
        }
        
        lastNameField.snp.makeConstraints { make in
            make.edges.equalTo(lastNameContainer)
        }
        
        firstNameContainer.snp.makeConstraints { make in
            make.top.equalTo(lastNameContainer.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.right.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(lastNameField)
        }
        
        firstNameField.snp.makeConstraints { make in
            make.edges.equalTo(firstNameContainer)
        }

        patronymicContainer.snp.makeConstraints { make in
            make.top.equalTo(firstNameContainer.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.right.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(lastNameField)
        }
        
        patronymicField.snp.makeConstraints { make in
            make.edges.equalTo(patronymicContainer)
        }

        positionField.snp.makeConstraints { make in
            make.top.equalTo(patronymicContainer.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.right.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(lastNameField)
        }

        adminLabel.snp.makeConstraints { make in
            make.top.equalTo(positionField.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.right.equalTo(adminRightsSwitch.snp.left).offset(-Constants.cgFloat.p10.rawValue)
            make.height.equalTo(lastNameField)
        }

        adminRightsSwitch.snp.makeConstraints { make in
            make.centerY.equalTo(adminLabel)
            make.right.equalToSuperview().offset(-Constants.cgFloat.p10.rawValue)
        }
    }
}

// MARK: - AddEmployeeControllerProtocol
extension AddEmployeeController: AddEmployeeControllerProtocol {
    public func setEmployeeData(employee: Employee) {
        firstNameField.text = employee.firstName
        lastNameField.text = employee.lastName
        patronymicField.text = employee.patronymic
        positionField.text = employee.position
        adminRights = employee.adminRights
        adminRightsSwitchTogle(adminRights: adminRights)
        self.employee = employee
    }
    
    public func setEmployeesData(employees: [Employee]) {
        self.employees = employees
    }
    
}

// MARK: - Actions
extension AddEmployeeController {
    override func handleCancelButton() {
        viewModel.handleClose()
    }
    
    override func handleCreateButton() {
        let newEmployee = Employee()
        guard let firstName = firstNameField.text, !firstName.isEmpty,
            let lastName = lastNameField.text, !lastName.isEmpty,
            let patronymic = patronymicField.text, !patronymic.isEmpty else {
                firstNameContainer.unimateRequiredFields()
                lastNameContainer.unimateRequiredFields()
                patronymicContainer.unimateRequiredFields()
                return
        }
        newEmployee.firstName = firstName
        newEmployee.lastName = lastName
        newEmployee.patronymic = patronymic
        if let position = positionField.text, !position.isEmpty {
            newEmployee.position = position
        }
        newEmployee.adminRights = adminRights
        
        var employeeDataIsMatch = false
        employees.forEach { employee in
            if employee.firstName == newEmployee.firstName
                && employee.lastName == newEmployee.lastName
                && employee.patronymic == newEmployee.patronymic
                && employee.position == newEmployee.position {
                employeeDataIsMatch = true
            }
        }
        guard let employee = self.employee else { return }
        if UserDefaults.dataIsBeingEdited {
            viewModel.updateEmployee(employee: employee, newEmployee: newEmployee)
        } else if employeeDataIsMatch {
            viewModel.presentPopupAlertScene(alertType: .employeeDataMatch,
                                             scheduleList: [])
        } else {
            viewModel.createEmployee(employee: newEmployee)
        }
        
        NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
    }
}

private extension AddEmployeeController {
    func adminRightsSwitchTogle(adminRights: Bool) {
        adminRightsSwitch.setOn(adminRights, animated: true)
    }
    
    @objc func handleSwitch() {
        adminRights.toggle()
    }
}
