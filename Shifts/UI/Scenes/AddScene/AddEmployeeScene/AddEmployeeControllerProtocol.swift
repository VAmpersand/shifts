public protocol AddEmployeeControllerProtocol: class {
    func setEmployeeData(employee: Employee)
    func setEmployeesData(employees: [Employee])
}

