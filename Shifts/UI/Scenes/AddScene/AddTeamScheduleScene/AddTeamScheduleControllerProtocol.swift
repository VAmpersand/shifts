public protocol AddTeamScheduleControllerProtocol: class {
    func setTeamSchedule(teamSchedule: TeamSchedule)
    func setTeamSchedules(teamSchedules: [TeamSchedule])
}
