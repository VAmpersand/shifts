import UIKit

extension AddTeamScheduleController {
    public class MonthSelectView: UIView {
        init() {
            super.init(frame: .zero)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        public var year = Calendar.current.component(.year, from: Date()) {
            didSet {
                if year == Calendar.current.component(.year, from: Date()) {
                    currentYearView.isChosen = true
                    nextYearView.isChosen = false
                } else {
                    currentYearView.isChosen = false
                    nextYearView.isChosen = true
                }
            }
        }
        
        public var month = Calendar.current.component(.month, from: Date()) - 1
        
        public lazy var currentYearView: MonthSelectCell = {
            let view = MonthSelectCell()
            view.contentLabel.text = "\(Calendar.current.component(.year, from: Date()))"
            view.isChosen = true

            return view
        }()

        public lazy var nextYearView: MonthSelectCell = {
            let view = MonthSelectCell()
            view.contentLabel.text = "\(Calendar.current.component(.year, from: Date()) + 1)"

            return view
        }()
        
        public lazy var monthView: UICollectionView = {
            let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            layout.minimumLineSpacing = 4
            layout.minimumInteritemSpacing = 4
            layout.scrollDirection = .vertical
            
            let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            collectionView.showsVerticalScrollIndicator = false
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            collectionView.register(MonthSelectCell.self,
                                    forCellWithReuseIdentifier: MonthSelectCell.cellID)
            collectionView.decelerationRate = .fast
            collectionView.backgroundColor = .clear
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.backgroundColor = .white
            collectionView.isScrollEnabled = false
            
            return collectionView
        }()
    }
}

//extension AddTeamShiftsController.MonthPickerView: LayerEffectable {
//    public func applyEffects() {
//
//    }
//}

private extension AddTeamScheduleController.MonthSelectView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        addGesture()
    }
    
    func addSubviews() {
        addSubview(monthView)
        addSubview(currentYearView)
        addSubview(nextYearView)
    }
    
    func constraintSubviews() {
        currentYearView.snp.makeConstraints { make in
            make.left.top.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.right.equalTo(snp.centerX).offset(-Constants.cgFloat.p5.rawValue)
            make.height.equalTo(Constants.cgFloat.p50.rawValue)
        }
        
        nextYearView.snp.makeConstraints { make in
            make.right.top.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.left.equalTo(currentYearView.snp.right).offset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(Constants.cgFloat.p50.rawValue)
        }
        
        monthView.snp.makeConstraints { make in
            make.top.equalTo(currentYearView.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.right.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
    }
}

// MARK: - UICollectionViewDataSource
extension AddTeamScheduleController.MonthSelectView: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Texts.months.count
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MonthSelectCell.cellID,
                                                      for: indexPath) as! MonthSelectCell
        cell.contentLabel.alpha = 1
        cell.borderView.isHidden = indexPath.row == month ? false : true
        
        cell.contentLabel.text = Texts.monthsReduct[indexPath.row]
        if currentYearView.isChosen,
            indexPath.row < Calendar.current.component(.month, from: Date()) - 1 {
            cell.contentLabel.alpha = 0.2
        } else if nextYearView.isChosen,
            indexPath.row > Calendar.current.component(.month, from: Date()) - 1 {
            cell.contentLabel.alpha = 0.2
        }
        
        return cell
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension AddTeamScheduleController.MonthSelectView: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 12) / 4
        return CGSize(width: width, height: Constants.cgFloat.p50.rawValue)
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MonthSelectCell
        
        if !UserDefaults.dataIsBeingEdited {
            collectionView.reloadData()
            if currentYearView.isChosen,
                indexPath.row >= Calendar.current.component(.month, from: Date()) - 1 {
                cell.borderView.isHidden = false
                month = indexPath.row
            } else if nextYearView.isChosen,
                indexPath.row <= Calendar.current.component(.month, from: Date()) - 1 {
                cell.borderView.isHidden = false
                month = indexPath.row
            }
        }
    }
}

extension AddTeamScheduleController.MonthSelectView {
    func addGesture() {
        currentYearView.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                                    action: #selector(currentYearViewPressed)))
        nextYearView.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                                 action: #selector(nextYearViewPressed)))
    }
    
    @objc func currentYearViewPressed() {
        if !UserDefaults.dataIsBeingEdited {
            currentYearView.isChosen = true
            nextYearView.isChosen = false
            year = Calendar.current.component(.year, from: Date())
            month = Calendar.current.component(.month, from: Date()) - 1
            monthView.reloadData()
        }
    }
    
    @objc func nextYearViewPressed() {
        if !UserDefaults.dataIsBeingEdited {
            currentYearView.isChosen = false
            nextYearView.isChosen = true
            year = Calendar.current.component(.year, from: Date()) + 1
            month = Calendar.current.component(.month, from: Date()) - 1
            monthView.reloadData()
        }
    }
}

extension AddTeamScheduleController.MonthSelectView {
    func setDate(_ data: Date) {
        self.year = Calendar.current.component(.year, from: data)
        self.month = Calendar.current.component(.month, from: data) - 1
    }
}
