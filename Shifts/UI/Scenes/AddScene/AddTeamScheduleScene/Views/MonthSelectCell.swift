import UIKit

extension AddTeamScheduleController.MonthSelectView {
    public class MonthSelectCell: UICollectionViewCell {
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        public static let cellID = String(describing: MonthSelectCell.self)
        
        public var isChosen: Bool = false {
            didSet {
                if self.isChosen {
                    self.borderView.isHidden = false
                } else {
                    self.borderView.isHidden = true
                }
            }
        }

        public lazy var contentLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .center
            
            return label
        }()
        
        public lazy var borderView: UIView = {
            let view = UIView()
            view.layer.borderWidth = 1
            view.layer.borderColor = UIColor.red.cgColor
            view.layer.cornerRadius = 10
            
            return view
        }()
    }
}

private extension AddTeamScheduleController.MonthSelectView.MonthSelectCell {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        
        borderView.isHidden = true
        backgroundColor = .white
    }
    
    func addSubviews() {
        addSubview(borderView)
        addSubview(contentLabel)
    }
    
    func constraintSubviews() {
        borderView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(Constants.cgFloat.p2.rawValue)
        }
        
        contentLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

