import UIKit

public final class AddTeamScheduleController: BaseAddController {
    override public func viewDidLoad() {
        super.viewDidLoad()

        viewModel.viewDidLoad()
        setupSelf()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    public var viewModel: AddTeamScheduleViewModelProtocol!
    private var teamSchedules: [TeamSchedule] = []
    private var teamSchedule: TeamSchedule!

    private lazy var workingTimeLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.numberOfLines = 2
        label.text = Texts.AddTeamSchedule.workingTime
        
        return label
    }()
    
    private lazy var workingTimeContainer = BaseRequiredView()
    
    private lazy var workingTimeField: UITextField = {
        let textField = BaseTextField()
        textField.placeholder =  Texts.AddTeamSchedule.workingTimePlaceholder
        
        return textField
    }()
    
    private lazy var scheduleMonthLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.text = Texts.AddTeamSchedule.monthPickerLable
        
        return label
    }()
    
    public lazy var scheduleNameField: UITextField = {
        let textField = BaseTextField()
        textField.placeholder = Texts.AddTeamSchedule.scheduleNamePlaceholder
        
        return textField
    }()
    
    private lazy var scheduleNameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .right
        label.text = Texts.AddTeamSchedule.scheduleNameSubtitle
        
        return label
    }()
    
    private lazy var monthSelectView = MonthSelectView()
}

extension AddTeamScheduleController {
    override func setupSelf() {
        super.setupSelf()
        addObserver()
        addGestureRecogniserForEndEditing(view: contentView)
        
        titleLabel.text = Texts.AddTeamSchedule.title
    }
    
    override func addSubviews() {
        super.addSubviews()
        contentView.addSubview(workingTimeLabel)
        contentView.addSubview(workingTimeContainer)
        contentView.addSubview(workingTimeField)
        contentView.addSubview(scheduleMonthLabel)
        contentView.addSubview(scheduleNameField)
        contentView.addSubview(scheduleNameLabel)
        view.addSubview(monthSelectView)
    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
        workingTimeContainer.snp.makeConstraints { make in
            make.right.top.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(Constants.AddTeamShifts.workingTimeFieldHeight)
            make.width.equalTo(workingTimeField.snp.height).multipliedBy(1.5)
        }
        
        workingTimeField.snp.makeConstraints { make in
            make.edges.equalTo(workingTimeContainer)
        }
        
        workingTimeLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(workingTimeField)
            make.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.right.equalTo(workingTimeField.snp.left).offset(Constants.cgFloat.p10.rawValue)
        }
        
        scheduleMonthLabel.snp.makeConstraints { make in
            make.top.equalTo(workingTimeField.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
        
        scheduleNameField.snp.makeConstraints { make in
            make.top.equalTo(scheduleMonthLabel.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.right.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(Constants.cgFloat.p50.rawValue)
        }
        
        scheduleNameLabel.snp.makeConstraints { make in
            make.top.equalTo(scheduleNameField.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.right.left.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
        
        monthSelectView.snp.makeConstraints { make in
            make.top.equalTo(contentView.snp.bottom)
            make.right.left.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(238)
        }
    }
}

// MARK: - AddTeamScheduleControllerProtocol
extension AddTeamScheduleController: AddTeamScheduleControllerProtocol {
    public func setTeamSchedule(teamSchedule: TeamSchedule) {
        self.teamSchedule = teamSchedule
        scheduleNameField.text = teamSchedule.scheduleName
        workingTimeField.text = teamSchedule.workingTime == 0 ? "" : "\(teamSchedule.workingTime)"
        
        monthSelectView.setDate(teamSchedule.scheduleDate)
    }
    
    public func setTeamSchedules(teamSchedules: [TeamSchedule]) {
        self.teamSchedules = teamSchedules
    }
}

// MARK: - Action
extension AddTeamScheduleController {
    override func handleCancelButton() {
        viewModel.handleClose()
    }
    
    override func handleCreateButton() {
        let newTeamSchedule = TeamSchedule()
        guard let workingTimeStr = workingTimeField.text,
            let workingTime = Double(workingTimeStr),
            let scheduleName = scheduleNameField.text else {
                workingTimeContainer.unimateRequiredFields()
                return
        }
        let teamScheduleDataIsMatch = checkForNameMatched(month: monthSelectView.month,
                                                          year: monthSelectView.year,
                                                          scheduleName: scheduleName)
        
        newTeamSchedule.workingTime = workingTime
        newTeamSchedule.scheduleName = scheduleName
        newTeamSchedule.shiftTypes = teamSchedule.shiftTypes
        newTeamSchedule.personalSchedules = teamSchedule.personalSchedules
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.formatForScheduleDate
        if let scheduleDate = dateFormatter.date(from: "01-\(monthSelectView.month)-\(monthSelectView.year)") {
            newTeamSchedule.scheduleDate = scheduleDate
        }
        
        if UserDefaults.dataIsBeingEdited {
            viewModel.updateTeamSchedule(teamSchedule: teamSchedule,
                                         newTeamSchedule: newTeamSchedule)
        } else if teamScheduleDataIsMatch {
            viewModel.presentPopupAlertScene(alertType: .teamScheduleDataMatch,
                                             scheduleList: [])
        } else {
            viewModel.setDefaultTeamSchedule(presentedMonth: monthSelectView.month,
                                             presentedYear: monthSelectView.year,
                                             workingTime: workingTime,
                                             scheduleName: scheduleName)
        }
        NotificationCenter.default.post(name: .scheduleDataWasEdited, object: nil)
    }
}

private extension AddTeamScheduleController {
    func checkForNameMatched(month: Int, year: Int, scheduleName: String) -> Bool{
        var isUsed = false
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.formatForScheduleDate
        
        let scheduleDate = dateFormatter.date(from: "01-\(month + 1)-\(year)")
        let scheduleName = scheduleName
        
        teamSchedules.forEach { teamSchedule in
            if teamSchedule.scheduleName == scheduleName
                && teamSchedule.scheduleDate == scheduleDate {
                isUsed = true
                return
            }
            
        }
        return isUsed
    }
}

private extension AddTeamScheduleController {
    func addObserver() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(closeSelf),
                                               name: .closeAddTeamScheduleScene,
                                               object: nil)
    }
    
    @objc func closeSelf() {
        viewModel.handleClose()
    }
}
