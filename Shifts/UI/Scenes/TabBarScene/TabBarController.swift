import SnapKit

public final class TabBarController: BaseTabController {
    public enum Tab: Int {
        case personalSchedule, teamShifts, settings
    }

    public init(controllers: [UIViewController]) {
        super.init(nibName: nil, bundle: nil)

        self.viewControllers = controllers
        self.delegate = self
        setupTabBar()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func viewDidLoad() {
        super.viewDidLoad()

        applyTheme()
    }
}

// MARK: - Setup
private extension TabBarController {
    func setupTabBar() {
        setupTabBarAppearance()
        updateTabBar()
    }

    func setupTabBarAppearance() {
        let path = UIBezierPath(roundedRect: .init(x: 0,
                                                   y: 0,
                                                   width: UIScreen.main.bounds.width,
                                                   height: 300),
                                byRoundingCorners: UIRectCorner(arrayLiteral: .allCorners),
                                cornerRadii: CGSize(width: 30, height: 30))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        tabBar.layer.mask = mask
        let shadowView = UIView()
        shadowView.backgroundColor = .white
        view.insertSubview(shadowView, belowSubview: tabBar)
        shadowView.snp.makeConstraints { make in
            make.top.right.left.equalTo(tabBar).inset(1)
            make.bottom.equalTo(tabBar).inset(-100)
        }
        shadowView.layer.cornerRadius = 30
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowRadius = 10
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = .init(width: 0, height: 10)
    }

    func updateTabBar() {
        let tabBarIcons = Icons.TabBar.tabBarIcons
        let selectedTabBarIcons = Icons.TabBar.selectedTabBarIcons
        let tabBarTitles = Texts.TabBar.tabBarTitles
        tabBar.unselectedItemTintColor = Colors.tabBarTextUnselected
        tabBar.tintColor = Colors.tabBarTextSelected

        viewControllers?.enumerated().forEach { index, controller in
            controller.tabBarItem.image = tabBarIcons[index]
            controller.tabBarItem.selectedImage = selectedTabBarIcons[index]
            controller.tabBarItem.title = tabBarTitles[index]
            if UIScreen.main.bounds.height > 800 {
                controller.tabBarItem.imageInsets = UIEdgeInsets(top: 6,
                                                                 left: 0,
                                                                 bottom: -6,
                                                                 right: 0)
            }
        }
    }
}

// MARK: - Constraints
private extension TabBarController {

}

// MARK: - TabBarControllerProtocol
extension TabBarController: TabBarControllerProtocol {

}

// MARK: - Themable
extension TabBarController {
    override public func applyGenericTheme() {
        super.applyGenericTheme()
        updateTabBar()
        tabBar.barTintColor = Colors.tabTintColor
    }
}

extension TabBarController: UITabBarControllerDelegate {
    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
         NotificationCenter.default.post(name: .saveEmployeesChange, object: nil)
    }
}
