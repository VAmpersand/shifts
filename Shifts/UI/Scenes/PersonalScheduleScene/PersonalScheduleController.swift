import UIKit

public final class PersonalScheduleController: BaseController {
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    public var viewModel: PersonalScheduleViewModelProtocol!
    
    private var navigationBar: StaticNavigationBar!
    private lazy var calendarView = CalendarView()
}

extension PersonalScheduleController {
    override func setupSelf() {
        super.setupSelf()
    
        titleLabel.text = Texts.PersonalSchedule.title
    }
    
    override func addSubviews() {
        super.addSubviews()
        view.addSubview(calendarView)
    }

    override func constraintSubviews() {
        super.constraintSubviews()
        calendarView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(Constants.cgFloat.p5.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
    }
}

// MARK: - PersonalScheduleControllerProtocol
extension PersonalScheduleController: PersonalScheduleControllerProtocol {

}
