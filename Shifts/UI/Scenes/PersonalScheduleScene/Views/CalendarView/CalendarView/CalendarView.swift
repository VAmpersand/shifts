import UIKit

extension PersonalScheduleController {
    public class CalendarView: UIView {
        override init(frame: CGRect) {
            super.init(frame: frame)

            setupSelf()
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        private var minLineSpacing: CGFloat = 5
        private var minInteritemSpacing: CGFloat = 12
        private var calendarCellCount = 42
        public var presentedDate = Date()
        
        private lazy var contentView: UIView = {
            let view = UIView()
            view.layer.cornerRadius = 15
            view.backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1).withAlphaComponent(0.2)

            return view
        }()
        
        private lazy var selectMonthView = SelectMonthView()
        private lazy var weekdayView = WeekdayView()

        public lazy var calendarView: UICollectionView = {
            let layout = UICollectionViewFlowLayout()
            layout.minimumLineSpacing = minLineSpacing
            layout.minimumInteritemSpacing = minInteritemSpacing

            let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
            collectionView.register(DateViewCell.self,
                                    forCellWithReuseIdentifier: PersonalScheduleController.DateViewCell.cellID)
            collectionView.backgroundColor = .clear
            collectionView.dataSource = self
            collectionView.delegate = self
            collectionView.isScrollEnabled = false

            return collectionView
        }()
    }
}

private extension PersonalScheduleController.CalendarView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        
        selectMonthView.delegate = self
        
        layer.cornerRadius = 15
        backgroundColor = #colorLiteral(red: 0.1019607857, green: 0.2784313858, blue: 0.400000006, alpha: 1).withAlphaComponent(0.2)
    }
    
    func addSubviews() {
        addSubview(selectMonthView)
        addSubview(weekdayView)
        addSubview(calendarView)
    }
    
    func constraintSubviews() {
        selectMonthView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(Constants.cgFloat.p20.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p23.rawValue)
            make.height.equalTo(Constants.cgFloat.p20.rawValue)
        }
        
        weekdayView.snp.makeConstraints { make in
            make.top.equalTo(selectMonthView.snp.bottom).offset(Constants.cgFloat.p15.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
    
        setCalendarConstraints()
    }
}

// MARK: - UICollectionViewDataSource
extension PersonalScheduleController.CalendarView: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return calendarCellCount
    }

    public func collectionView(_ collectionView: UICollectionView,
                               cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: PersonalScheduleController.DateViewCell.cellID,
            for: indexPath
            ) as? PersonalScheduleController.DateViewCell else { return UICollectionViewCell() }

        setDates(cell: cell, dateLabel: cell.dateLabel, indexPath: indexPath)
        setMarkForToday(todayMarkView: cell.todayMarkView, indexPath: indexPath)

        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PersonalScheduleController.CalendarView: UICollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - minInteritemSpacing * 6) / 7
        return CGSize(width: width, height: width)
    }
}

private extension PersonalScheduleController.CalendarView {
    func setDates(cell: UICollectionViewCell, dateLabel: UILabel, indexPath: IndexPath) {
        let firstWeekDayOfMonth = getIndexFirstWeekDay(date: presentedDate)
        let calcDate = indexPath.row - firstWeekDayOfMonth + 2

        if indexPath.row <= firstWeekDayOfMonth - 2 {
            let prevMonth = presentedDate.agoPrevius(.month)
            dateLabel.text = "\(prevMonth.getDaysInMonth() - firstWeekDayOfMonth + 2 + indexPath.row)"
            dateLabel.textColor = .lightGray
        } else if calcDate > presentedDate.getDaysInMonth() {
            dateLabel.text = "\(calcDate % presentedDate.getDaysInMonth())"
            dateLabel.textColor = .lightGray
        } else {
            dateLabel.text = "\(calcDate)"
            dateLabel.textColor = Colors.title
        }
    }
    
    func setMarkForToday(todayMarkView: UIImageView, indexPath: IndexPath) {
        todayMarkView.image = Icons.Stats.todayMark
        todayMarkView.isHidden = true
        
        let firstWeekDayOfMonth = getIndexFirstWeekDay(date: presentedDate)
        let calcDate = indexPath.row - firstWeekDayOfMonth + 2
        if calcDate == Calendar.current.component(.day, from: presentedDate)
            && presentedDate.stripMonthDay() == Date().stripMonthDay() {
            todayMarkView.isHidden = false
        }
    }
}

private extension PersonalScheduleController.CalendarView {
    func getCalendarHeight() -> CGFloat {
        let firstWeekDayOfMonth = getIndexFirstWeekDay(date: presentedDate)
        let numberOfCells = presentedDate.getDaysInMonth() + firstWeekDayOfMonth - 1
        let calendarInset: CGFloat = 10 + 10
        let cellHeight = ((UIScreen.main.bounds.width - calendarInset * 2) - minInteritemSpacing * 6) / 7
        let lineSpacing = minLineSpacing + 1

        switch ceil(Double(numberOfCells) / 7) {
        case 4: return cellHeight * 4 + lineSpacing * 3
        case 5: return cellHeight * 5 + lineSpacing * 4
        case 6: return cellHeight * 6 + lineSpacing * 5
        default: return 200
        }
    }

    func setCalendarConstraints() {
        calendarView.snp.remakeConstraints { make in
            make.top.equalTo(weekdayView.snp.bottom).offset(Constants.cgFloat.p10.rawValue)
            make.left.right.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
            make.height.equalTo(getCalendarHeight())
            make.bottom.equalToSuperview().inset(Constants.cgFloat.p10.rawValue)
        }
    }
}

private extension PersonalScheduleController.CalendarView {
    func getIndexFirstWeekDay(date: Date) -> Int {
        return Calendar.current.component(.weekday, from: date.stripMonthDay())
    }
}

// MARK: - SelectMonthViewDelegate
extension PersonalScheduleController.CalendarView: SelectMonthViewDelegate {
    public func prevButtonPressed() {
        presentedDate = presentedDate.agoPrevius(.month)
        updateCalendar()
    }

    public func nextButtonPressed() {
        presentedDate = presentedDate.agoForward(.month)
        updateCalendar()
    }
}

extension PersonalScheduleController.CalendarView {
    func updateCalendar() {
        calendarView.reloadData()
        setCalendarConstraints()
        selectMonthView.setDateLabel(date: presentedDate)
    }
}

//
//extension PersonalScheduleController.CalendarView: Themable {
//    public func applyLightTheme() {
//        separatorView.backgroundColor = Colors.statCellSeparator
//        statsLabel.textColor = Colors.title
//        containerView.backgroundColor = Colors.statsCalendarView
//    }
//
//    public func applyDarkTheme() {
//        separatorView.backgroundColor = Colors.statCellSeparator
//        statsLabel.textColor = Colors.title
//        containerView.backgroundColor = Colors.statsCalendarView
//    }
//}
