import UIKit

extension PersonalScheduleController {
    public class DateViewCell: UICollectionViewCell {
        override init(frame: CGRect) {
            super.init(frame: frame)
            
            setupSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        public static let cellID = String(describing: DateViewCell.self)
        
        override public func layoutSubviews() {
            super.layoutSubviews()
            
            shiftTypeView.layer.cornerRadius = shiftTypeView.frame.width / 2
        }
        
        public lazy var dateLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .center
            
            return label
        }()
        
        public lazy var shiftTypeView = UIView()
        public lazy var todayMarkView = UIImageView()
    }
}

private extension PersonalScheduleController.DateViewCell {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
    }
    
    func addSubviews() {
        addSubview(shiftTypeView)
        addSubview(todayMarkView)
        addSubview(dateLabel)
    }
    
    func constraintSubviews() {
        shiftTypeView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        todayMarkView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        dateLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

