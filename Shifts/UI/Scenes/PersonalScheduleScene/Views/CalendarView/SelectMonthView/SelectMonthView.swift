import UIKit

extension PersonalScheduleController {
    public class SelectMonthView: UIView {
        override init(frame: CGRect) {
            super.init(frame: frame)

            setupSelf()
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        weak var delegate: SelectMonthViewDelegate?
        
        private let trianglePrevious = Icons.Stats.trianglePrevious.withRenderingMode(.alwaysTemplate)
        private let triangleForward = Icons.Stats.triangleForward.withRenderingMode(.alwaysTemplate)

        private lazy var prevView: UIImageView = {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFit
            imageView.image = trianglePrevious
            imageView.tintColor = .black

            return imageView
        }()

        private lazy var prevButton: UIButton = {
            let button = UIButton()
            button.addTarget(self,
                             action: #selector(prevButtonPressed),
                             for: .touchUpInside)

            return button
        }()

        private lazy var monthLabel: UILabel = {
            let label = UILabel()
            label.textAlignment = .center
            label.adjustsFontSizeToFitWidth = true

            return label
        }()

        private lazy var nextView: UIImageView = {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFit
            imageView.image = triangleForward
            imageView.tintColor = .black

            return imageView
        }()

        private lazy var nextButton: UIButton = {
            let button = UIButton()
            button.addTarget(self,
                             action: #selector(nextButtonPressed),
                             for: .touchUpInside)

            return button
        }()
    }
}

private extension PersonalScheduleController.SelectMonthView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
        
        setDateLabel(date: Date())
        clipsToBounds = false
    }

    func addSubviews() {
    [
        prevView,
        prevButton,
        monthLabel,
        nextView,
        nextButton,
    ].forEach(addSubview)

    }

    func constraintSubviews() {
        prevView.snp.makeConstraints { make in
            make.left.centerY.equalToSuperview()
            make.height.equalTo(Constants.cgFloat.p10.rawValue)
            make.width.equalTo(Constants.cgFloat.p5.rawValue)
        }

        prevButton.snp.makeConstraints { make in
            make.center.equalTo(prevView)
            make.size.equalTo(Constants.cgFloat.p35.rawValue)
        }

        monthLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }

        nextView.snp.makeConstraints { make in
            make.right.centerY.equalToSuperview()
            make.height.equalTo(Constants.cgFloat.p10.rawValue)
            make.width.equalTo(Constants.cgFloat.p5.rawValue)
        }

        nextButton.snp.makeConstraints { make in
            make.center.equalTo(nextView)
            make.size.equalTo(Constants.cgFloat.p35.rawValue)
        }
    }
}

// MARK: - Action
extension PersonalScheduleController.SelectMonthView {
    @objc func prevButtonPressed() {
        delegate?.prevButtonPressed()
    }

    @objc func nextButtonPressed() {
        delegate?.nextButtonPressed()
    }
}

public extension PersonalScheduleController.SelectMonthView {
    func setDateLabel(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM yyyy"
        monthLabel.text = dateFormatter.string(from: date)
    }
}
//
//extension PersonalScheduleController.SelectMonthView: Themable {
//    public func applyLightTheme() {
//        prevView.tintColor = Colors.statsSelectMonthView
//        monthLabel.textColor = Colors.statsSelectMonthView
//        nextView.tintColor = Colors.statsSelectMonthView
//    }
//
//    public func applyDarkTheme() {
//        prevView.tintColor = Colors.statsSelectMonthView
//        monthLabel.textColor = Colors.statsSelectMonthView
//        nextView.tintColor = Colors.statsSelectMonthView
//    }
//}
