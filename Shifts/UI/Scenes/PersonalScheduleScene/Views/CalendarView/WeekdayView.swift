import UIKit

extension PersonalScheduleController {
    public class WeekdayView: UIView {
        override init(frame: CGRect) {
            super.init(frame: frame)

            setupSelf()
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }

        private lazy var stackView: UIStackView = {
            let stackView = UIStackView()
            stackView.distribution = .fillEqually
            stackView.spacing = 12
            stackView.axis = .horizontal

            let weekdays = Calendar.current.weekdaySymbols
            for day in weekdays {
                let label = UILabel()
                label.textAlignment = .center
                if let dayFirst = day.first {
                    label.text = String(dayFirst)
                }
                stackView.addArrangedSubview(label)
            }

            return stackView
        }()
    }
}

private extension PersonalScheduleController.WeekdayView {
    func setupSelf() {
        addSubviews()
        constraintSubviews()
    }

    func addSubviews() {
        addSubview(stackView)
    }
    
    func constraintSubviews() {
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
