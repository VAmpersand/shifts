import UIKit

public final class TeamScheduleController: BaseController {
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        viewModel.viewDidLoad()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .darkContent
    }

    public var viewModel: TeamScheduleViewModelProtocol!
    
    private var teamSchedules: [TeamSchedule]! {
        didSet {
             setupPresentedTeamSchedule(teamSchedules)
        }
    }
}

extension TeamScheduleController {
    override func setupSelf() {
        super.setupSelf()
        
        titleLabel.text = Texts.TeamSchedule.title
    }
    
    override func addSubviews() {
        super.addSubviews()

    }
    
    override func constraintSubviews() {
        super.constraintSubviews()
    }
}

// MARK: - TeamScheduleControllerProtocol
extension TeamScheduleController: TeamScheduleControllerProtocol {
    public func setTeamSchedules(_ teamSchedules: [TeamSchedule]) {
        self.teamSchedules = teamSchedules
    }
}

private extension TeamScheduleController {
    func setupPresentedTeamSchedule(_ teamSchedules: [TeamSchedule]){
        let currentPresentDate = Date().stripMonthDay()
        let sortTeamSchedules = teamSchedules.sorted { $0.scheduleDate < $1.scheduleDate }
        
        let teamSchedule = sortTeamSchedules.first { teamSchedule in
            teamSchedule.scheduleDate >= currentPresentDate
        }
        
        guard let presentedTeamSchedule = teamSchedule else { return }
        let teamScheduleView = TeamSchedulePresentor(presentedTeamSchedule)
        
        view.addSubview(teamScheduleView)
        
        teamScheduleView.snp.makeConstraints { make in
            make.left.right.bottom.equalTo(view.safeAreaLayoutGuide).inset(10)
            make.top.equalTo(titleView.snp.bottom).offset(60)
        }
    }
}
