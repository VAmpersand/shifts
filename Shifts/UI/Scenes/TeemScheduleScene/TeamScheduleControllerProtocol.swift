public protocol TeamScheduleControllerProtocol: class {
    func setTeamSchedules(_ teamSchedules: [TeamSchedule])
}

