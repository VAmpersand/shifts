import Foundation

@objc
public protocol Themable {
    func applyLightTheme()
    func applyDarkTheme()
}

public extension Themable {
    func applyTheme() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applyLightTheme),
                                               name: .lightThemeDidApplied,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applyDarkTheme),
                                               name: .darkThemeDidApplied,
                                               object: nil)

        switch UserDefaults.theme {
        case .light: applyLightTheme()
        case .dark:  applyDarkTheme()
        }
    }

    func lightThemeDidApplied() {
        UserDefaults.theme = .light
        NotificationCenter.default.post(name: .lightThemeDidApplied, object: nil)
    }

    func darkThemeDidApplied() {
        UserDefaults.theme = .dark
        NotificationCenter.default.post(name: .darkThemeDidApplied, object: nil)
    }

    func resetTheme() {
        switch Theme.current {
        case .dark:  applyDarkTheme()
        case .light: applyLightTheme()
        }
    }
}
