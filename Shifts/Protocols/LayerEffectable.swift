import UIKit

public protocol LayerEffectable {
    func applyEffects()
}
