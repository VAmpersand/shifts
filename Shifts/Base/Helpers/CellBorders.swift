import UIKit

public enum CellBorders {
    case top
    case right
    case bottom
    case left
}

