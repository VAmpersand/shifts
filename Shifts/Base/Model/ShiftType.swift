import Foundation
import RealmSwift

@objcMembers
public class ShiftType: Object, Decodable {
    public dynamic var shiftTypeName: String = ""
    public dynamic var startTime: String = ""
    public dynamic var endTime: String = ""
    public dynamic var shiftDuration: Double = 0
    public dynamic var shiftColorIndex: Int = 0
    
    public override var description: String {
        return """
        Shift Type: \(shiftTypeName), start time: \(startTime),
        end time: \(endTime), color index: \(shiftColorIndex)
        """
    }
}
