import Foundation
import RealmSwift

@objcMembers
public class PersonalSchedule: Object, Decodable {
    public dynamic var order: Int = 0
//    public dynamic var scheduleName: String = ""
//    public dynamic var scheduleDate: Date = Date()
    public dynamic var employee: Employee!
    public dynamic var shifts: List<ShiftType> = List<ShiftType>()
//    public dynamic var shiftTypes: List<ShiftType> = List<ShiftType>()
    
    public override var description: String {
        return "Shifts: \(shifts)"
    }    
}
