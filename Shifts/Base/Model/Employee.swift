import Foundation
import RealmSwift

@objcMembers
public class Employee: Object, Decodable {
//    public dynamic var order: Int = 0
    public dynamic var firstName: String = ""
    public dynamic var lastName: String = ""
    public dynamic var patronymic: String = ""
    public dynamic var position: String = ""
    public dynamic var adminRights: Bool = false
    
    public override var description: String {
        return """
        Employee name: \(firstName), surname: \(lastName),
        position: \(position), can edit: \(adminRights)
        """
    }
}
