import Foundation
import RealmSwift

@objcMembers
public class TeamSchedule: Object, Decodable {
    public dynamic var scheduleDate: Date = Date()
    public dynamic var scheduleName: String = ""
    public dynamic var personalSchedules: List<PersonalSchedule> = List<PersonalSchedule>()
    public dynamic var shiftTypes: List<ShiftType> = List<ShiftType>()
    public dynamic var workingTime: Double = 0

    public override var description: String {
        return "Date: \(scheduleName), shifts: \(personalSchedules)"
    }
}
