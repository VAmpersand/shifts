import UIKit

public extension CALayer {
    func removeAllSublayers() {
        guard let sublayers = sublayers else {
            print("No sublayers for \(self.debugDescription)")
            return
        }

        for sublayer in sublayers {
            sublayer.removeFromSuperlayer()
        }
    }
}
