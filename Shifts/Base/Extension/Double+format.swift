//
//  String+ext.swift
//  AboutMe
//
//  Created by Амир Гезаль on 27.02.2020.
//

import Foundation

public extension Double {
    func formattedLabel(maxFractionDigits: Int) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = maxFractionDigits
        return numberFormatter.string(from: NSNumber(value: self)) ?? "\(self)"
    }
}
