import UIKit

public extension UIView {
    func updateAllSublayers() {
        guard let sublayers = layer.sublayers else {
            print("No sublayers for \(self.debugDescription)")
            return
        }

        for sublayer in sublayers {
            sublayer.frame = bounds
        }

    }
}
