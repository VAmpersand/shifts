import UIKit

public extension UIView {
    func drowTodayMark(color: UIColor) {
        let center = Constants.PersonalShifts.getMarkForTodayParametrs().center
        let radius = Constants.PersonalShifts.getMarkForTodayParametrs().radius
      
        let circlePath = UIBezierPath(arcCenter: center, radius: radius, startAngle: (3 * CGFloat.pi / 2), endAngle: (9 * CGFloat.pi / 2), clockwise: true)
        
        let circleLayer = CAShapeLayer()
        circleLayer.path = circlePath.cgPath
        circleLayer.strokeColor = color.cgColor
        circleLayer.lineWidth = 2
        circleLayer.strokeEnd = 0
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.lineCap = CAShapeLayerLineCap.round
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.duration = 0.5
        animation.toValue = 1
        animation.fillMode = CAMediaTimingFillMode.forwards
        animation.isRemovedOnCompletion = false
        
        circleLayer.add(animation, forKey: nil)
        
        self.layer.addSublayer(circleLayer)
        self.layer.backgroundColor = UIColor.clear.cgColor
    }
    
}
