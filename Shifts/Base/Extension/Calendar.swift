//import Foundation
//
//extension Calendar {
//
//    func dayOfWeek(_ date: Date) -> Int {
//        (self.component(.weekday, from: date) + 7 - self.firstWeekday) % 7 + 1
//    }
//
//    func startOfWeek(_ date: Date) -> Date {
//        return self.date(byAdding: DateComponents(day: -self.dayOfWeek(date) + 1), to: date) ?? date
//    }
//
//    func endOfWeek(_ date: Date) -> Date {
//        return self.date(byAdding: DateComponents(day: 6), to: self.startOfWeek(date)) ?? date
//    }
//
//    func startOfMonth(_ date: Date) -> Date {
//        return self.date(from: self.dateComponents([.year, .month], from: date)) ?? date
//    }
//
//    func endOfMonth(_ date: Date) -> Date {
//        return self.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth(date)) ?? date
//    }
//
//    func startOfYear(_ date: Date) -> Date {
//        return self.date(from: self.dateComponents([.year], from: date)) ?? date
//    }
//
//    func endOfYear(_ date: Date) -> Date {
//        return self.date(from: DateComponents(year: self.component(.year, from: date),
//                                              month: 12,
//                                              day: 31,
//                                              hour: 23,
//                                              minute: 59)) ?? date
//    }
//}
