import UIKit

public extension TimeInterval {
    static let second: TimeInterval = 1
    static let minute: TimeInterval = .second * 60
    static let hour: TimeInterval = .minute * 60
}
