import Foundation

extension String {
    static var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "eu_EU")
        formatter.timeZone = NSTimeZone(name:"UTC+00:00") as TimeZone?
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    var dateString: Date? {
        return String.dateFormatter.date(from: self)
    }
    
    static var roundedTimeFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateFormat = "HH:mm"
        return formatter
    }()

    var roundedTimeStr: Date? {
        return String.roundedTimeFormatter.date(from: self)
    }
}
