import UIKit

extension UserDefaults {
    public static var theme: Theme {
        get {
            return Theme(rawValue: standard._theme) ?? .light
        } set {
            standard._theme = newValue.rawValue
        }
    }
    
    private var _theme: String {
        get {
            return UserDefaults.standard.string(forKey: "_theme") ?? "light"
        } set {
            UserDefaults.standard.set(newValue, forKey: "_theme")
        }
    }
    
    static var dataIsBeingEdited: Bool {
        get {
            UserDefaults.standard.bool(forKey: "employeeDataIsBeingEdited")
        } set {
            UserDefaults.standard.set(newValue, forKey: "employeeDataIsBeingEdited")
        }
    }
    
    static var defaultDataSetted: Bool {
        get {
            UserDefaults.standard.bool(forKey: "defaultDataSetted")
        } set {
            UserDefaults.standard.set(newValue, forKey: "defaultDataSetted")
        }
    }
    
    
    //    static var expiryDate: Date? {
    //        get {
    //            return UserDefaults.standard.value(forKey: "expiryDate") as? Date
    //        }
    //        set {
    //            UserDefaults.standard.set(newValue, forKey: "expiryDate")
    //        }
    //    }
}
