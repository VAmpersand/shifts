import UIKit

public extension UIView {
    enum GradientDirection {
        // Horizontal
        case fromLeft
        case fromRight

        // Vertical
        case fromTop
        case fromBottom

        // Diagonal
        case fromTopLeftCorner
        case fromTopRightCorner
        case fromBottomRightCorner
        case fromBottomLeftCorner
    }

    func setGradient(_ colors: [UIColor] = [.black, .white],
                     locations: [NSNumber] = [0, 1],
                     direction: GradientDirection = .fromLeft) {
        let gradientLayer = CAGradientLayer()

        gradientLayer.colors = colors.map { $0.cgColor }
        gradientLayer.locations = locations
        gradientLayer.frame = bounds

        switch direction {
        // Horizontal
        case .fromLeft:
            gradientLayer.startPoint = .init(x: 0, y: 0)
            gradientLayer.endPoint   = .init(x: 1, y: 0)
        case .fromRight:
            gradientLayer.startPoint = .init(x: 1, y: 0)
            gradientLayer.endPoint   = .init(x: 0, y: 0)

        // Vertical
        case .fromTop:
            gradientLayer.startPoint = .init(x: 0, y: 0)
            gradientLayer.endPoint   = .init(x: 0, y: 1)
        case .fromBottom:
            gradientLayer.startPoint = .init(x: 0, y: 1)
            gradientLayer.endPoint   = .init(x: 0, y: 0)

        // Diagonal
        case .fromTopLeftCorner:
            gradientLayer.startPoint = .init(x: 0, y: 0)
            gradientLayer.endPoint   = .init(x: 1, y: 1)
        case .fromTopRightCorner:
            gradientLayer.startPoint = .init(x: 1, y: 0)
            gradientLayer.endPoint   = .init(x: 0, y: 1)
        case .fromBottomRightCorner:
            gradientLayer.startPoint = .init(x: 1, y: 1)
            gradientLayer.endPoint   = .init(x: 0, y: 0)
        case .fromBottomLeftCorner:
            gradientLayer.startPoint = .init(x: 0, y: 1)
            gradientLayer.endPoint   = .init(x: 1, y: 0)
        }

        layer.insertSublayer(gradientLayer, at: 0)
    }
}
