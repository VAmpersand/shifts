import UIKit

public extension UIStackView {
    func clear() {
        for arrangedSubview in arrangedSubviews {
            removeArrangedSubview(arrangedSubview)
        }

        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
}
