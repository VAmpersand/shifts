//import UIKit
//
//public extension CGRect {
//    func appending(dx: CGFloat = 0, dy: CGFloat = 0, dw: CGFloat = 0, dh: CGFloat = 0) -> CGRect {
//        let rect = CGRect(x: minX + dx, y: minY + dy, width: width + dw, height: height + dh)
//
//        return rect
//    }
//
//    mutating func append(dx: CGFloat = 0, dy: CGFloat = 0, dw: CGFloat = 0, dh: CGFloat = 0) {
//        origin.x += dx
//        origin.y += dy
//        size.width += dw
//        size.height += dh
//    }
//}
//
//public extension CGPoint {
//     func appending(dx: CGFloat = 0, dy: CGFloat = 0) -> CGPoint {
//        let point = CGPoint(x: x + dx, y: y + dy)
//
//        return point
//    }
//
//    mutating func append(dx: CGFloat = 0, dy: CGFloat = 0) {
//        x += dx
//        y += dy
//    }
//}
//
//public extension CGSize {
//    func appending(dw: CGFloat = 0, dh: CGFloat = 0) -> CGSize {
//        let size = CGSize(width: width + dw, height: height + dh)
//
//        return size
//    }
//
//    mutating func append(dw: CGFloat = 0, dh: CGFloat = 0) {
//        width += dw
//        height += dh
//    }
//}
