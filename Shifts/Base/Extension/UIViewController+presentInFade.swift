import UIKit

public extension UIViewController {
    func fadeIn(_ controller: UIViewController, duration: TimeInterval, completion: (() -> Void)? = nil) {
        controller.view.alpha = 0
        present(controller, animated: false) {
            UIView.animate(withDuration: duration, animations: {
                controller.view.alpha = 1
            }, completion: { _ in
                completion?()
            })
        }
    }

    func fadeOut(duration: TimeInterval, completion: (() -> Void)? = nil) {
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.view.alpha = 0
        }) { [weak self] _ in
            self?.dismiss(animated: false, completion: completion)
        }
    }
}
