import UIKit

extension Notification.Name {
    public static var lightThemeDidApplied: Notification.Name {
        return .init("lightThemeDidApplied")
    }

    public static var darkThemeDidApplied: Notification.Name {
        return .init("darkThemeDidApplied")
    }

    public static var monthWasSelected: Notification.Name {
        return .init("monthWasSelected")
    }

    public static var scheduleDataWasEdited: Notification.Name {
        return .init("scheduleDataWasEdited")
    }
  
    public static var shiftTypesWasSelected: Notification.Name {
        return .init("shiftTypesWasSelected")
    }
    
    public static var okButtonPressed: Notification.Name {
        return .init("okButtonPressed")
    }
    
    public static var saveEmployeesChange: Notification.Name {
        return .init("saveEmployeesChange")
    }
    
    public static var closeAddTeamScheduleScene: Notification.Name {
        return .init("closeAddTeamScheduleScene")
    }
}
