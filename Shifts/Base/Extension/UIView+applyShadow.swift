import UIKit

public extension UIView {
    func applyShadow(color: UIColor = .black,
                     alpha: Float = 1,
                     x: CGFloat,
                     y: CGFloat,
                     blur: CGFloat = 4) {

        layer.shadowColor = color.cgColor
        layer.shadowOpacity = alpha
        layer.shadowOffset = CGSize(width: x, height: y)
        layer.shadowRadius = blur
    }

    func applyFirstShadow() {
        applyShadow(color: .black, alpha: 0.15, x: 0, y: 2, blur: 6)
    }

    func applySecondShadow() {
        applyShadow(color: .black, alpha: 0.15, x: 0, y: 4, blur: 6)
    }
}
