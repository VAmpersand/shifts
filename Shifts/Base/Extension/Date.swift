import Foundation

extension Date {
    
    func agoPrevius(_ component: Calendar.Component) -> Date {
        return Calendar.current.date(byAdding: component, value: -1, to: self) ?? Date()
    }
    
    func agoForward(_ component: Calendar.Component) -> Date {
        return Calendar.current.date(byAdding: component, value: 1, to: self) ?? Date()
    }
    
    func localDate() -> Date {
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: self))
        guard let localDate = Calendar.current.date(byAdding: .second,
                                                    value: Int(timeZoneOffset), to: self) else { return Date() }
        
        return localDate
    }
    
    //    static func dates(from fromDate: Date, to toDate: Date, component: Calendar.Component) -> [Date] {
    //        var dates: [Date] = []
    //        var date = fromDate
    //
    //        while date <= toDate {
    //            dates.append(date)
    //            guard let newDate = Calendar.current.date(byAdding: component, value: 1, to: date) else { break }
    //            date = newDate
    //        }
    //        return dates
    //    }
    //
    //        func stripTime() -> Date {
    //            let calendar = Calendar.current
    //            let components = calendar.dateComponents([.year, .month, .day], from: self)
    //            let date = calendar.date(from: components)
    //            return date ?? self
    //        }
    
    func stripMonthDay() -> Date {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month], from: self)
        let date = calendar.date(from: components)
        return date ?? self
    }
    
    var weekday: Int {
        var weekday = Calendar.current.component(.weekday, from: self) - 1
        if weekday == 0 {
            weekday = 7
        }
        
        return weekday
    }
    
    var firstDayOfTheMonth: Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year,.month], from: self))!
    }
    
    func getRoundedTime() -> String{
        
        var minute = 0
        var hour = 0
        
        var minuteInt = 0
        var time = ""
        
        let minuteFormatter = DateFormatter()
        minuteFormatter.dateFormat = "mm"
        let minuteStr = minuteFormatter.string(from: self as Date)
        
        let hourFormatter = DateFormatter()
        hourFormatter.dateFormat = "HH"
        let hourStr = hourFormatter.string(from: self as Date)
        
        let minuteIntOpt: Int?  = Int(minuteStr)
        if minuteIntOpt != nil {
            minuteInt = minuteIntOpt ?? 0
        }
        
        let hourIntOpt: Int? = Int(hourStr)
        if hourIntOpt != nil {
            hour = hourIntOpt ?? 0
        }
        
        minute = Int(minuteInt / 15) * 15
        
        if minute == 0{
            time = "\(hour):00"
        } else {
            time = "\(hour):\(minute)"
        }
        return time
    }
    
    func getDaysInMonth() -> Int {
        let calendar = Calendar.current
        
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        
        guard let range = calendar.range(of: .day, in: .month, for: date) else { return 0 }
        let numDays = range.count
        
        return numDays
    }
}
