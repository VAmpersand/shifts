import UIKit

public extension UIButton {
    func makeSystem() {
        addTarget(self, action: #selector(handleIn), for: [.touchDown, .touchDragInside])
        let events: UIControl.Event = [
            .touchDragOutside,
            .touchUpInside,
            .touchUpOutside,
            .touchDragExit,
            .touchCancel,
        ]
        addTarget(self, action: #selector(handleOut), for: events)
    }

    @objc func handleIn() {
        UIView.animate(withDuration: 0.15) { self.alpha = 0.55 }
    }

    @objc func handleOut() {
        UIView.animate(withDuration: 0.15) { self.alpha = 1 }
    }
}
