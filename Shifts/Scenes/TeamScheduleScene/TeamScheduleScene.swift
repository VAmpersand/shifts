final class TeamScheduleScene: BaseScene {

    // weaver: parentRouter <= Router

    // weaver: teamScheduleRouter = TeamScheduleRouter
    // weaver: teamScheduleRouter.scope = .transient

    // weaver: teamScheduleViewModel = TeamScheduleViewModel
    // weaver: teamScheduleViewModel.scope = .transient

    // weaver: teamScheduleController = TeamScheduleController
    // weaver: teamScheduleController.scope = .transient

    init(injecting dependencies: TeamScheduleSceneDependencyResolver) {
        let router = dependencies.teamScheduleRouter
        let viewModel = dependencies.teamScheduleViewModel
        let controller = dependencies.teamScheduleController

        controller.viewModel = viewModel
        viewModel.router = router
        viewModel.parentRouter = dependencies.parentRouter
        viewModel.controller = controller

        super.init(viewController: controller,
                   router: router,
                   parentRouter: dependencies.parentRouter)
    }
}
