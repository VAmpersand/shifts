final class TeamScheduleViewModel {
    
    // weaver: teamScheduleStorageService = TeamScheduleStorageService
    
    var router: TeamScheduleRouterProtocol!
    var parentRouter: Router!
    weak var controller: TeamScheduleControllerProtocol?

    private let dependencies: TeamScheduleViewModelDependencyResolver
    init(injecting dependencies: TeamScheduleViewModelDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - TeamShiftsViewModelProtocol
 extension TeamScheduleViewModel: TeamScheduleViewModelProtocol {
    func viewDidLoad() {
        dependencies.teamScheduleStorageService.loadTeamSchedules()
        let teamSchedules = dependencies.teamScheduleStorageService.teamSchedules.value
        controller?.setTeamSchedules(teamSchedules)
    }
}
