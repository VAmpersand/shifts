import UIKit

final class TabBarScene: BaseScene {

    // weaver: parentRouter <= Router

    // weaver: tabBarRouter = TabBarRouter
    // weaver: tabBarRouter.scope = .transient

    // weaver: tabBarViewModel = TabBarViewModel
    // weaver: tabBarViewModel.scope = .transient

    // weaver: personalScheduleScene = PersonalScheduleScene
    // weaver: personalScheduleScene.scope = .transient

    // weaver: teamScheduleScene = TeamScheduleScene
    // weaver: teamScheduleScene.scope = .transient

    // weaver: settingsScene = SettingsScene
    // weaver: settingsScene.scope = .transient

    init(injecting dependencies: TabBarSceneDependencyResolver) {
        let router = dependencies.tabBarRouter
        let viewModel = dependencies.tabBarViewModel

        let personalScheduleScene = dependencies.personalScheduleScene(parentRouter: router)
        let teamScheduleScene = dependencies.teamScheduleScene(parentRouter: router)
        let settingsScene = dependencies.settingsScene(parentRouter: router)
       
        router.parentRouter = dependencies.parentRouter
        router.childRouters = [
            personalScheduleScene.router,
            teamScheduleScene.router,
            settingsScene.router,
        ]
        
        let controller: TabBarController!

            let controllers = [
                personalScheduleScene,
                teamScheduleScene,
                settingsScene,
            ].compactMap { $0.viewController }
            controller = TabBarController(controllers: controllers)
 
        viewModel.router = router
        viewModel.controller = controller

        super.init(viewController: controller, router: router, parentRouter: nil)
    }
}
