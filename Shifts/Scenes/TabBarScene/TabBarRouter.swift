import UIKit

final class TabBarRouter: BaseRouter { }

// MARK: - TabBarRouterProtocol
extension TabBarRouter: TabBarRouterProtocol {
    func switchToTab(_ tab: Tab) {
        if let tabBarController = controller as? UITabBarController,
            let viewControllers = tabBarController.viewControllers,
            viewControllers.indices.contains(tab.rawValue) {
            tabBarController.selectedIndex = tab.rawValue
        }
    }
}
