enum Tab: Int {
    case personalShifts, teamShifts, settings
}

protocol TabBarRouterProtocol {
    func switchToTab(_ tab: Tab)
}

