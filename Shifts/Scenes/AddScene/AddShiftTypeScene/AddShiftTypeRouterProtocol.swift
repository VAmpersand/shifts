protocol AddShiftTypeRouterProtocol {
      func handleClose()
      func presentPopupAlertScene(alertType: AlertType, scheduleList: [String])
}
