public protocol AddShiftTypeViewModelProtocol: class {
    func viewDidLoad()
    func handleClose()
    func createShiftType(shiftType: ShiftType)
    func updateShifType(shiftType: ShiftType, newShiftType: ShiftType)
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String])
}
