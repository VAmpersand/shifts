final class AddShiftTypeRouter: BaseRouter {
    
    // weaver: popupAlertScene = PopupAlertScene
    // weaver: popupAlertScene.scope = .transient
    
    private let dependencies: AddShiftTypeRouterDependencyResolver
    
    init(injecting dependencies: AddShiftTypeRouterDependencyResolver) {
        self.dependencies = dependencies
    }
    
}

// MARK: - AddShiftTypeRouterProtocol
extension AddShiftTypeRouter: AddShiftTypeRouterProtocol {
    func handleClose() {
        dismissSelf(using: FadePresentation())
    }
    
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) {
        let popupAlertScene = dependencies.popupAlertScene(parentRouter: self,
                                                           alertType: alertType,
                                                           scheduleList: scheduleList)
        present(popupAlertScene, using: FadePresentation())
    }
}
