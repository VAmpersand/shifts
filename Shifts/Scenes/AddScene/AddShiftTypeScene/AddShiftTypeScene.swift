final class AddShiftTypeScene: BaseScene {

    // weaver: parentRouter <= Router
    // weaver: shiftType <= ShiftType

    // weaver: addShiftTypeRouter = AddShiftTypeRouter
    // weaver: addShiftTypeRouter.scope = .transient

    // weaver: addShiftTypeViewModel = AddShiftTypeViewModel
    // weaver: addShiftTypeViewModel.scope = .transient

    // weaver: addShiftTypeController = AddShiftTypeController
    // weaver: addShiftTypeController.scope = .transient

    init(injecting dependencies: AddShiftTypeSceneDependencyResolver) {
        let router = dependencies.addShiftTypeRouter
        let viewModel = dependencies.addShiftTypeViewModel(shiftType: dependencies.shiftType)
        let controller = dependencies.addShiftTypeController

        controller.viewModel = viewModel
        viewModel.router = router
        viewModel.parentRouter = dependencies.parentRouter
        viewModel.controller = controller

        super.init(viewController: controller,
                   router: router,
                   parentRouter: dependencies.parentRouter)
    }
}
