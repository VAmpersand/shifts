final class AddShiftTypeViewModel {
    
    // weaver: shiftType <= ShiftType
    
    // weaver: shiftTypeStorageService = ShiftTypeStorageService

    var router: AddShiftTypeRouterProtocol!
    var parentRouter: Router!
    weak var controller: AddShiftTypeControllerProtocol?
    private let dependencies: AddShiftTypeViewModelDependencyResolver

    init(injecting dependencies: AddShiftTypeViewModelDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - AddShiftTypeViewModelProtocol
extension AddShiftTypeViewModel: AddShiftTypeViewModelProtocol {
    func viewDidLoad() {
        controller?.setShiftTypeData(shiftType: dependencies.shiftType)
        let shiftTypes = getShiftTypes()
        controller?.setShiftTypesData(shiftTypes: shiftTypes)
    }
    
    func handleClose() {
        router.handleClose()
    }
    
    func createShiftType(shiftType: ShiftType) {
        dependencies.shiftTypeStorageService.create(shiftType)
        router.handleClose()
    }
    
    func updateShifType(shiftType: ShiftType, newShiftType: ShiftType) {
        dependencies.shiftTypeStorageService.update(shiftType, newShiftType: newShiftType)
        router.handleClose()
    }
    
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) {
        router.presentPopupAlertScene(alertType: alertType,
                                                  scheduleList: scheduleList)
    }
}

extension AddShiftTypeViewModel {
    func getShiftTypes() -> [ShiftType] {
        dependencies.shiftTypeStorageService.loadShiftType()
        return dependencies.shiftTypeStorageService.shiftTypes.value
    }
}
