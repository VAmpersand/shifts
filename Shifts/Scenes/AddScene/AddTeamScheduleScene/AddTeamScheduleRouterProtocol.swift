protocol AddTeamScheduleRouterProtocol {
    func handleClose()
    func presentBuilderTeamScheduleScene(teamSchedule: TeamSchedule)
    func presentPopupAlertScene(alertType: AlertType,
                                   scheduleList: [String])
}
