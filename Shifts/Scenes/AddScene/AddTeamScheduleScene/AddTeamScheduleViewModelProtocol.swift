public protocol AddTeamScheduleViewModelProtocol: class {
    func viewDidLoad()
    func handleClose()
    func updateTeamSchedule(teamSchedule: TeamSchedule,
                            newTeamSchedule: TeamSchedule)
    func setDefaultTeamSchedule(presentedMonth: Int,
                                presentedYear: Int,
                                workingTime: Double,
                                scheduleName: String)
    func presentBuilderTeamScheduleScene(teamSchedule: TeamSchedule)
    func presentPopupAlertScene(alertType: AlertType,
                                scheduleList: [String])
}
