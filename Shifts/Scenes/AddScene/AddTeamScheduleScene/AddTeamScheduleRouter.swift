final class AddTeamScheduleRouter: BaseRouter {
    
    // weaver: builderTeamScheduleScene = BuilderTeamScheduleScene
    // weaver: builderTeamScheduleScene.scope = .transient
    
    // weaver: popupAlertScene = PopupAlertScene
    // weaver: popupAlertScene.scope = .transient
    
    private let dependencies: AddTeamScheduleRouterDependencyResolver
    
    init(injecting dependencies: AddTeamScheduleRouterDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - AddTeamScheduleRouterProtocol
extension AddTeamScheduleRouter: AddTeamScheduleRouterProtocol {
    func handleClose() {
        dismissSelf(using: FadePresentation())
    }
    
    func presentBuilderTeamScheduleScene(teamSchedule: TeamSchedule) {
        let builderTeamScheduleScene = dependencies.builderTeamScheduleScene(parentRouter: self,
                                                                             teamSchedule: teamSchedule)
        present(builderTeamScheduleScene, using: FadePresentation())
    }
    
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) {
        let popupAlertScene = dependencies.popupAlertScene(parentRouter: self,
                                                           alertType: alertType,
                                                           scheduleList: scheduleList)
        present(popupAlertScene, using: FadePresentation())
    }
}
