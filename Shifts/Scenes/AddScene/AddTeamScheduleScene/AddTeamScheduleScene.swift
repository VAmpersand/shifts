final class AddTeamScheduleScene: BaseScene {

    // weaver: parentRouter <= Router
    // weaver: teamSchedule <= TeamSchedule

    // weaver: addTeamScheduleRouter = AddTeamScheduleRouter
    // weaver: addTeamScheduleRouter.scope = .transient

    // weaver: addTeamScheduleViewModel = AddTeamScheduleViewModel
    // weaver: addTeamScheduleViewModel.scope = .transient

    // weaver: addTeamScheduleController = AddTeamScheduleController
    // weaver: addTeamScheduleController.scope = .transient

    init(injecting dependencies: AddTeamScheduleSceneDependencyResolver) {
        let router = dependencies.addTeamScheduleRouter
        let viewModel = dependencies.addTeamScheduleViewModel(teamSchedule: dependencies.teamSchedule)
        let controller = dependencies.addTeamScheduleController

        controller.viewModel = viewModel
        viewModel.router = router
        viewModel.parentRouter = dependencies.parentRouter
        viewModel.controller = controller

        super.init(viewController: controller,
                   router: router,
                   parentRouter: dependencies.parentRouter)
    }
}
