final class AddTeamScheduleViewModel {
    
    // weaver: teamSchedule <= TeamSchedule
    
    // weaver: teamScheduleStorageService = TeamScheduleStorageService

    var router: AddTeamScheduleRouterProtocol!
    var parentRouter: Router!
    weak var controller: AddTeamScheduleControllerProtocol?
    private let dependencies: AddTeamScheduleViewModelDependencyResolver

    init(injecting dependencies: AddTeamScheduleViewModelDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - AddTeamSchedulViewModelProtocol
extension AddTeamScheduleViewModel: AddTeamScheduleViewModelProtocol {
    func viewDidLoad() {
        let teamSchedules = getTeamSchedule()
        controller?.setTeamSchedules(teamSchedules: teamSchedules)
        controller?.setTeamSchedule(teamSchedule: dependencies.teamSchedule)
    }
    
    func handleClose() {
        router.handleClose()
    }
 
    func updateTeamSchedule(teamSchedule: TeamSchedule,
                            newTeamSchedule: TeamSchedule) {
        dependencies.teamScheduleStorageService.update(teamSchedule,
                                                       newTeamSchedule: newTeamSchedule)
        router.handleClose()
    }
    
    func setDefaultTeamSchedule(presentedMonth: Int,
                                presentedYear: Int,
                                workingTime: Double,
                                scheduleName: String){
        let teamSchedule = dependencies.teamScheduleStorageService.createDefaultSchedule(presentedMonth: presentedMonth,
                                                                                         presentedYear: presentedYear,
                                                                                         workingTime: workingTime,
                                                                                         scheduleName: scheduleName)
        router.presentBuilderTeamScheduleScene(teamSchedule: teamSchedule)
    }
    
    func presentBuilderTeamScheduleScene(teamSchedule: TeamSchedule) {
        dependencies.teamScheduleStorageService.create(teamSchedule)
        router.presentBuilderTeamScheduleScene(teamSchedule: teamSchedule)
    }
    
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) {
        router.presentPopupAlertScene(alertType: alertType, scheduleList: scheduleList)
    }
}


extension AddTeamScheduleViewModel {
    func getTeamSchedule() -> [TeamSchedule] {
        dependencies.teamScheduleStorageService.loadTeamSchedules()
        return dependencies.teamScheduleStorageService.teamSchedules.value
    }
}
