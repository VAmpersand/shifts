final class AddEmployeeViewModel {
    
    // weaver: employee <= Employee
    
    // weaver: employeesStorageService = EmployeesStorageService

    var router: AddEmployeeRouterProtocol!
    var parentRouter: Router!
    weak var controller: AddEmployeeControllerProtocol?
    private let dependencies: AddEmployeeViewModelDependencyResolver

    init(injecting dependencies: AddEmployeeViewModelDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - AddEmployeeViewModelProtocol
extension AddEmployeeViewModel: AddEmployeeViewModelProtocol {
    func viewDidLoad() {
        controller?.setEmployeeData(employee: dependencies.employee)
        let employees = getEmploees()
        controller?.setEmployeesData(employees: employees)
    }
    
    func handleClose() {
        router.handleClose()
    }
    
    func createEmployee(employee: Employee) {
        dependencies.employeesStorageService.create(employee)
        router.handleClose()
    }
    
    func updateEmployee(employee: Employee, newEmployee: Employee) {
        dependencies.employeesStorageService.update(employee, newEmployee: newEmployee)
        router.handleClose()
    }
    
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) {
        router.presentPopupAlertScene(alertType: alertType,
                                                 scheduleList: scheduleList)
    }
}

extension AddEmployeeViewModel {
    func getEmploees() -> [Employee] {
        dependencies.employeesStorageService.loadEmployees()
        return dependencies.employeesStorageService.employees.value
    }
}


