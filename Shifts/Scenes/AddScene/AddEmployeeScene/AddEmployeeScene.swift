final class AddEmployeeScene: BaseScene {

    // weaver: parentRouter <= Router
    // weaver: employee <= Employee

    // weaver: addEmployeeRouter = AddEmployeeRouter
    // weaver: addEmployeeRouter.scope = .transient

    // weaver: addEmployeeViewModel = AddEmployeeViewModel
    // weaver: addEmployeeViewModel.scope = .transient

    // weaver: addEmployeeController = AddEmployeeController
    // weaver: addEmployeeController.scope = .transient

    init(injecting dependencies: AddEmployeeSceneDependencyResolver) {
        let router = dependencies.addEmployeeRouter
        let viewModel = dependencies.addEmployeeViewModel(employee: dependencies.employee)
        let controller = dependencies.addEmployeeController

        controller.viewModel = viewModel
        viewModel.router = router
        viewModel.parentRouter = dependencies.parentRouter
        viewModel.controller = controller

        super.init(viewController: controller,
                   router: router,
                   parentRouter: dependencies.parentRouter)
    }
}
