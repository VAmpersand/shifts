protocol AddEmployeeRouterProtocol {
      func handleClose()
      func presentPopupAlertScene(alertType: AlertType,
                                             scheduleList: [String]) 
}
