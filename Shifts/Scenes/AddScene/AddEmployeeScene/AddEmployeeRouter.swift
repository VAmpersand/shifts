final class AddEmployeeRouter: BaseRouter {
    
    // weaver: popupAlertScene = PopupAlertScene
    // weaver: popupAlertScene.scope = .transient
    
    private let dependencies: AddEmployeeRouterDependencyResolver
    
    init(injecting dependencies: AddEmployeeRouterDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - AddEmployeeRouterProtocol
extension AddEmployeeRouter: AddEmployeeRouterProtocol {
    func handleClose() {
        dismissSelf(using: FadePresentation())
    }
    
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) {
        let popupAlertScene = dependencies.popupAlertScene(parentRouter: self,
                                                           alertType: alertType,
                                                           scheduleList: scheduleList)
        present(popupAlertScene, using: FadePresentation())
    }
}
