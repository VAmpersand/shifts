public protocol AddEmployeeViewModelProtocol: class {
    func viewDidLoad()
    func handleClose()
    func createEmployee(employee: Employee)
    func updateEmployee(employee: Employee, newEmployee: Employee)
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String])
}

