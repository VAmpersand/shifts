import UIKit

final class SettingsRouter: BaseRouter {
        
    // weaver: addEmployeeScene = AddEmployeeScene
    // weaver: addEmployeeScene.scope = .transient
    
    // weaver: addShiftTypeScene = AddShiftTypeScene
    // weaver: addShiftTypeScene.scope = .transient
    
    // weaver: addTeamScheduleScene = AddTeamScheduleScene
    // weaver: addTeamScheduleScene.scope = .transient
    
    // weaver: builderTeamScheduleScene = BuilderTeamScheduleScene
    // weaver: builderTeamScheduleScene.scope = .transient
    
    // weaver: popupAlertScene = PopupAlertScene
    // weaver: popupAlertScene.scope = .transient
    
    private let dependencies: SettingsRouterDependencyResolver

    init(injecting dependencies: SettingsRouterDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - SettingsRouterProtocol
extension SettingsRouter: SettingsRouterProtocol {
    func presentAddEmployeeScene(_ employee: Employee) {
        let addEmployeeScene = dependencies.addEmployeeScene(parentRouter: self,
                                                             employee: employee)
        present(addEmployeeScene, using: FadePresentation())
    }
    
    func presentAddShiftTypeScene(_ shiftType: ShiftType) {
        let addShiftTypeScene = dependencies.addShiftTypeScene(parentRouter: self,
                                                               shiftType: shiftType)
        present(addShiftTypeScene, using: FadePresentation())
    }
    
    func presentAddTeamScheduleScene(_ teamSchedule: TeamSchedule) {
        let addTeamScheduleScene = dependencies.addTeamScheduleScene(parentRouter: self,
                                                                     teamSchedule: teamSchedule)
        present(addTeamScheduleScene, using: FadePresentation())
    }
    
    func presentBuilderTeamScheduleScene(_ teamSchedule: TeamSchedule) {
        let builderTeamScheduleScene = dependencies.builderTeamScheduleScene(parentRouter: self,
                                                                             teamSchedule: teamSchedule)
        present(builderTeamScheduleScene, using: FadePresentation())
    }
    
    func presentPopupAlertScene(with alertType: AlertType, scheduleList: [String]) {
        let popupAlertScene = dependencies.popupAlertScene(parentRouter: self,
                                                           alertType: alertType,
                                                           scheduleList: scheduleList)
        present(popupAlertScene, using: FadePresentation())
    }
}
