import UIKit

public protocol SettingsViewModelProtocol: class {
    func viewDidLoad()
    func moveEmployee(from index: Int, to newIndex: Int)
    func presentAddEmployeeScene(_ employee: Employee)
    func deleteEmployee(_ employee: Employee)
    func presentAddShiftTypeScene(_ shiftType: ShiftType)
    func deleteShiftType(_ shiftType: ShiftType)
    func presentAddTeamScheduleScene(_ teamSchedule: TeamSchedule)
    func deleteTeamSchedule(_ teamSchedule: TeamSchedule)
    func presentBuilderTeamScheduleScene(_ teamSchedule: TeamSchedule)
    func presentPopupAlertScene(with alertType: AlertType, scheduleList: [String])
}

