import UIKit

protocol SettingsRouterProtocol {
    func presentAddEmployeeScene(_ employee: Employee)
    func presentAddShiftTypeScene(_ shiftType: ShiftType)
    func presentAddTeamScheduleScene(_ teamSchedule: TeamSchedule)
    func presentBuilderTeamScheduleScene(_ teamSchedule: TeamSchedule)
    func presentPopupAlertScene(with alertType: AlertType, scheduleList: [String])
}
