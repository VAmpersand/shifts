final class SettingsScene: BaseScene {

    // weaver: parentRouter <= Router

    // weaver: settingsRouter = SettingsRouter
    // weaver: settingsRouter.scope = .transient

    // weaver: settingsViewModel = SettingsViewModel
    // weaver: settingsViewModel.scope = .transient

    // weaver: settingsController = SettingsController
    // weaver: settingsController.scope = .transient

    init(injecting dependencies: SettingsSceneDependencyResolver) {
        let router = dependencies.settingsRouter
        let viewModel = dependencies.settingsViewModel
        let controller = dependencies.settingsController

        controller.viewModel = viewModel
        viewModel.router = router
        viewModel.parentRouter = dependencies.parentRouter
        viewModel.controller = controller

        super.init(viewController: controller,
                   router: router,
                   parentRouter: dependencies.parentRouter)
    }
}
