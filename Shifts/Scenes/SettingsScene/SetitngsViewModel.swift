import UIKit

final class SettingsViewModel {
    
    // weaver: employeesStorageService = EmployeesStorageService
    // weaver: shiftTypeStorageService = ShiftTypeStorageService
    // weaver: teamScheduleStorageService = TeamScheduleStorageService
    // weaver: personalScheduleStorageService = PersonalScheduleStorageService
    
    var router: SettingsRouterProtocol!
    var parentRouter: Router!
    weak var controller: SettingsControllerProtocol?
    private let dependencies: SettingsViewModelDependencyResolver
    private var employees: [Employee]!
    private var shiftTypes: [ShiftType]!
    private var teamSchedules: [TeamSchedule]!
    
    init(injecting dependencies: SettingsViewModelDependencyResolver) {
        self.dependencies = dependencies
        self.employees = getEmployees()
        self.shiftTypes = getShiftTypes()
        self.teamSchedules = getTeamSchedule()
    }
}

// MARK: - SettingsViewModelProtocol
extension SettingsViewModel: SettingsViewModelProtocol {    
    func viewDidLoad() {
        employees = getEmployees()
        controller?.setEmployees(employees)
        shiftTypes = getShiftTypes()
        controller?.setShiftTypes(shiftTypes)
        teamSchedules = getTeamSchedule()
        controller?.setTeamSchedules(teamSchedules)
        
        let personalSchedules = getPersonalSchedules()
        controller?.setPersonalSchedules(personalSchedules)
    }
    
    func moveEmployee(from index: Int, to newIndex: Int) {
//        dependencies.employeesStorageService.moveEmployee(from: index,
//                                                          to: newIndex)
    }
    
    func presentAddEmployeeScene(_ employee: Employee) {
        router.presentAddEmployeeScene(employee)
    }
    
    func deleteEmployee(_ employee: Employee) {
        dependencies.employeesStorageService.deleteEmployeeWithPersonalSchedules(employee)
    }
        
    func presentAddShiftTypeScene(_ shiftType: ShiftType) {
        router.presentAddShiftTypeScene(shiftType)
    }
    
    func deleteShiftType(_ shiftType: ShiftType) {
        dependencies.shiftTypeStorageService.delete(shiftType)
    }
    
    func presentAddTeamScheduleScene(_ teamSchedule: TeamSchedule) {
        router.presentAddTeamScheduleScene(teamSchedule)
    }
    
    func deleteTeamSchedule(_ teamSchedule: TeamSchedule) {
        dependencies.teamScheduleStorageService.delete(teamSchedule)
    }
    
    func presentBuilderTeamScheduleScene(_ teamSchedule: TeamSchedule) {
        router.presentBuilderTeamScheduleScene(teamSchedule)
    }

    func presentPopupAlertScene(with alertType: AlertType, scheduleList: [String]) {
        router.presentPopupAlertScene(with: alertType,
                                      scheduleList: scheduleList)
    }
}

extension SettingsViewModel {
    func getEmployees() -> [Employee] {
        dependencies.employeesStorageService.loadEmployees()
        return dependencies.employeesStorageService.employees.value
    }
    
    func getShiftTypes() -> [ShiftType] {
        dependencies.shiftTypeStorageService.loadShiftType()
        return dependencies.shiftTypeStorageService.shiftTypes.value
    }
    
    func getTeamSchedule() -> [TeamSchedule] {
        dependencies.teamScheduleStorageService.loadTeamSchedules()
        return dependencies.teamScheduleStorageService.teamSchedules.value
    }
    
    
    func getPersonalSchedules() -> [PersonalSchedule] {
        dependencies.personalScheduleStorageService.loadPersonalSchedule()
        return dependencies.personalScheduleStorageService.personalSchedules.value
    }

}
