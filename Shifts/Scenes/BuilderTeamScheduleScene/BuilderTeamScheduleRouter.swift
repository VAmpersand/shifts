final class BuilderTeamScheduleRouter: BaseRouter {
    
    // weaver: popupAlertScene = PopupAlertScene
    // weaver: popupAlertScene.scope = .transient
    
    private let dependencies: BuilderTeamScheduleRouterDependencyResolver
    
    init(injecting dependencies: BuilderTeamScheduleRouterDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - BuilderTeamScheduleRouterProtocol
extension BuilderTeamScheduleRouter: BuilderTeamScheduleRouterProtocol {
    func handleClose() {
        dismissSelf(using: FadePresentation())
    }
    
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) {
        let popupAlertScene = dependencies.popupAlertScene(parentRouter: self,
                                                           alertType: alertType,
                                                           scheduleList: scheduleList)
        present(popupAlertScene, using: FadePresentation())
    }
}
