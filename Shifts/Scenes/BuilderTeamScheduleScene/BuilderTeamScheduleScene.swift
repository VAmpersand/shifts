final class BuilderTeamScheduleScene: BaseScene {

    // weaver: parentRouter <= Router
    // weaver: teamSchedule <= TeamSchedule

    // weaver: teamScheduleRouter = BuilderTeamScheduleRouter
    // weaver: teamScheduleRouter.scope = .transient

    // weaver: teamScheduleViewModel = BuilderTeamScheduleViewModel
    // weaver: teamScheduleViewModel.scope = .transient

    // weaver: teamScheduleController = BuilderTeamScheduleController
    // weaver: teamScheduleController.scope = .transient

    init(injecting dependencies: BuilderTeamScheduleSceneDependencyResolver) {
        let router = dependencies.teamScheduleRouter
        let viewModel = dependencies.teamScheduleViewModel(teamSchedule: dependencies.teamSchedule)
        let controller = dependencies.teamScheduleController

        controller.viewModel = viewModel
        viewModel.router = router
        viewModel.parentRouter = dependencies.parentRouter
        viewModel.controller = controller

        super.init(viewController: controller,
                   router: router,
                   parentRouter: dependencies.parentRouter)
    }
}
