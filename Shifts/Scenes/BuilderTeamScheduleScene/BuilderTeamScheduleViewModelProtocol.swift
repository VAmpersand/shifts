public protocol BuilderTeamScheduleViewModelProtocol: class {
    func viewDidLoad()
    func handleClose()
    func createTeamSchedule(teamSchedule: TeamSchedule)
    func updateTeamSchedule(teamSchedule: TeamSchedule, newTeamSchedule: TeamSchedule)
    func updatePersonalSchedule(personalSchedule: PersonalSchedule, newPersonalSchedule: PersonalSchedule)
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) 
}

