final class BuilderTeamScheduleViewModel {
    
    // weaver: teamSchedule <= TeamSchedule
    
    // weaver: shiftTypeStorageService = ShiftTypeStorageService
    // weaver: teamScheduleStorageService = TeamScheduleStorageService
    // weaver: personalScheduleStorageService = PersonalScheduleStorageService

    var router: BuilderTeamScheduleRouterProtocol!
    var parentRouter: Router!
    weak var controller: BuilderTeamScheduleControllerProtocol?
    private let dependencies: BuilderTeamScheduleViewModelDependencyResolver

    init(injecting dependencies: BuilderTeamScheduleViewModelDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - BuilderTeamScheduleViewModelProtocol
extension BuilderTeamScheduleViewModel: BuilderTeamScheduleViewModelProtocol {
    func viewDidLoad() {
        controller?.setTeamSchedule(teamSchedule: dependencies.teamSchedule)
        let shiftTypes = getShiftTypes()
        controller?.setShiftTypes(shiftTypes: shiftTypes)
    }
    
    func handleClose() {
        router.handleClose()
    }
    
    func createTeamSchedule(teamSchedule: TeamSchedule) {
        dependencies.teamScheduleStorageService.create(teamSchedule)
        router.handleClose()
    }
    
    func updateTeamSchedule(teamSchedule: TeamSchedule, newTeamSchedule: TeamSchedule) {
        dependencies.teamScheduleStorageService.update(teamSchedule,
                                                       newTeamSchedule: newTeamSchedule)
        router.handleClose()
    }
    
    func updatePersonalSchedule(personalSchedule: PersonalSchedule, newPersonalSchedule: PersonalSchedule) {
        dependencies.personalScheduleStorageService.updateSchedule(personalSchedule,
                                                                   newPersonalSchedule: newPersonalSchedule)
    }
    
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) {
        router.presentPopupAlertScene(alertType: alertType, scheduleList: scheduleList)
    }
}

extension BuilderTeamScheduleViewModel {
    func getShiftTypes() -> [ShiftType]{
        dependencies.shiftTypeStorageService.loadShiftType()
        return dependencies.shiftTypeStorageService.shiftTypes.value
    }
}


