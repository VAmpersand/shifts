protocol BuilderTeamScheduleRouterProtocol {
    func handleClose()
    func presentPopupAlertScene(alertType: AlertType, scheduleList: [String]) 
}
