final class PersonalScheduleScene: BaseScene {

    // weaver: parentRouter <= Router

    // weaver: personalScheduleRouter = PersonalScheduleRouter
    // weaver: personalScheduleRouter.scope = .transient

    // weaver: personalScheduleViewModel = PersonalScheduleViewModel
    // weaver: personalScheduleViewModel.scope = .transient

    // weaver: personalScheduleController = PersonalScheduleController
    // weaver: personalScheduleController.scope = .transient

    init(injecting dependencies: PersonalScheduleSceneDependencyResolver) {
        let router = dependencies.personalScheduleRouter
        let viewModel = dependencies.personalScheduleViewModel
        let controller = dependencies.personalScheduleController

        controller.viewModel = viewModel
        viewModel.router = router
        viewModel.parentRouter = dependencies.parentRouter
        viewModel.controller = controller

        super.init(viewController: controller,
                   router: router,
                   parentRouter: dependencies.parentRouter)
    }
}
