final class PopupAlertViewModel {
    
    // weaver: alertType <= AlertType
    // weaver: scheduleList <= [String]

    var router: PopupAlertRouterProtocol!
    var parentRouter: Router!

    weak var controller: PopupAlertControllerProtocol?
    private let dependencies: PopupAlertViewModelDependencyResolver

    init(injecting dependencies: PopupAlertViewModelDependencyResolver) {
        self.dependencies = dependencies
    }
}

// MARK: - PopupAlertViewModelProtocol
 extension PopupAlertViewModel: PopupAlertViewModelProtocol {
    func viewDidLoad() {
        controller?.setScheduleList(scheduleList: dependencies.scheduleList)
        controller?.setAlert(alertType: dependencies.alertType)
    }
    
    func handleClose() {
        router.handleClose()
    }
}
