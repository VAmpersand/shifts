final class PopupAlertRouter: BaseRouter {

}

// MARK: - PopupAlertRouterProtocol
extension PopupAlertRouter: PopupAlertRouterProtocol {
    func handleClose() {
        dismissSelf(using: FadePresentation())
    }
}
