public enum AlertType {
    case deleteEmployee
    case deleteShiftType
    case deleteTeamSchedule
    case cancelScheduleEditing
    case clearTeamSchedule
    case employeeDataMatch
    case shiftTypeDataMatch
    case teamScheduleDataMatch
}

public protocol PopupAlertViewModelProtocol: class {
    func viewDidLoad()
    func handleClose()
}
