final class PopupAlertScene: BaseScene {

    // weaver: parentRouter <= Router
    // weaver: alertType <= AlertType
    // weaver: scheduleList <= [String]
  
    // weaver: popupAlertRouter = PopupAlertRouter
    // weaver: popupAlertRouter.scope = .transient

    // weaver: popupAlertViewModel = PopupAlertViewModel
    // weaver: popupAlertViewModel.scope = .transient

    // weaver: popupAlertController = PopupAlertController
    // weaver: popupAlertController.scope = .transient

    init(injecting dependencies: PopupAlertSceneDependencyResolver) {
        let router = dependencies.popupAlertRouter
        let viewModel = dependencies.popupAlertViewModel(alertType: dependencies.alertType,
                                                         scheduleList: dependencies.scheduleList)
        let controller = dependencies.popupAlertController

        controller.viewModel = viewModel
        viewModel.router = router
        viewModel.parentRouter = dependencies.parentRouter
        viewModel.controller = controller

        super.init(viewController: controller,
                   router: router,
                   parentRouter: dependencies.parentRouter)
    }
}
