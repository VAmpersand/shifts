import UIKit

final class MainRouter: BaseRouter {

    // weaver: tabBarScene = TabBarScene
    // weaver: tabBarScene.scope = .transient
    
    // weaver: shiftTypeStorageService = ShiftTypeStorageService
        
    private let dependencies: MainRouterDependencyResolver
    
    private var window: UIWindow!
    private var windowScene: UIWindowScene!
    
    init(injecting dependencies: MainRouterDependencyResolver) {
        self.dependencies = dependencies
        super.init()
    }
}

// MARK: - Starting app
extension MainRouter {
    func startApp(in scene: UIScene) {
        setupWindow(in: scene)
        
        setDefaultData()
    }
        
    func setDefaultData() {
        dependencies.shiftTypeStorageService.createDefaultShiftType()
    }
    
    private func setupWindow(in scene: UIScene) {
        guard let windowScene = (scene as? UIWindowScene) else {
            fatalError("Failed to configure windowScene")
        }
        self.windowScene = windowScene
        let tabBarScene = dependencies.tabBarScene(parentRouter: self)
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.makeKeyAndVisible()
        window.rootViewController = tabBarScene.viewController
    }
}
