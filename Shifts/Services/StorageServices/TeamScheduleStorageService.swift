import Firebase
import RealmSwift
import RxCocoa

final class TeamScheduleStorageService {

    let teamSchedules = BehaviorRelay<[TeamSchedule]>(value: [])
    private let realm = try! Realm()
    private let employeesStorageService = EmployeesStorageService()
    private let shiftTypeStorageService = ShiftTypeStorageService()
    private let personalScheduleStorageService = PersonalScheduleStorageService()
//    private let firebaseStorageService = FirebaseStorageService()
}

// MARK: - CRUD
extension TeamScheduleStorageService {
    func create(_ teamSchedule: TeamSchedule) {
        do {
            try realm.write {
                realm.add(teamSchedule)
            }
        } catch {
            print(error.localizedDescription)
        }
        teamSchedules.accept(teamSchedules.value + [teamSchedule])
    }

    func isOnStorage(_ teamSchedule: TeamSchedule) -> Bool {
        return teamSchedules.value.contains(where: {
            $0.scheduleDate == teamSchedule.scheduleDate
                && $0.scheduleName == teamSchedule.scheduleName
        })
    }
    
    func loadTeamSchedules() {
        teamSchedules.accept(Array(realm.objects(TeamSchedule.self)))
    }
    
    func createDefaultSchedule(presentedMonth: Int,
                                 presentedYear: Int,
                                 workingTime: Double,
                                 scheduleName: String) -> TeamSchedule {
        let teamSchedule = TeamSchedule()
        teamSchedule.scheduleName = scheduleName
        teamSchedule.workingTime = workingTime
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Constants.formatForScheduleDate
        if let date = dateFormatter.date(from: "01-\(presentedMonth + 1)-\(presentedYear)") {
            teamSchedule.scheduleDate = date
        }

        shiftTypeStorageService.loadShiftType()
        let shiftTypes = shiftTypeStorageService.shiftTypes.value
        shiftTypes.forEach {
            teamSchedule.shiftTypes.append($0)
        }
     
        employeesStorageService.loadEmployees()
        let employees = employeesStorageService.employees.value
        
        employees.enumerated().forEach {
            let personalShifts = personalScheduleStorageService.createDefaultPersonalSchedule(
                teamSchedule: teamSchedule,
                employee: $0.element
            )
            teamSchedule.personalSchedules.append(personalShifts)
        }
        
        create(teamSchedule)
        return teamSchedule
    }
    
    func appendPersonalScheduleIfNeeded(for employee: Employee) {
        loadTeamSchedules()
        let teamSchedules = self.teamSchedules.value
        
        teamSchedules.forEach { teamSchedule in
            let personalSchedule = personalScheduleStorageService.createDefaultPersonalSchedule(
                teamSchedule: teamSchedule,
                employee: employee
            )
            if !personalScheduleStorageService.isOnStorage(personalSchedule) {
                do {
                    try realm.write {
                        teamSchedule.personalSchedules.append(personalSchedule)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
    
    func update(_ teamSchedule: TeamSchedule, newTeamSchedule: TeamSchedule) {
        do {
            try realm.write {
                teamSchedule.scheduleName = newTeamSchedule.scheduleName
                teamSchedule.workingTime = newTeamSchedule.workingTime
            }
        } catch {
            print(error.localizedDescription)
        }
    }

    func delete(_ teamSchedule: TeamSchedule) {
        loadTeamSchedules()
        var teamSchedules = self.teamSchedules.value
        guard let index = teamSchedules.firstIndex(where: {
            $0.scheduleDate == teamSchedule.scheduleDate
                && $0.scheduleName == teamSchedule.scheduleName
        }) else {
            print("Cant find teamSchedule - \(teamSchedule)")
            return
        }
        
        teamSchedules.remove(at: Int(index))
        self.teamSchedules.accept(teamSchedules)
        
        teamSchedule.personalSchedules.forEach {
            personalScheduleStorageService.delete($0)
        }
        
        do {
            try realm.write {
                  realm.delete(teamSchedule)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
