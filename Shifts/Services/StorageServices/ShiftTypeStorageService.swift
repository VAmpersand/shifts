import Firebase
import RealmSwift
import RxCocoa

final class ShiftTypeStorageService {
    
    let shiftTypes = BehaviorRelay<[ShiftType]>(value: [])
    private let realm = try! Realm()
//    private let firebaseStorageService = FirebaseStorageService()
}

// MARK: - CRUD
extension ShiftTypeStorageService {
    func create(_ shiftType: ShiftType) {
        do {
            try realm.write {
                realm.add(shiftType)
            }
        } catch {
            print(error.localizedDescription)
        }
        shiftTypes.accept(shiftTypes.value + [shiftType])
    }
    
    func isOnStorage(_ shiftType: ShiftType) -> Bool {
        return shiftTypes.value.contains(where: {
            $0.shiftTypeName == shiftType.shiftTypeName
        })
    }
    
    func loadShiftType() {
        shiftTypes.accept(Array(realm.objects(ShiftType.self)))
    }
    
    func createDefaultShiftType() {
        loadShiftType()
        let shiftType = ShiftType()
        shiftType.shiftTypeName = Texts.AddShiftType.defaultShiftTypeName
        shiftType.shiftColorIndex = Colors.shiftTypeColors.count - 1
        
        if !isOnStorage(shiftType) {
            create(shiftType)
        }
    }
    
    func update(_ shiftType: ShiftType, newShiftType: ShiftType) {
        do {
            try realm.write {
                shiftType.shiftTypeName = newShiftType.shiftTypeName
                shiftType.startTime = newShiftType.startTime
                shiftType.endTime = newShiftType.endTime
                shiftType.shiftColorIndex = newShiftType.shiftColorIndex
                shiftType.shiftDuration = newShiftType.shiftDuration
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func delete(_ shiftType: ShiftType) {
        loadShiftType()
        var shiftTypes = self.shiftTypes.value
        guard let index = shiftTypes.firstIndex(where: {
            $0.shiftTypeName == shiftType.shiftTypeName
        }) else {
            print("Cant find shiftType - \(shiftType)")
            return
        }
        
        shiftTypes.remove(at: Int(index))
        self.shiftTypes.accept(shiftTypes)
        
        do {
            try realm.write {
                realm.delete(shiftType)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
