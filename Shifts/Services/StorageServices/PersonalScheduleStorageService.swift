import Firebase
import RealmSwift
import RxCocoa

final class PersonalScheduleStorageService {

    let personalSchedules = BehaviorRelay<[PersonalSchedule]>(value: [])
    private let realm = try! Realm()
    private let shiftTypeStorageService = ShiftTypeStorageService()
//    private let firebaseStorageService = FirebaseStorageService()
    
    public func logRealmPath() {
        print(String(describing: realm.configuration.fileURL?.deletingLastPathComponent().path))
    }
}

// MARK: - CRUD
extension PersonalScheduleStorageService {
    func create(_ personalSchedule: PersonalSchedule) {
        loadPersonalSchedule()
        let personalSchedules = self.personalSchedules.value
        
        personalSchedule.order = personalSchedules.count

        do {
            try realm.write {
                realm.add(personalSchedule)
            }
        } catch {
            print(error.localizedDescription)
        }
        self.personalSchedules.accept(self.personalSchedules.value + [personalSchedule])
        let teamScheduleStorageService = TeamScheduleStorageService()
        teamScheduleStorageService.appendPersonalScheduleIfNeeded(for: personalSchedule.employee)
    }
    
    func isOnStorage(_ personalSchedule: PersonalSchedule) -> Bool {
        return personalSchedules.value.contains(where: {
            $0.employee.firstName == personalSchedule.employee.firstName
                && $0.employee.lastName == personalSchedule.employee.lastName
                && $0.employee.patronymic == personalSchedule.employee.patronymic
        })
    }

    func loadPersonalSchedule() {
        personalSchedules.accept(Array(realm.objects(PersonalSchedule.self).sorted(byKeyPath: "order")))
    }
    
    func createDefaultPersonalSchedule(teamSchedule: TeamSchedule,
                                       employee: Employee) -> PersonalSchedule {
        let personalSchedule = PersonalSchedule()
        personalSchedule.employee = employee
        
        for _ in 1...teamSchedule.scheduleDate.getDaysInMonth() {
            shiftTypeStorageService.loadShiftType()
            let shiftTypes = shiftTypeStorageService.shiftTypes.value
            if let shiftType = shiftTypes.first(where: {
                $0.shiftTypeName == Texts.AddShiftType.defaultShiftTypeName
            }) {
                personalSchedule.shifts.append(shiftType)
            }
        }
        
        return personalSchedule
    }
    
    func update(_ personalSchedule: PersonalSchedule, newPersonalSchedule: PersonalSchedule) {
        do {
            try realm.write {
                personalSchedule.employee = newPersonalSchedule.employee
                personalSchedule.shifts = newPersonalSchedule.shifts
                personalSchedule.order = newPersonalSchedule.order
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func movePersonalSchedules(from sourseIndex: Int, to destinationIndex: Int) {
        loadPersonalSchedule()
        let personalSchedules = self.personalSchedules.value
        do {
            try realm.write {
                let source = personalSchedules[sourseIndex]
                let destination = personalSchedules[destinationIndex]
                
                let destinationOrder = destination.order
                
                if sourseIndex < destinationIndex {
                    for index in sourseIndex...destinationIndex {
                        let personalSchedule = personalSchedules[index]
                        personalSchedule.order -= 1
                    }
                } else {
                    for index in (destinationIndex..<sourseIndex).reversed() {
                        let personalSchedule = personalSchedules[index]
                        personalSchedule.order += 1
                    }
                }
                source.order = destinationOrder
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
//    func movePersonalSchedules(for employees: [Employee]) {
//        loadPersonalSchedule()
//        let personalSchedules = self.personalSchedules.value
//        
//        personalSchedules.forEach { personalSchedule in
//            if let employee = employees.first(where: {
//                $0.firstName == personalSchedule.employee.firstName
//                    && $0.lastName == personalSchedule.employee.lastName
//                    && $0.patronymic == personalSchedule.employee.patronymic
//            }) {
//                do {
//                    try realm.write {
//                        personalSchedule.setValue(employee.order, forKey: "order")
//                        self.personalSchedules.accept(Array(realm.objects(PersonalSchedule.self)))
//                    }
//                } catch {
//                    print(error.localizedDescription)
//                }
//            }
//        }
//    }
    
    func updateSchedule(_ personalSchedule: PersonalSchedule, newPersonalSchedule: PersonalSchedule) {
            do {
                try realm.write {
                    personalSchedule.setValue(newPersonalSchedule.shifts, forKey: "shifts")
                    self.personalSchedules.accept(Array(realm.objects(PersonalSchedule.self)))
                }
            } catch {
                print(error.localizedDescription)
            }
     }
    
    func delete(_ personalSchedule: PersonalSchedule) {
        loadPersonalSchedule()
        var personalShiftsArray = self.personalSchedules.value
        guard let index = personalShiftsArray.firstIndex(where: {
            $0.employee == personalSchedule.employee
        }) else {
            print("Cant find personalSchedule - \(personalSchedule)")
            return
        }

        personalShiftsArray.remove(at: Int(index))
        self.personalSchedules.accept(personalShiftsArray)

        do {
            try realm.write {
                  realm.delete(personalSchedule)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
