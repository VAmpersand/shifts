import Firebase
import RealmSwift
import RxCocoa

final class EmployeesStorageService {
    
    let employees = BehaviorRelay<[Employee]>(value: [])
    private let realm = try! Realm()
    private let personalScheduleStorageService = PersonalScheduleStorageService()
//    private let firebaseStorageService = FirebaseStorageService()
    
    // For log purposes
    public func logRealmPath() {
        print(String(describing: realm.configuration.fileURL?.deletingLastPathComponent().path))
    }
}

// MARK: - CRUD
extension EmployeesStorageService {
    func create(_ employee: Employee) {
        do {
            try realm.write {
                realm.add(employee)
            }
        } catch {
            print(error.localizedDescription)
        }
        self.employees.accept(self.employees.value + [employee])
    }
    
    func isOnStorage(_ employee: Employee) -> Bool {
        return employees.value.contains(where: {
            $0.firstName == employee.firstName
            && $0.lastName == employee.lastName
            && $0.patronymic == employee.patronymic
            && $0.position == employee.position
        })
    }
    
    func loadEmployees() {
        employees.accept(Array(realm.objects(Employee.self)))
    }
    
    func update(_ employee: Employee, newEmployee: Employee) {
        do {
            try realm.write {
                employee.firstName = newEmployee.firstName
                employee.lastName = newEmployee.lastName
                employee.patronymic = newEmployee.patronymic
                employee.position = newEmployee.position
                employee.adminRights = newEmployee.adminRights
            }
        } catch {
            print(error.localizedDescription)
        }
    }
        
    func deleteEmployeeWithPersonalSchedules(_ employee: Employee) {
        personalScheduleStorageService.loadPersonalSchedule()
        let personalSchedules = personalScheduleStorageService.personalSchedules.value
        
        personalSchedules.forEach {
            if let personalScheduleEmployee = $0.employee,
                personalScheduleEmployee.firstName == employee.firstName
                    && personalScheduleEmployee.lastName == employee.lastName
                    && personalScheduleEmployee.patronymic == employee.patronymic
                    && personalScheduleEmployee.position == employee.position {
                personalScheduleStorageService.delete($0)
            }
        }
        
        delete(employee)
    }
    
    func delete(_ employee: Employee) {
        loadEmployees()
        var employees = self.employees.value
        guard let index = employees.firstIndex(where: {
            $0.firstName == employee.firstName
                && $0.lastName == employee.lastName
                && $0.patronymic == employee.patronymic
        }) else {
            print("Cant find employee - \(employee)")
            return
        }
        
        employees.remove(at: Int(index))
        self.employees.accept(employees)
        
        do {
            try realm.write {
                realm.delete(employee)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
}
