import UIKit

class SceneDelegate: UIResponder {
    
    // weaver: mainRouter = MainRouter
    // weaver: mainRouter.scope = .container
    
    // weaver: employeesStorageService = EmployeesStorageService
    // weaver: employeesStorageService.scope = .container
    
    // weaver: shiftTypeStorageService = ShiftTypeStorageService
    // weaver: shiftTypeStorageService.scope = .container
    
    // weaver: teamScheduleStorageService = TeamScheduleStorageService
    // weaver: teamScheduleStorageService.scope = .container
    
    private let dependencies = SceneDelegateDependencyContainer()
    
    var window: UIWindow?
}


// MARK: - UIWindowSceneDelegate
extension SceneDelegate: UIWindowSceneDelegate {
    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        dependencies.mainRouter.startApp(in: scene)
    }
}




